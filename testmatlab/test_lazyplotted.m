% Start from the Cohen-Daubechies-Feauveau wavelet 
% and get the corresponding lifting scheme.
lscdf = liftwave('cdf3.1');

% Visualize the obtained lifting scheme.
displs(lscdf);

lscdf = {...                                         
'p'             [ -0.33333333]              [-1]  
'd'             [ -0.37500000 -1.12500000]  [1]   
'p'             [  0.44444444]              [0]   
[  2.12132034]  [  0.47140452]              []    
};                                                

% Transform the lifting scheme to biorthogonal
% filters quadruplet.
[LoD,HiD,LoR,HiR] = ls2filt(lscdf);

% Visualize the two pairs of scaling and wavelet
% functions.
bswfun(LoD,HiD,LoR,HiR,'plot');