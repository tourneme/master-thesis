% 1/ pr?paration des donn?es ? simuler
% 1.a) X et Y sont tir?s dans [0 1[
taille=6000;
X=rand(taille,1) ;
Y=rand(taille,1) ;

% 1.b) on simule Z = X^a * Y^b apr?s avoir ajout? du bruit ? X et Y
bruit = 0.1 ;
a=1.75 ;
b=-0.9 ;
Z=(X+bruit*rand(taille,1)).^a .* (Y+bruit*rand(taille,1)).^b ;

% 2/ d?finition du mod?le ? r?soudre
px=13 ; % degr? max en X
py=13 ; % degr? max en Y
ptot=13 ; % degr? max total
[i,j]=meshgrid(0:px,0:py) ; % g?n?ration de toutes les possibilit?s
u=find(i+j<=ptot) ; % ?carter les degr?s trop ?lev?s
i=i(u) ; j=j(u) ;
nk=length(u) ; % Nombre de param?tres ? calculer ?

% 3/ R?solution du probl?me
% 3.a) on calcule 4 matrices de dimention [taille * nk]
nx = length(X) ; % combien de donn?es ?
X=repmat(X,1,nk) ;
Y=repmat(Y,1,nk) ;
i=repmat(i',nx,1) ;
j=repmat(j',nx,1) ;

% 3.b) on les assemble dans la matrice M
M=X.^i.*Y.^j ;

% 3.c1) r?solution normale par le \ de Matlab
k_matlab = M\Z ;

% 3.c2) pour compraison, r?solution par inv(X'X) 
k_manuel = inv(M' * M) * M' * Z ;

% 3.d) comparaison des coefficients du polynome par les deux m?thodes
% C'est quasiment la meme chose, mais pas tout ? fait.
% Dans certains cas d?g?n?r?s, l'inversion peut etre fausse et l'anti-divison contnuer ? trouver le bon r?sultat.
disp('R?ultats de chaque calcul') ;
disp ([k_matlab k_manuel]) ;

disp('diff?rence relative entre les deux m?thodes') ;
disp(k_matlab-k_manuel) ;

% 4. Calcul des Z simul?s
Z_matlab = M*k_matlab ;
Z_manuel = M*k_manuel ;
differences = (Z_matlab-Z_manuel) ;

disp ('l''ecart type des differences en Z entre chaque version est') ;
disp (std(differences)) ;

% 5. calcul du coeficient de correlation
residus = Z-Z_matlab ;
r2 = var(Z_matlab)/var(Z) ;
disp(sprintf('coefficient de correlation r2 = %f', r2)) ;
