close all
base_path_1='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
path(path, base_path_1);
path(path, [base_path_1 'toolbox/']);
path(path,'/Users/cducroz/Documents/Robin/CODE MATLAB/reusable_code');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode/test.mat');

load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode/WaveletBankBW500inv200.mat');
check_bool=0;


load('/Users/cducroz/Documents/Robin/CODE MATLAB/DATA/data_sph/06012010_Wt-1H_#0009_T0030_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/15-01-2010_WT-1H30_#06_T68_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/20100107_E64 1H_#0004_T0008_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/20100106_E64-1H30_#0001_T0015_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/Amoebas treated/06102010_WT1H_smo/06102010_WT1H_#0005_smo/06102010_Wt-1H_#0005_T0071_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/20100106_E64-1H30_#0001_T0019_CALD_smo.mat');

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/initParamCALD/15-01-10_WT_30min_#02_T08_CALD_ini.mat');


plot_mesh(vertices,faces);

%view for 6/12 WT 1h 9 T30

view(10,-40);
camlight('left');

%view for 15/1 WT 30 2 T8

% view(60,-60);




if ~exist('/Users/cducroz/Documents/Robin/CODE MATLAB/results/wavelet_coef_overcomplete/amoeba_of_interest.mat','file');

    disp('Performing Wavelet Decomposition');



    vertices=vertices-repmat(mean(vertices,1),length(vertices),1);


    vertexFaces =  MARS_convertFaces2FacesOfVert(int32(faces), int32(size(sph_verts, 1)));
    num_per_vertex = length(vertexFaces)/size(sph_verts,1);
    vertexFaces = reshape(vertexFaces, size(sph_verts,1), num_per_vertex);

    vertexNbors = MARS_convertFaces2VertNbors(int32(faces), int32(size(sph_verts,1)));
    num_per_vertex = length(vertexNbors)/size(sph_verts,1);
    vertexNbors = reshape(vertexNbors, size(sph_verts,1), num_per_vertex);

    sbjMesh.vertices=sph_verts';
    sbjMesh.metricVerts=vertices';
    sbjMesh.faces=faces';
    sbjMesh.vertexFaces=vertexFaces';
    sbjMesh.vertexNbors=vertexNbors';

    [recon_cell, analysis_cell] = PartiallyReconMesh(sbjMesh, analysis_filters_my, synthesis_filters_my, check_bool);

    save('/Users/cducroz/Documents/Robin/CODE MATLAB/results/wavelet_coef_overcomplete/amoeba_of_interest.mat','recon_cell','analysis_cell');
else
    load('/Users/cducroz/Documents/Robin/CODE MATLAB/results/wavelet_coef_overcomplete/amoeba_of_interest.mat');
end
    

for i=[1,2,3]

    a=analysis_cell{i};
    b=recon_cell{i};

    t=sqrt(sum(a.vertices.^2,1));

    
    figure;
    s_handle=trisurf(b.faces',b.vertices(1,:)',b.vertices(2,:)',b.vertices(3,:)',t);
    shading interp;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    axis off;
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    axis equal;
    
    %view for 6/12 WT 1h 9 T30
    
    view(10,-40);
%     view(0,90);

    %view for 15/1 WT 30 2 T8

%     view(60,-60);


    Level{i}=1:length(t);
    j=i;
    k=1;
    plot_thresh=1;
    add=1;
%     fw_clean= WAT(a.vertices,Level,add,j,k,0);
%     
%     max_fw=max(max(abs(a.vertices)));
%     min_fw=min(min(abs(a.vertices)));
%     
%     for i=1:3
%     
%         ind_fw=find(abs(fw_clean(:,i))<0.5*max_fw & fw_clean(:,i)~=0);
%         
%         
%         
%         fw_clean(ind_fw,:)=0;
%     
%     end
%     
%     fw_tot=sqrt(sum(fw_clean.^2,2));
    
%     fw_tot_final= WAT(fw_tot,Level,add,j,k,plot_thresh);

    max_t=max(max(abs(t)));
    min_t=min(min(abs(t)));

    if i==1
        ind_fw=find(t<min_t+0.65*(max_t-min_t));
    elseif i==2
        %good choice for 6/12 WT 1h 9 T30
        ind_fw=find(t<min_t+0.45*(max_t-min_t));
    elseif i==3
        %perfect for 6/12 WT 1h 9 T30
        ind_fw=find(t<min_t+0.41*(max_t-min_t));
    else
        
    end
    
    fw_tot=t;
    
    fw_tot(ind_fw)=0;

    
    figure;
    s_handle=trisurf(b.faces',b.vertices(1,:)',b.vertices(2,:)',b.vertices(3,:)',fw_tot);
    shading interp;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    axis off;
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    axis equal;
    
%   view for 6/12 WT 1h 9 T30
    
    view(10,-40);
%     view(0,90)
    
    %view for 15/1 WT 30 2 T8

%     view(60,-60);
    
    
%     saveas(gcf,['/Users/cducroz/Documents/Robin/CODE MATLAB/rec_graph/bleb_first_amoeba11_level_' num2str(i) '.fig']);
%     saveas(gcf-1,['/Users/cducroz/Documents/Robin/CODE MATLAB/rec_graph/overcomplete_detection_first_amoeba11_level_' num2str(i) '.fig']);

end





