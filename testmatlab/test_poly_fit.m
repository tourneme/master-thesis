

function y = test_poly_fit( x, a, b, c, d, k )
% PIECEWISELINE   A line made of two pieces
% that is not continuous.

y = zeros( size( x ) );

% This example includes a for-loop and if statement
% purely for demonstration purposes.
for i = 1:length( x )
    if x(i) < k,
        y(i) = a + b.* x(i);
    else
        y(i) = c + d.* x(i);
    end
end
end


