function test_construct_zones

path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');
path(path, '../reusable_code');

global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);

path(path, [RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);




gaus='05';
blebs='03';
frame='092';

close all;
figure;

base='/Users/cducroz/Documents/Robin/CODE MATLAB/DATA/data_sph_simu/zones/';

load([base 'cir_gaus#' gaus '_bleb#' blebs '_#' frame '.mat']);

%test
% zones=[0,0,0,0.25,0.25,0.4;...
% 	   0,0,0,0.4,0.4,0.4];

SOI=min(1.05*min(zones(1,4),zones(1,5)),max(zones(1,4),zones(1,5)));
otherGS=min(1.05*min(zones(4,4),zones(4,5)),max(zones(4,4),zones(4,5)));

dsig=max(SOI/otherGS,otherGS/SOI);


div=(0.38/0.6)*dsig+(1.62-0.38/0.6);

mindist=2*(atan(SOI/div)+atan(otherGS/div));
lol=mindist*180/pi;

% zones(2,1)=mindist;

   
%creating the base mesh

options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;

[vertex,face] = compute_semiregular_sphere(5,options);

vertices = vertex{end};
faces = face{end}';
vertices=vertices';


for i=[3,2,5]
	zones(i,6)=0;
end
for i=1:size(zones,1)
	%compute every blebs
	
	[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
	theta=pi/2-theta;
	
	k=zones(i,6);
	sigma1=zones(i,4);
	sigma2=zones(i,5);
	
	new_radius=Gaussian2D(k,sigma1,sigma2,theta,phi);
	radius=radius+new_radius;
	
	[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,pi/2-theta,radius);
	
	%rotation of the cell in order not to have mountains of blebs
	if i~=size(zones,1)
		R=rotate_mat(zones(i+1,1),zones(i+1,2),zones(i+1,3));		
		vertices=R*vertices';
		vertices=vertices';
	end
	s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),radius);
	hold on
	plot3([0 2],[0 0],[0 0],'-b');
	plot3([0 0],[0 2],[0 0],'-r');
	plot3([0 0],[0 0],[0 2],'-g');
	hold off
	lighting phong;
	camproj('perspective');
	axis square;
	axis tight;
	axis off;
	axis equal;
	shading interp;
	view(90, 0);
	camlight('headlight', 'infinite');
	view(270, 0);
	camlight('headlight', 'infinite');
	set(s_handle, 'DiffuseStrength', 1);
	set(s_handle, 'SpecularStrength', 0);
	view(0, 0);
	cameramenu;
	
		keyboard;

end


[phi,theta] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));

%has to be replaced by the right spherical parameterization (here we
%are in an exception because our cell is convex, so we can "cheat")
[sph_verts(:,1),sph_verts(:,2),sph_verts(:,3)]=sph2cart(phi,theta,ones(length(theta),1));

inFinal=[ RELATIV_ROOT 'DATA/data_sph_simu/cir_gaus#02_bleb#01_#001.mat'];

save(inFinal,'vertices','faces','sph_verts');

% 	SPHARM extraction following Shen method
%%
%Spherical harmonic calculation on the BW.
BW=25;

confs.MaxSPHARMDegree = BW;
confs.OutDirectory = [RELATIV_ROOT 'DATA/data_sph_simu/_des/'];

if ~exist(confs.OutDirectory,'dir')
	mkdir(confs.OutDirectory);
end

% 	outDes = SpharmMatExpansion(confs, inSmo, 'ExpLSF');
inFinalCell=cell(1);
inFinalCell{1}=inFinal;
outDes = SpharmMatExpansion(confs, inFinalCell, 'ExpLSF');
newoutDes=[inFinal(1:22) '_des/' inFinal(23:end)];
java.io.File(outDes{1}).renameTo(java.io.File(newoutDes))
movefile(newoutDes,inFinal(1:21));


delete('/Users/cducroz/Documents/Robin/CODE MATLAB/results/overcomplete_coef_simu/LoG/BW25max_invert25kernel_from_512_to_016sigma1/amoeba_cir_gaus#02_bleb#01_#001.mat');


 %set to 1 if you want to extract the wavelet coefficient from your
    %SPHERICAL representation of the cartesian function
    coef_extraction=1;
    
    %Laplacian filter parameters (you directly have to change the function
    %filter_choice
    [BW, max_invertibility, kernel_sizes, sigma]=filter_choice;
    
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

    %set to 1 if you want to do the k-mean, Otsu or WAT analysis
    coef_analysis=1;

        %levels of depth you want to dig in
        levels=2:3;
        

		num_over=2;
		
		cells{1}.type='cir';
		cells{1}.gaus=2;
		cells{1}.blebs=1;
		cells{1}.k=0.2;
		cells{1}.sigma1=0.2;
		cells{1}.sigma2=0.1;
		cells{1}.frames=1;
		cells{1}.on=2;

		correction=0;
		
[num,cells]=overcomplete_wavelet(num_over,levels,cells,correction,BW,...
	max_invertibility,kernel_sizes,sigma);

bleb_detection(num,levels,cells,correction,BW,max_invertibility,...
	kernel_sizes,sigma);


end












