path('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes',path);



% 
% base_path_1='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
% path(path, base_path_1);
% path(path, [base_path_1 'toolbox/']);
% path(path, '../reusable_code');
% base_path_2='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/';
% path(path, [base_path_2 'utility/']);
% path(path, [base_path_2 'rotation/']);
% path(path, [base_path_2 'experiments/']);
% path(path, [base_path_2 'dswt/']);
% path(path, base_path_2);
% 
% 
%     Folder = [base_path_2 'data_sph/'];
%     
%     %the following suffix helps to go to very interesting cells
%     Folder=[Folder 'Amoebas treated/06012010_Wt-1H_smo/06012010_Wt-1H_#0009_smo/'];
%     
%     d = dir([Folder '*.mat']);
% 
%             name =  d(29).name ;
%             disp(name);
% 
%             load([Folder name]);

close all;

plot_mesh(vertices,faces);shading faceted; axis tight