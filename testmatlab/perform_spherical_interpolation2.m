function f = perform_spherical_interpolation2(pos,posInit,fI,center)

if size(pos,1)>size(pos,2)
    pos = pos';
end

if nargin<3
    center = 1;
end


y = atan2(posInit(2,:),posInit(1,:))/(2*pi) + 1/2;
if center
    x = acos(posInit(3,:))/(2*pi) + 1/4;
else
    x = acos(posInit(3,:))/(pi);
end
disp('size');
disp(size(x(:)));
disp(size(y(:)));

figure;
hold on
keyboard;
for i=1:length(x)
plot(x(i),y(i),'*r');
end

Y = atan2(pos(2,:),pos(1,:))/(2*pi) + 1/2;
if center
    X = acos(pos(3,:))/(2*pi) + 1/4;
else
    X = acos(pos(3,:))/(pi);
end

disp(size(X(:)));
disp(size(Y(:)));

f = interp2( y(:), x(:), fI, Y(:), X(:) );
end
