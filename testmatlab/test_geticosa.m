Folder='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';

    global RELATIV_ROOT

    

path(path, Folder);

% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;

[vertex,face] = compute_semiregular_sphere(6,options);



sph_verts = vertex{end}';
faces = face{end}';

save([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_6.mat'],'sph_verts','faces');
            

Z = calculate_SPHARM_basis(sph_verts, 150-1);


save([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_6_SPHARMed.mat'],'Z');

