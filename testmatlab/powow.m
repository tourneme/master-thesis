function result=powow(n,p,varargin)

if size(varargin)<1
    result=1;
else
    result=varargin{1};
end

if mod(p,2)==1
    result=result*n;
end
    n=n*n;

if p>=1
    result=powow(n,floor(p/2),result);
end
end