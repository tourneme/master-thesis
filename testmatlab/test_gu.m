global RELATIV_ROOT
RELATIV_ROOT='../';
path(path, '../reusable_code');

write=0;

cell_name='19-06-09_E64_1h30_#00_T02';

load([RELATIV_ROOT 'DATA/data_sph/' cell_name '.mat']);

if write

	
	fid = fopen([RELATIV_ROOT 'DATA/data_sph_gu/' cell_name '.m'],'w');
	
	vertices=vertices-repmat(mean(vertices,1),length(vertices),1);
	
	
	
	
	for i=1:length(vertices)
		
		Ngb=findNeighbors(i,faces);
		
		normal=-sum(vertices(Ngb,:)-repmat(vertices(i,:),length(Ngb),1),1);
		
		fprintf(fid,'Vertex %u  %6.3f %6.3f %6.3f {normal=(%6.3f %6.3f %6.3f)}\n',i,vertices(i,1),vertices(i,2),vertices(i,3),normal(1),normal(2),normal(3));
	end
	
	for i=1:length(faces)
		fprintf(fid,'Face %u  %u %u %u\n',i,faces(i,1),faces(i,2),faces(i,3));
	end
	fclose(fid);
end

read=1;

if read
	[type,idx,x,y,z,rest1,rest2,rest3,rest4,rest5,rest6,rest7]=textread([RELATIV_ROOT 'DATA/data_sph_gu/results/m/' cell_name '.sphere.m'],...
		'%s %u %f %f %f %s %s %s %s %s %s %s');
	
	for i=1:length(type)
		if strcmp(type{i},'Vertex')
			sph_verts(idx(i),1)=x(i);
			sph_verts(idx(i),2)=y(i);
			sph_verts(idx(i),3)=z(i);
		end
	end
	
	save([RELATIV_ROOT 'DATA/data_sph_gu/results/' cell_name '.mat'],...
		'vertices','faces','sph_verts');
	
end
	
