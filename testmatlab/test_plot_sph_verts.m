clear all
close all

global RELATIV_ROOT
RELATIV_ROOT='../';
base_path=[RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/'];
path(path, [base_path 'toolbox/']);
path(path, ['../reusable_code/']);


% TO GET A ROUND CELL
% Folder = [RELATIV_ROOT 'DATA/data_sph_Backup'];
% 
%     
% names=dir2cell(Folder);
% 
% cells.date='06-01-10';
% cells.type='WT_';
% cells.time='1h00';
% cells.number='09';
% cells.frames=30;
% % cells{1}.set=1;
% % cells{1}.on=1;
% 
% valuesss=getNum(cells,[RELATIV_ROOT 'DATA/data_sph']);


% TO GET THE RIGHT CELL ^^

Folder = [RELATIV_ROOT 'DATA/data_sph'];

    
names=dir2cell(Folder);

cells.date='06-01-10';
cells.type='WT_';
cells.time='1h00';
cells.number='09';
cells.frames=30;
% cells{1}.set=1;
% cells{1}.on=1;

valuesss=getNum(cells,[RELATIV_ROOT 'DATA/data_sph']);

% figure;
% 
subject =  names{valuesss} ;

load([Folder,'/',subject]);
disp(subject); 
    
mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);
% plot_mesh(vertices,faces); 
% % axis on


% figure;
% 
% verts=0.48*vertices(:,1)/max(abs(vertices(:,1)));
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);
% 
% 
% h1=subplot(1,5,1);
% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on
% 
% verts=0.55*vertices(:,1)/max(abs(vertices(:,1)));
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);
% 
% h2=subplot(1,5,2);
% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on
% 
% verts=0.60*vertices(:,1)/max(abs(vertices(:,1)));
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);
% 
% h3=subplot(1,5,3);
% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on
% 
% verts=0.65*vertices(:,1)/max(abs(vertices(:,1)));
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);
% 
% h4=subplot(1,5,4);
% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on
% 
% verts=0.7*vertices(:,1)/max(abs(vertices(:,1)));
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);
% 
% h5=subplot(1,5,5);
% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on
% 
% hlink = linkprop([h1 h2 h3 h4 h5],{'CameraPosition','CameraUpVector'});
% key = 'graphics_linkprop';
% % Store link object on first subplot axes
% setappdata(h1,key,hlink);
% 
figure;
e0=subplot(1,3,1);
s=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),vertices(:,1));
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s, 'DiffuseStrength', 1);
set(s, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
cameramenu;


% 
% 
% 
% 
% 
% 
% 
% figure;
% 
% verts=0.48*vertices(:,2)/max(abs(vertices(:,2)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% 
% h1=subplot(1,5,1);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.55*vertices(:,2)/max(abs(vertices(:,2)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h2=subplot(1,5,2);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.60*vertices(:,2)/max(abs(vertices(:,2)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h3=subplot(1,5,3);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.65*vertices(:,2)/max(abs(vertices(:,2)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h4=subplot(1,5,4);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.7*vertices(:,2)/max(abs(vertices(:,2)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h5=subplot(1,5,5);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% hlink = linkprop([h1 h2 h3 h4 h5],{'CameraPosition','CameraUpVector'});
% key = 'graphics_linkprop';
% % Store link object on first subplot axes
% setappdata(h1,key,hlink);
% 

e1=subplot(1,3,2);

verts=0.7*vertices(:,2)/max(abs(vertices(:,2)));
yx=sph_verts(:,1).*(1+verts);
yy=sph_verts(:,2).*(1+verts);
yz=sph_verts(:,3).*(1+verts);

s=trisurf(faces,yx,yy,yz,vertices(:,2));
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s, 'DiffuseStrength', 1);
set(s, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
cameramenu;


% 
% figure;
% 
% verts=0.48*vertices(:,3)/max(abs(vertices(:,3)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% 
% h1=subplot(1,5,1);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.55*vertices(:,3)/max(abs(vertices(:,3)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h2=subplot(1,5,2);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.60*vertices(:,3)/max(abs(vertices(:,3)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h3=subplot(1,5,3);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.65*vertices(:,3)/max(abs(vertices(:,3)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h4=subplot(1,5,4);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% verts=0.7*vertices(:,3)/max(abs(vertices(:,3)));
% yx=sph_verts(:,1).*(1+verts);
% yy=sph_verts(:,2).*(1+verts);
% yz=sph_verts(:,3).*(1+verts);
% 
% h5=subplot(1,5,5);
% plot_mesh([yx yy yz],faces);
% shading faceted;
% axis on
% 
% hlink = linkprop([h1 h2 h3 h4 h5],{'CameraPosition','CameraUpVector'});
% key = 'graphics_linkprop';
% % Store link object on first subplot axes
% setappdata(h1,key,hlink);
% 
e2=subplot(1,3,3);


s=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),vertices(:,3));
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s, 'DiffuseStrength', 1);
set(s, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
cameramenu;


hlink = linkprop([e0 e1 e2],{'CameraPosition','CameraUpVector'});
key = 'graphics_linkprop';
% Store link object on first subplot axes
setappdata(e0,key,hlink);



figure;



verts=sqrt(sum(vertices.^2,2));

% verts=0.6*(verts/max(verts)-0.5);
% xx=sph_verts(:,1).*(1+verts);
% xy=sph_verts(:,2).*(1+verts);
% xz=sph_verts(:,3).*(1+verts);


verts=verts/max(verts);
xx=sph_verts(:,1).*(verts);
xy=sph_verts(:,2).*(verts);
xz=sph_verts(:,3).*(verts);

s=trisurf(faces,xx,xy,xz,verts);
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s, 'DiffuseStrength', 1);
set(s, 'SpecularStrength', 0);
view(0,0);
% zoom(1.4);
cameramenu;

% plot_mesh([xx xy xz],faces);
% shading faceted;
% axis on


% 
% 
% figure;
% 
% d0d=subplot(1,2,1);
% s=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),verts);
% lighting phong;
% camproj('perspective');
% axis square; 
% axis tight;
% axis off;
% axis equal;
% shading interp;
% view(90, 0);
% camlight('headlight', 'infinite');
% view(270, 0);
% camlight('headlight', 'infinite');
% set(s, 'DiffuseStrength', 1);
% set(s, 'SpecularStrength', 0);
% view(0,0);
% % zoom(1.4);
% cameramenu;
% 
% dd=subplot(1,2,2);
figure;

s1=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),verts);
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s1, 'DiffuseStrength', 1);
set(s1, 'SpecularStrength', 0);
view(0,0);
% zoom(1.4);
cameramenu;
% 
% % hlink = linkprop([d0d dd],{'CameraPosition','CameraUpVector'});
% % key = 'graphics_linkprop';
% % Store link object on first subplot axes
% % setappdata(d0d,key,hlink);
