global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');
path(path, [RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT '/reusable_code']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/export_fig']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code/']);


path(path, [RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);


path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/export_fig']);

% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;
J = 7;
[vertex,face] = compute_semiregular_sphere(J,options);

clf;
nverts = size(vertex{end}, 2);
i = 0;
for j = 3
	figure;
    i = i+1;
    nj = size(vertex{j},2); nj1 = size(vertex{j+1},2);
    sel = nj+1:nj1-1;
    d = sum( abs(vertex{end}(:,sel)) );
    [tmp,k] = min(d); k = sel(k);
    fw2 = zeros(nverts,1); fw2(k) = 1;
    f2 = perform_wavelet_mesh_transform(vertex,face, fw2, -1, options);
	
	
vertices=vertex{end};
faces=face{end};
	figure;
s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),f2);
lighting phong;
camproj('perspective');

axis square;
% axis tight;
axis off;
% axis equal;
view(90, 0);
shading interp;
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
%this very tiny line (cameramenu) gives you a perfect tool to make the
%cell move, unfortunately, you will have to type it again after zooming
%on the cell...
cameramenu;
export_fig('/Users/cducroz/Documents/Robin/rendu/rapport/latex/pictures/figureintro/erreurSPHARMMM.pdf','-pdf','-transparent')
	
%     options.color = 'wavelets';
%     options.use_color = 1;
%     options.rho = .6;
%     options.use_elevation = 1;
%     options.view_param = [104,-40];
%     plot_spherical_function(-vertex{end},face{end},f2, options); axis tight;
%     title(['Wavelet, scale ' num2str(j)]);
end
options.view_param = [];