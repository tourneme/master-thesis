CALD

	if act_cell.blebs==1
		inObj{1}=[ RELATIV_ROOT 'DATA/data_sph_simu/_obj/' act_cell.type '_bleb#'...
			strblebs(act_cell.blebs) '_k' num2str(act_cell.k) '_sigma1_'...
			num2str(act_cell.sigma1) '_sigma2_' num2str(act_cell.sigma2)...
			'_#' right_frame(frame) '_obj.mat'];
	else
		inObj{1}=[ RELATIV_ROOT 'DATA/data_sph_simu/_obj/' act_cell.type  ...
			'_bleb#' strblebs(act_cell.blebs) '_#' right_frame(frame)...
			'_obj.mat'];
	end
	save(inObj{1},'vertices','faces');


	confs.MeshGridSize = 50;
	confs.MaxSPHARMDegree = 6;
	confs.Tolerance = 2;
	confs.Smoothing = 2;
	confs.Iteration = 100;
	confs.LocalIteration = 10;
	% Available values for t_major- 'x';'y'
	confs.t_major = 'x';
	% Available values for SelectDiagonal- 'ShortDiag';'LongDiag'
	confs.SelectDiagonal = 'ShortDiag';

	confs.OutDirectory = [RELATIV_ROOT 'DATA/data_sph_simu/_smo/'];

	if ~exist(confs.OutDirectory,'dir')
		mkdir(confs.OutDirectory);
	end

	inSmo = SpharmMatParameterization(confs, inObj, 'ParamCALD');

	load(inSmo{1});