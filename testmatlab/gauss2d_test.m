close all;
figure;
[X,Y]=meshgrid(-1:0.01:1,-1.5:0.01:1.5);
k=0.3;
sigma1=0.25;
sigma2=0.6;
Z = k * exp(-X.^2/(2*sigma1^2)+ - Y.^2/(2*sigma2^2));                                        
s_handle=surf(X,Y,Z);
alpha(s_handle,0.6)

Yel=-sigma2:0.01:sigma2;
Xel=sigma1*sqrt(1-Yel.^2/sigma2^2);
Yel=[Yel,Yel];
Xel=[Xel,-Xel];

hold on;
plot([-sigma1,sigma1],[0,0],'-k','LineWidth',2);
plot([0,0],[-sigma2,sigma2],'-k','LineWidth',2);
text([sigma1+0.05,0],[0,sigma2+0.15],{'\sigma_1','\sigma_2'},'FontSize',20,'FontWeight','bold','FontName','verdana');
% text(sigma1+0.05,0,'\sigma','FontSize',20,'FontWeight','bold','FontName','verdana');


plot(Xel,Yel,'-k','LineWidth',2);

% [Xs,Ys,Zs]=sphere(100);
% 
% Zs=Zs-1;
% 
% s_handle2=surf(Xs,Ys,Zs,max(max(Z))*ones(length(Zs),length(Zs)));
% alpha(s_handle2,0.3)
% 
% 
% plot3([0,0],[0,0],[-1,0],'-k','LineWidth',3,'LineWidth',2);
% plot3([0,0.7*sigma1],[0,0],[-1,0],'-k','LineWidth',2);
% 
% xc=0:0.01:0.068;
% zc=0.25*sqrt(1-xc.^2)-1;
% yc=zeros(length(xc),1);
% 
% plot3(xc,yc,zc,'-k','LineWidth',3);
% text(0.1,0,-0.75,'MinDist = 2Arctan( \sigma  / Fact)','FontSize',25,'FontWeight','bold','FontName','verdana');

shading interp;
axis square;
lighting phong;
camproj('perspective')
cameramenu
% axis([-1.5,1.5,-1.5,1.5,-2,0.3]);
axis tight;
axis equal;
view(-35,35);
axis off;
