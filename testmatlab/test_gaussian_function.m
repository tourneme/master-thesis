close all;
clear all;

global RELATIV_ROOT
RELATIV_ROOT='../';

path(path,[RELATIV_ROOT 'Toolboxes and functions/GridSphere']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/splinefit']);
path(path,[RELATIV_ROOT 'reusable_code']);


cells{1}.date='06-01-10';
cells{1}.type='WT_';
cells{1}.time='0h30';
cells{1}.number='00';
cells{1}.frames=2; %0-90


Folder_names = [RELATIV_ROOT 'DATA/data_sph'];
num_name=getNum(cells{1},Folder_names);

names=dir2cell(Folder_names);
load([Folder_names,'/',names{num_name}]);

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

verts=sqrt(sum(vertices.^2,2));
xx=sph_verts(:,1).*(verts);
xy=sph_verts(:,2).*(verts);
xz=sph_verts(:,3).*(verts);

[theta,phi]=Cartesian2Spherical(sph_verts(:,1),sph_verts(:,2),sph_verts(:,3));

figure;

subplot(1,2,1);

x=-90:6:90;
[n,xout]=hist(theta,x);

n=n(2:end-1)./(2*pi*cos(x(2:end-1)*pi/180));

bar(xout(2:end-1),n);

hold on

xc=[-90 90 -90 90];
yc=[0 0 0 0];
cc=[1,1,0,0;0,0,1,1];
con=struct('xc',xc,'cc',cc);

fit=splinefit([-90 x(2:end-1) 90] ,[0 n-min(n) 0],1,11,'r',con);

plot(-90:90,ppval(fit,-90:90)+min(n),'.r');

fitder.breaks=fit.breaks;
fitder.form=fit.form;
fitder.pieces=fit.pieces;
fitder.order=fit.order-1;
fitder.dim=fit.dim;

for i=1:size(fit.coefs,1)
    
    fitder.coefs(i,:)=polyder(fit.coefs(i,:));
end
plot(-90:90,-10*ppval(fitder,-90:90),'.g');

plot([80,90],[10,0],'-c',[-90,-80],[0 -10],'-c');

subplot(1,2,2);
s_handle=trisurf(faces,xx,xy,xz,verts);
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
% set(s_handle, 'DiffuseStrength', 1);
% set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
cameramenu;

modif=ppval(fitder,theta);

theta_modif=theta-20*modif;

% grid=1:0.01:10;
% 
% n_total=zeros(length(x),length(grid));
% 
% idx=0;
% for i=grid
%     idx=idx+1;
% 
%     theta_modif=theta-modif.^(i);
% 
%     [n,xout]=hist(theta_modif,x);
% 
%     n_total(:,idx)=n;
% end
% 
% var_distrib=var(n_total);
% 
% figure;
% 
% plot(grid,var_distrib);
% 
% min_value=min(var_distrib);
% pos_min=find(var_distrib==min_value);
% hold on
% disp(['the minimum is at ' num2str(grid(pos_min))]);
% plot(grid(pos_min),min_value,'*r');
% 
% 
% theta_modif=theta_modif-modif.^(pos_min(1));

[n,xout]=hist(theta_modif,x);

figure;
subplot(1,2,1);

n=n(2:end-1)./(2*pi*cos(x(2:end-1)*pi/180));

bar(xout(2:end-1),n);

thr=theta_modif*pi/180;
phr=phi*pi/180;

[new_x,new_y,new_z]=sph2cart(phr,thr,ones(length(thr),1));

subplot(1,2,2);

yx=new_x.*(verts);
yy=new_y.*(verts);
yz=new_z.*(verts);

s_handle=trisurf(faces,yx,yy,yz,verts);
lighting phong;
camproj('perspective');
axis square; 
axis tight;
axis off;
axis equal;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
% set(s_handle, 'DiffuseStrength', 1);
% set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
cameramenu;
