	
cells{1}.on=4;
cells{1}.type='cir';
cells{1}.gaus=5;
cells{1}.minblebs=3;
cells{1}.maxblebs=3;
cells{1}.frames=40;
	
	
global RELATIV_ROOT
RELATIV_ROOT='../';


path(path, [RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);
path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');

% cells{1}.on=3;
% cells{1}.date='19-06-09';
% cells{1}.type='E64';
% cells{1}.time='1h30';
% cells{1}.number='00';
% cells{1}.frames=2;


switch cells{1}.on
case 4
		num=[];
		
		Folder =[ RELATIV_ROOT 'DATA/data_sph_simu' ];
		
	
		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/level_quest'];
		
		% 		figure;
		for j=1:length(cells)
			%retrieve the line numbe of each cell to analyze at every
			%frame
			if cells{j}.gaus==1
				type='simu_sigma';
			else
				type='simu';
			end
			if ~isfield(cells{j},'frames')
				[cells{j}.minblebs,cells{j}.maxblebs,cells{j}.frames]=compute_simulated_signals(cells{j},...
					cells{1}.freesurfer,fast,length(cells),j,varargin{1},varargin{2},varargin{3},varargin{4});

			end
		end
		%at the end (cause the list is dynamic), finding index in the folder:
		for j=1:length(cells)
			if cells{j}.maxblebs==0
				%to avoid problem with cells not having bleb enough in
				%comparison with gaussian number
				continue;
			end
			num=[num getNum(cells{j},Folder,type)];
		end
	case 3
		num=[];
		
		Folder =[ RELATIV_ROOT 'DATA/data_sph_freesurfer/results' ];

		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/level_quest' ];
		
		for j=1:length(cells)
			
			for jj=1:length(cells{j}.frames)
				currentName=cell_naming(cells{j},cells{j}.frames(jj),'');
				if ~exist([Folder '/' currentName(1:end-4) '.mat'],'file')
					freesurferParam(currentName(1:end-4));
				end
			end
			
		end
		
		for j=1:length(cells)
			%retrieve the line numbe of each cell to analyze at every frame
			num=[num getNum(cells{j},Folder,'')];
		end	
end

names=dir2cell(Folder);
% wavelet coefficients calculation
currentName=names{num};

BW = 25;
max_invertibility = 25; %Degree above which we create a high pass filter.
% kernel_sizes = [0.004*(2^7) 0.004*(2^6) 0.004*(2^5) 0.004*(2^4) 0.004*(2^3) 0.004*(2^2)]; % number of filters will be equal to kernel_sizes + 1.
% % % % % kernel_sizes = [0.1920 0.2240 0.004*(2^5)]; % number of filters will be equal to kernel_sizes + 1.
% kernel_sizes =[0.006*(2^7) 0.004*(2^6) 0.16 0.004*(2^4) 0.004*(2^3) 0.004*(2^2)]; % number of filters will be equal to kernel_sizes + 1.
% kernel_sizes = 0.26:-0.005:0.09;
kernel_sizes= [0.512 , 0.2560, 0.24:-0.01:0.02];
sigma=1;


check_bool=0;
kernel1=num2str(kernel_sizes(1));
kernelend=num2str(kernel_sizes(end));

if ~exist([RELATIV_ROOT 'results/axisymmetric_filter/LoG/LaplacianBW' num2str(BW) 'inv'...
		num2str(max_invertibility) 'kernel' kernel1(3:end) 'to' ...
		kernelend(3:end) 'sigma' num2str(sigma) '.mat'],'file')
	
	%create filters if not already existing
	CreateLaplacianAnalysisSynthesisFilters(1,BW,max_invertibility,kernel_sizes,sigma);
end

%load the filters used for the wavelet transform
load([RELATIV_ROOT 'results/axisymmetric_filter/LoG/LaplacianBW' num2str(BW) 'inv'...
		num2str(max_invertibility) 'kernel' kernel1(3:end) 'to' ...
		kernelend(3:end) 'sigma' num2str(sigma) '.mat']);

%loading surface data (actual mesh and spherical mesh)
name =  names{num};
disp(name);
load([Folder,'/',name]);

disp('Performing Wavelet Decomposition');

vertices=vertices-repmat(mean(vertices,1),length(vertices),1);

%computing pretreatments
vertexFaces =  MARS_convertFaces2FacesOfVert(int32(faces), int32(size(sph_verts, 1)));
num_per_vertex = length(vertexFaces)/size(sph_verts,1);
vertexFaces = reshape(vertexFaces, size(sph_verts,1), num_per_vertex);

vertexNbors = MARS_convertFaces2VertNbors(int32(faces), int32(size(sph_verts,1)));
num_per_vertex = length(vertexNbors)/size(sph_verts,1);
vertexNbors = reshape(vertexNbors, size(sph_verts,1), num_per_vertex);

sbjMesh.vertices=sph_verts';
sbjMesh.metricVerts=vertices';
sbjMesh.faces=faces';
sbjMesh.vertexFaces=vertexFaces';
sbjMesh.vertexNbors=vertexNbors';

if exist('fvec','var')
	sbjMesh.fvec=fvec;
end

%calculation of coefficients
[recon_cell, analysis_cell] = PartiallyReconMeshModif(sbjMesh, analysis_filters_my, synthesis_filters_my, check_bool,length(kernel_sizes)-1,0,1,7);

%saving files
save([Folder_final '/amoeba_' currentName],'recon_cell','analysis_cell');


bleb_detection_grid(num,1:length(kernel_sizes)-1,cells,BW,max_invertibility,...
				kernel_sizes,sigma);