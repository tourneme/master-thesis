close all

path(path,'../Toolboxes and functions/soho/utility/');
base_path='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
path(path, base_path);
path(path, [base_path 'toolbox/']);
path(path, [base_path 'gim/']);
path(path, [base_path 'data/']);
path(path, [base_path 'funcRegMeshes/']);
path(path, '../reusable_code');

% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;
J=6;


[vertex,face] = compute_semiregular_sphere(J,options);
posInitmesh=vertex{4};

% f = perform_spherical_interpolation2(hi,posInit,fI,center);
load('/Users/cducroz/Documents/SPHARM MAT/SPHARM-MAT-v1-0-0/data/parameterization/mesh02_smo /Amoebas treated/06012010_Wt-1H_smo/06012010_Wt-1H_#0009_smo/06012010_Wt-1H_#0009_T0030_CALD_smo.mat');

posInitobj=sph_verts';

figure;
subplot(1,2,1);plot_mesh(vertices,faces,options); shading faceted; axis tight;

subplot(1,2,2);plot_mesh(sph_verts,faces,options); shading faceted; axis tight;

uv_mesh = mapPointFromSphereToTex( posInitmesh);

uv_obj = mapPointFromSphereToTex( posInitobj);


figure;
plot(uv_mesh(1,:),uv_mesh(2,:),'*r');
hold on;
% plot(uv_obj(1,:),uv_obj(2,:),'*b');

