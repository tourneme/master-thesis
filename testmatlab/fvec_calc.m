global RELATIV_ROOT
RELATIV_ROOT='../';


%la moiti? des path ne sert ? rien
path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');
path(path, [RELATIV_ROOT 'reusable_code']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/FileRename_29Nov2010']);

%Data ? analyser  ---------------------------------

DATA='/Users/cducroz/Documents/Robin/CODE MATLAB/DATA/data_sph';

%-------------------------------------

%Dossier de sortie --------------------------------

OUTDIR='/Users/cducroz/Documents/Robin/CODE MATLAB/testmatlab/FVEC';

%------------------------------------

%cellules ? fvecter ------------------

% cells=cell_choice;
cells=COI;

% ----------------------------------


for i=1:length(cells)

	act_cell=cells{i};
	
	for j=1:length(act_cell.frames)
		
		currentName=cell_naming(act_cell,act_cell.frames(j),'');

		inFinal=[DATA '/' currentName];

		
		% 	SPHARM extraction following Shen method
		%%
		%Spherical harmonic calculation on the BW.
		BW=20;
		
		confs.MaxSPHARMDegree = BW;
		confs.OutDirectory = OUTDIR;
		
		inFinalCell=cell(1);
		inFinalCell{1}=inFinal;
		
		outDes = SpharmMatExpansion(confs, inFinalCell, 'ExpLSF');
		
		newoutDes=[OUTDIR '/' currentName];
		FileRename(outDes{1}, newoutDes);
	end
end