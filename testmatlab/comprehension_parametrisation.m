close all;

version='3';

Path_init='/Users/cducroz/Documents/Robin/CODE MATLAB/results/parameterization/Brechbuhler + CALD/with_Critel_SPHARM_MAT/initParamCALD';

Path_cald='/Users/cducroz/Documents/Robin/CODE MATLAB/results/parameterization/Brechbuhler + CALD/with_Critel_SPHARM_MAT';



Path_cald_my_data='/Users/cducroz/Documents/Robin/CODE MATLAB/DATA/data_sph';

file_new_naming='06-01-10_WT__0h30_#00_T02.mat';

file_old_naming='06-01-10_wt-30__#0000_T0002_CALD';

figure;

load([Path_init '/' file_old_naming '_ini.mat']);

%%

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

verts=sqrt(sum(vertices.^2,2));
norm_ini_x=sph_verts(:,1).*(verts);
norm_ini_y=sph_verts(:,2).*(verts);
norm_ini_z=sph_verts(:,3).*(verts);

h1=subplot(2,3,1);
s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);

h4=subplot(2,3,4);
s_handle=trisurf(faces,norm_ini_x,norm_ini_y,norm_ini_z);
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);

vert_ini=vertices;
sph_verts_ini=sph_verts;

clear verts sph_verts vertices mean_vert faces


disp([Path_cald '/' file_old_naming '_smo_' version '.mat']);


% load([Path_cald '/' file_old_naming '_smo_' version '.mat']);


% load([Path_cald '/06-01-10_WT__0h30_#00_CALD_smo_' version '.mat']);

load([Path_cald '/06-01-10_wt-30__#0000_T0002_CALD_smo_3.mat']);

%%

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

verts=sqrt(sum(vertices.^2,2));
norm_cald_x=sph_verts(:,1).*(verts);
norm_cald_y=sph_verts(:,2).*(verts);
norm_cald_z=sph_verts(:,3).*(verts);

h2=subplot(2,3,2);
s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);

h5=subplot(2,3,5);
s_handle=trisurf(faces,norm_cald_x,norm_cald_y,norm_cald_z);
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);


hlink = linkprop([h4 h5 h1 h2],{'CameraPosition','CameraUpVector'});
key = 'graphics_linkprop';
% Store link object on first subplot axes
setappdata(h4,key,hlink);

cameramenu;

vert_cald=vertices;
sph_verts_cald=sph_verts;


clear verts sph_verts vertices mean_vert faces

load([Path_cald_my_data '/' file_new_naming]);


%%

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

verts=sqrt(sum(vertices.^2,2));
norm_data_x=sph_verts(:,1).*(verts);
norm_data_y=sph_verts(:,2).*(verts);
norm_data_z=sph_verts(:,3).*(verts);

h3=subplot(2,3,3);
s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);

h6=subplot(2,3,6);
s_handle=trisurf(faces,norm_data_x,norm_data_y,norm_data_z);
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);


hlink = linkprop([h4 h5 h6 h1 h2 h3],{'CameraPosition','CameraUpVector'});
key = 'graphics_linkprop';
% Store link object on first subplot axes
setappdata(h4,key,hlink);

cameramenu;

vert_data=vertices;
sph_verts_data=sph_verts;


diff1=norm_ini_x-norm_cald_x;
diff2=norm_ini_y-norm_cald_y;
diff3=norm_ini_z-norm_cald_z;

diffdata1=norm_ini_x-norm_data_x;
diffdata2=norm_ini_y-norm_data_y;
diffdata3=norm_ini_z-norm_data_z;

disp(['ce que j ai: ' num2str(mean(diff1)) ' - '  num2str(mean(diff2)) ' - '  num2str(mean(diff3))]);
disp(['ce que je veux: ' num2str(mean(diffdata1)) ' - '  num2str(mean(diffdata2)) ' - '  num2str(mean(diffdata3))]);


% figure;
% 
% 
% g1=subplot(1,3,1);
% s_handle=trisurf(faces,vert_ini(:,1),vert_ini(:,2),vert_ini(:,3));
% lighting phong;
% camproj('perspective');
% axis square;
% axis tight;
% axis off;
% axis equal;
% %     shading interp;
% view(90, 0);
% camlight('headlight', 'infinite');
% view(270, 0);
% camlight('headlight', 'infinite');
% set(s_handle, 'DiffuseStrength', 1);
% set(s_handle, 'SpecularStrength', 0);
% view(10,-40);
% 
% g2=subplot(1,3,2);
% s_handle=trisurf(faces,vert_cald(:,1),vert_cald(:,2),vert_cald(:,3));
% lighting phong;
% camproj('perspective');
% axis square;
% axis tight;
% axis off;
% axis equal;
% %     shading interp;
% view(90, 0);
% camlight('headlight', 'infinite');
% view(270, 0);
% camlight('headlight', 'infinite');
% set(s_handle, 'DiffuseStrength', 1);
% set(s_handle, 'SpecularStrength', 0);
% view(10,-40);
% 
% g3=subplot(1,3,3);
% s_handle=trisurf(faces,vert_data(:,1),vert_cald(:,2),vert_cald(:,3));
% lighting phong;
% camproj('perspective');
% axis square;
% axis tight;
% axis off;
% axis equal;
% %     shading interp;
% view(90, 0);
% camlight('headlight', 'infinite');
% view(270, 0);
% camlight('headlight', 'infinite');
% set(s_handle, 'DiffuseStrength', 1);
% set(s_handle, 'SpecularStrength', 0);
% view(10,-40);
% 
% cameramenu;
% 
% hlink = linkprop([g1 g2 g3],{'CameraPosition','CameraUpVector'});
% key = 'graphics_linkprop';
% % Store link object on first subplot axes
% setappdata(g1,key,hlink);

figure;

f1=subplot(1,3,1);
s_handle=trisurf(faces,sph_verts_ini(:,1),sph_verts_ini(:,2),sph_verts_ini(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(2);


f2=subplot(1,3,2);
s_handle=trisurf(faces,sph_verts_cald(:,1),sph_verts_cald(:,2),sph_verts_cald(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(2);


f3=subplot(1,3,3);
s_handle=trisurf(faces,sph_verts_data(:,1),sph_verts_data(:,2),sph_verts_data(:,3));
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
%     shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(2);

hlink = linkprop([f1 f2 f3],{'CameraPosition','CameraUpVector'});
key = 'graphics_linkprop';
% Store link object on first subplot axes
setappdata(f1,key,hlink);

cameramenu