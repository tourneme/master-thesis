function bleb_detection_grid(num,levels,cells,varargin)
% This function makes the bleb detection: once the wavelet analysis is made
% and the wavelet coefficient found, they still has to be represented, and
% thresholded. This is the aim of this function.
%
%INPUT:
%   -levels: maximum scale you want to analyze: if you have three, you will
%   plot three graphs representing each the bleb detection according to 
%   each scale.
%   -cells: array of cell (matlab type: you will have to take care of the 
%   ambiguity that can rise from the fact that in our experiment a cell is
%   at the same type our subject of interest and a basic matlab type). This
%   array of cell has to contain only one component (because only 6 frames
%   of one amoeba can be analyzed). This cell has to follow the norm
%   defined in the file cell_choice.m notably
%   -varargin: variable only here to get the folder containing the wavelet coefficients and to plot the spherical
%   wavelets.
%
%OUTPUT:
%   -the outputs are only graphical
%
%
%modifications list:
%
%-Robin Tournemenne 26/07/2013 "creation"

global RELATIV_ROOT

%%%%%%%%%%%%%%%%%%    DATA preperation    %%%%%%%%%%%%%%%%%%%%
%%
BW=varargin{1};
max_invertibility=varargin{2};
kernel=varargin{3};
kernel1=num2str(kernel(1));
kernelend=num2str(kernel(end));
sigma=varargin{4};
			
Folder_data_pre= [RELATIV_ROOT 'results/overcomplete_coef/LoG/BW' ...
				num2str(BW) 'max_invert'  num2str(max_invertibility) ...
				'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
				'sigma' num2str(sigma)];

for k=1:length(cells)

	switch cells{1}.on
		case 1
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph' ];	
			Folder_data=[Folder_data_pre '/CALD'];
		case 2
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph_gu/results' ];
			Folder_data=[Folder_data_pre '/gu'];	
		case 3
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph_freesurfer/results' ];
			Folder_data=[Folder_data_pre '/freesurfer'];		
		case 4
			
			Folder_names = [RELATIV_ROOT 'DATA/data_sph_simu'];
			Folder_data= [RELATIV_ROOT 'results/level_quest'];
	end
	%getting the line number of the frame to analyze. num_name reaches the
	%spherical parameterization
	if cells{1}.on==4
		%getting the line number of the frame to analyze. num_name reaches the
		%spherical parameterization
		num_name=getNum(cells{k},Folder_names,'simu');
		type='simu_amoeba_';
	else
		num_name=getNum(cells{k},Folder_names,'');
		type='amoeba_';
	end
	%num_data reaches the wavelet coefficients
	num_data=getNum(cells{k},Folder_data,type);
	
	names=dir2cell(Folder_names);
	names_data=dir2cell(Folder_data);
	
	
	%The following part plot the sequence of frame you want to analyze: on the
	%first line, the actual frames of the cell, on the second line the
	%spherical parameterization of the cell where each point of the mesh
	%doesn't receive 1 has radius but the norm of the same point on the actual
	%cell. One can interpret this result as the convexified version of the
	%actual cell. This second line is very usefull to understand why some part
	%of the cell can be detected as bleb. Indeed, this the second line that will
	% be projected on the wavelet basis. Consequently, if you spherical
	% parameterization of the cell induces distorsion of, as instance, a flat
	% surface into little bump, the flat surface will be detected as a bleb
	%(which is, as you already understood, very bad).
	%%
	
	
% 	figure;
% 	h=zeros(1,2*length(num_data));
	xx=0;
	for j=num_name
		xx=xx+1;
		if xx<7
			x=xx;
		elseif xx<13
			if xx==7
				figure;
			end
			x=xx-6;
		elseif xx<19
			if xx==13
				figure;
			end
			x=xx-12;
		elseif xx<19
			if xx==19
				figure;
			end
			x=xx-18;
		end
		
		%loading the cell and its spherical parameterization
		load([Folder_names,'/',names{j}]);
		
		%we center the cell in order to get relevant norm.
		mean_vert=mean(vertices,1);
		vertices(:,1)=vertices(:,1)-mean_vert(1);
		vertices(:,2)=vertices(:,2)-mean_vert(2);
		vertices(:,3)=vertices(:,3)-mean_vert(3);
		
		%we calculate the coordinate of each point from the spherical
		%parameterization with the associate norm
		verts=sqrt(sum(vertices.^2,2));
		sph_param_norm_x=sph_verts(:,1).*(verts);
		sph_param_norm_y=sph_verts(:,2).*(verts);
		sph_param_norm_z=sph_verts(:,3).*(verts);
		
		%we calculate the coordinate of each point from the spherical
		%parameterization with the associate norm
% 		nrm=max([sph_param_norm_x;sph_param_norm_y;sph_param_norm_z]);
		nrm=10;
		sph_param_filter_x=sph_verts(:,1).*(1+0.1*(verts))*nrm;
		sph_param_filter_y=sph_verts(:,2).*(1+0.1*(verts))*nrm;
		sph_param_filter_z=sph_verts(:,3).*(1+0.1*(verts))*nrm;
		sph_param_filter=[sph_param_filter_x,sph_param_filter_y,sph_param_filter_z];
		nrm=min(sqrt(sum(sph_param_filter.^2,2)))/100;
		sph_underneath_x=sph_verts(:,1).*nrm;
		sph_underneath_y=sph_verts(:,2).*nrm;
		sph_underneath_z=sph_verts(:,3).*nrm;
		
		
		%this big block to plot the actual cell shape (colors gives you
		%information about how much a point is far from the center (definition
		%of a norm...)
% 		h(xx)=subplot(3,6,x);
% 		
% 		if cells{1}.on==4 && cells{1}.maxblebs>2
% 			load([Folder_names '/zones/' names{j}]);
% 			plotSimCell(faces,vertices,radius,cells{1}.gaus,zones);
% 			view(0, 0);
% 			zoom(2.4);
% 		else
% 			s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),verts);
% 			lighting phong;
% 			camproj('perspective');
% 			axis square;
% 			axis tight;
% 			axis off;
% 			axis equal;
% 			view(90, 0);
% 			camlight('headlight', 'infinite');
% 			view(270, 0);
% 			camlight('headlight', 'infinite');
% 			set(s_handle, 'DiffuseStrength', 1);
% 			set(s_handle, 'SpecularStrength', 0);
% 			view(10,-40);
% 			zoom(1.4);
% 			%this very tiny line (cameramenu) gives you a perfect tool to make the
% 			%cell move, unfortunately, you will have to type it again after zooming
% 			%on the cell...
% 			cameramenu;
% 		end
% 		
% 		
% 		
% 		%this big block plot the convexified cell shape
% 		h(xx+6)=subplot(3,6,x+6);
% 		s_handle=trisurf(faces,sph_param_norm_x,sph_param_norm_y,sph_param_norm_z,verts);
% 		lighting phong;
% 		camproj('perspective');
% 		axis square;
% 		axis tight;
% 		axis off;
% 		axis equal;
% 		view(90, 0);
% 		camlight('headlight', 'infinite');
% 		view(270, 0);
% 		camlight('headlight', 'infinite');
% 		set(s_handle, 'DiffuseStrength', 1);
% 		set(s_handle, 'SpecularStrength', 0);
% 		view(10,-40);
% 		zoom(1.4);
% 		cameramenu;
% 		
% 		
% 		
% 		%another spherical representation to see what the filter convolutes
% 		h(xx+12)=subplot(3,6,x+12);
% 		s_handle=trisurf(faces,sph_param_filter_x,sph_param_filter_y,sph_param_filter_z,verts);
% 		alpha(s_handle,0.5)
% 		hold on
% 		trisurf(faces,sph_underneath_x,sph_underneath_y,sph_underneath_z,min(verts)*ones(length(verts),1));
% 		lighting phong;
% 		camproj('perspective');
% 		axis square;
% 		axis tight;
% 		axis off;
% 		axis equal;
% 		view(90, 0);
% 		camlight('headlight', 'infinite');
% 		view(270, 0);
% 		camlight('headlight', 'infinite');
% 		set(s_handle, 'DiffuseStrength', 1);
% 		set(s_handle, 'SpecularStrength', 0);
% 		view(10,-40);
% 		cameramenu;
% 		
% 		if cells{1}.on~=4
% 			%the following lines are used to make every cell move together.
% 			hlink = linkprop([h(xx+6),h(xx+12)],{'CameraPosition','CameraUpVector'});
% 			key = 'graphics_linkprop';
% 			% Store link object on first subplot axes
% 			setappdata(h(xx+6),key,hlink);
% 			%command to get the moving tool for 3D surfaces
% 		end
% 		
% 	end
% 
% 	
% 	%used in order to save graphs in the folder \rec_graph
% 	clk=clock;
% 	
% 	
% 	% saving graph
% 	cells_name=cells_naming(names,num(1),num(end));
% 	
% 	suplabel(cells_name,'t');
% 	
% 	saveas(gcf,[ RELATIV_ROOT 'rec_graph/sequence  ' cells_name '   recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4)) 'h' num2str(clk(5)) '.fig']);

% 	%%
% 	
% 	% %The following plot the wavelet basis function in order to understand even
% 	% %more why parts of the cells have been detected as blebs.
% 	figure;
% 	disp('Creating Analysis Filters');
% 	for j=levels
% 	    create_SphWavelets(BW,kernel,j,1);
% 	end
% 	
% 	% %the last part is undertaking the visualization of the wavelet coefficients
% 	% %at each scale. Each graph corresponds to a particular scale (the coarser,
% 	% %the closer to 1). The first line is the representation of a cell at the
% 	% %given scale (+ the representation of the coarser scale (normally obvious
% 	% %but I have to check that). The second line gives the result of the
% 	% %thresholding method, dark blue means that the coefficients are not kept,
% 	% %any other color represents the value of wavelet coefficient in a region
% 	% %detected as bleb.
	%%
	
	figure;
	h=zeros(length(levels),1);
	for i=levels
		
		
		x=0;
		
		%get the maximum to have true color
		tmax=zeros(length(num_data),1);
		for j=num_data
			x=x+1;
			name =  names_data{j};
			load([Folder_data,'/',name]);
			a=analysis_cell{i};
			%wavelet coefficient kept in the vertices vector of the
			%analysis_cell{i} structure
			tmax(x)=max(sqrt(sum(a.vertices.^2,1)));
			
		end
		%     %tmax define the biggest value of wavelet coefficients in order to get
		%     %a unity between different frames following each other.
		tmax=max(tmax);
		xx=0;
		for j=num_data
			xx=xx+1;
			if xx<7
				x=xx;
			elseif xx<13
				if xx==7
					figure;
				end
				x=xx-6;
			elseif xx<19
				if xx==13
					figure;
				end
				x=xx-12;
			elseif xx<19
				if xx==19
					figure;
				end
				x=xx-18;
			end
			
			
			name =  names_data{j};
			disp(name);
			load([Folder_data,'/',name]);
			a=analysis_cell{i};
			b=recon_cell{i};
			t=sqrt(sum(a.vertices.^2,1));
			
			%little trick to have a color depending on the sequence
			%and not only one picture.
			t(1)=tmax;
			
			
			%plotting the wavelet coefficient at the scale j. The color
			%represents the value of the wavelet coefficient
% 			h(2*xx-1)=subplot(2,6,x);
% subplot(5,5,i)
% 			s_handle=trisurf(b.faces',b.vertices(1,:)',b.vertices(2,:)',b.vertices(3,:)',t);
% 			lighting phong;
% 			camproj('perspective');
% 			axis square;
% 			axis tight;
% 			axis off;
% 			axis equal;
% 			shading interp;
% 			view(90, 0);
% 			camlight('headlight', 'infinite');
% 			view(270, 0);
% 			camlight('headlight', 'infinite');
% 			set(s_handle, 'DiffuseStrength', 1);
% 			set(s_handle, 'SpecularStrength', 0);
% 			view(10,-40);
% 			zoom(1.4);
			
			%applying a certain thresholding method
			fw_final=thresholding(t,i,2);
			disp(num2str(max(t)));
			
% 			h(2*xx)=subplot(2,6,x+6);
h(i)=subplot(6,6,i);
			% counting blebs
			[bleb_number,bleb_list]=bleb_counting_over(fw_final,b.faces,1);
			
			ref_numb=str2num(name(17:18));
			
			
			% plotting the threshold cell
			
			s_handle=trisurf(b.faces',b.vertices(1,:)',b.vertices(2,:)',b.vertices(3,:)',bleb_list);
			xlabel(['Bleb number: ' num2str(bleb_number)-1]);
			lighting phong;
			camproj('perspective');
			axis square;
			axis tight;
			axis off;
			axis equal;
			shading interp;
			view(90, 0);
			camlight('headlight', 'infinite');
			view(270, 0);
			camlight('headlight', 'infinite');
			set(s_handle, 'DiffuseStrength', 1);
			set(s_handle, 'SpecularStrength', 0);
			view(10,-40);
			zoom(1);
			cameramenu;

			
		end
		


		
		
		%it is time to save the resulting graphs


	end
	% 		linking(h,num_data);

			%the following lines are used to make every cell move together.
			hlink = linkprop(h,{'CameraPosition','CameraUpVector'});
			key = 'graphics_linkprop';
			% Store link object on first subplot axes
			setappdata(h(levels(end)),key,hlink);
			%command to get the moving tool for 3D surfaces
	
end
end