clear all;
base_path='../Toolboxes and functions/toolbox_wavelet_meshes/';    

path(path, '../lazy lifted/');

options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;

Folder = [base_path 'ResultsWaveSph'];
d = dir(Folder); 


name =  d(20).name ;
 
load([Folder,'/',name]);

Out_vtx=Outvtx(vertex,face,5);

f=f1;
figure;
plot_mesh(f,face{4+1},options); shading faceted; axis tight;
hold on;
plot3( f(Out_vtx,1),f(Out_vtx,2),f(Out_vtx,3),'*','Color','r','lineWidth',3);