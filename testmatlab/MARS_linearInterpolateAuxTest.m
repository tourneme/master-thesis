function [vals, NViF, NF] = MARS_linearInterpolateAuxTest(points,...
	vertices, faces, vertexNbors,vertexFaces, nearest_vertex_guess, data)


MAX_DISTANCE_SQ=100000;
MIN_TOL=0.00005;


% The main routine.  
int nlhs,
mxArray *plhs[],
int nrhs,
const mxArray *prhs[])


%  int64 t1 = msTime();
%  int64 t2,t3,t4,t5,t6;*/
%     
%  
%  points = 3 x numOfPoints (single precision)
%  vertices = 3 x numOfVertices (single)
%  faces = 3 x numOfTriangles (int32)
%  vertexNbors = maxNumOfNbors x numOfVertices (int32)
%  vertexFaces = maxNumOfFaces x numOfVertices (int32)
%  data = d x numOfVertices (single);
%  seedVertices = 1 x numOfVertices (int32)  
  
    
%     const float *	points   		=	(float*) mxGetPr(prhs[0]);
% 	const float *   vertices		=	(float*) mxGetPr(prhs[1]);
%     const int   *   faces           =   (int*)  mxGetPr(prhs[2]);
% 	const int	*	vertexNbors	    =   (int*)  mxGetPr(prhs[3]);
%     const int   *   vertexFaces     =   (int*)  mxGetPr(prhs[4]);
%     const int   *   seedVertices    =   (int*)  mxGetPr(prhs[5]);
%     const float *   data            = (float*)  mxGetPr(prhs[6]);
    
numOfNbors      =  length(vertexNbors);
numOfVertFaces  =  length(vertexFaces);
numOfVertices   =   length(vertices);
numOfFaces      =   length(faces);
numOfPoints     =   length(points);
data_dim        =   length(data);

%     int out_dims[2], p, grad_dims[3];
%     int index_dims[2];
%     int * FaceInds_MatlabStyle, * NViF_MatlabStyle;
%     int cur_face_ind;
%     
%     const float * v0;
%     const float * v1;
%     const float * v2;
%     const float * d0;
%     const float * d1;
%     const float * d2;
%     const float * cur_point;
%     float * new_data;
%     float * grad;
    
out_dims(1) = data_dim;
out_dims(2) = numOfPoints;
grad_dims(1) = data_dim;
grad_dims(2) = 3;
grad_dims(3) = numOfPoints;
index_dims(1) = 1;
index_dims(2) = numOfPoints;



printf('NumOfPoints : %i, Num of Faces: %i, Num of Vertices: %i, Num of Max Nbors: %i, Num of Max Vert Faces: %i\n',...
	numOfPoints, numOfFaces, numOfVertices, numOfNbors, numOfVertFaces);



if (numOfPoints ~= 3)
	mexErrMsgTxt('Point should be three dimensional!');
end

if (numOfVertices ~= numOfNbors && numOfVertices ~= numOfVertFaces)
	mexErrMsgTxt('Inconsistent number of vertices!');
end

if (numOfPoints ~= length(seedVertices))
	mexErrMsgTxt('Numof points and number of seedVertices should be the same!');
end

if (data_dim ~= numOfVertices)
	mexErrMsgTxt('Data not the right size!');
end

%      First find faces

    
%     if(!(FaceInds_MatlabStyle = (int*) calloc(numOfPoints, sizeof(int))))
%         mxErrMsgTxt("Memory allocation error!!!");
    
		
plhs[2] = mxCreateNumericArray(2, index_dims, mxINT32_CLASS, mxREAL);
FaceInds_MatlabStyle = (int *)mxGetData(plhs[2]);


plhs[1] = mxCreateNumericArray(2, index_dims, mxINT32_CLASS, mxREAL);
NViF_MatlabStyle = (int*) mxGetData(plhs[1]);
    
%     /*t2 = msTime();*/
MARS_findFaces(points, numOfPoints, vertices, numOfVertices, faces, numOfFaces,...
	vertexNbors, numOfNbors, vertexFaces, numOfVertFaces, seedVertices,...
	FaceInds_MatlabStyle, NViF_MatlabStyle);
%     /*t3 = msTime();*/
    
    plhs[0] = mxCreateNumericArray(2, out_dims, mxSINGLE_CLASS, mxREAL);    
    new_data = (float *) mxGetData(plhs[0]);
    
    
    
%     /*t4 = msTime(); */
    for p=0:numOfPoints-1
    
%         /*
%          *Convert to C-style
%          */       
        
        cur_point = &points[3 * p];
        cur_face_ind = FaceInds_MatlabStyle[p] - 1;
        v0 = &vertices[3*(faces[3*cur_face_ind] - 1)];
        v1 = &vertices[3*(faces[3*cur_face_ind + 1] - 1)];
        v2 = &vertices[3*(faces[3*cur_face_ind + 2] - 1)];
        
        d0 = &data[data_dim *(faces[3*cur_face_ind] - 1)];
        d1 = &data[data_dim *(faces[3*cur_face_ind + 1] - 1)];
        d2 = &data[data_dim *(faces[3*cur_face_ind + 2] - 1)];
        
%         #ifdef DEBUG
%         printf("Interpolating at point: %f %f %f\n", cur_point[0], cur_point[1], cur_point[2]);
%         printf("v0: %f %f %f\n", v0[0], v0[1], v0[2]);
%         printf("v1: %f %f %f\n", v1[0], v1[1], v1[2]);
%         printf("v2: %f %f %f\n", v2[0], v2[1], v2[2]);
%         printf("Data:\n");
%         printf("d0: %f %f %f\n", d0[0], d0[1], d0[2]);
%         printf("d1: %f %f %f\n", d1[0], d1[1], d1[2]);
%         printf("d2: %f %f %f\n", d2[0], d2[1], d2[2]);
%         #endif
        linearInterp(cur_point, v0, v1, v2, d0, d1, d2, data_dim, &(new_data[data_dim * p]));   
        
	end 
%     t5 = msTime();
%       free(FaceInds_MatlabStyle);
%     
%     fprintf(stderr, "Total Time = %f, FindFaces = %f, interp = %f\n", (t5 - t1)/1000.0, (t3 - t2)/1000.0, (t5 - t4)/1000.0);*/

	end