// Generate a synthetic 3D image sequence simulating moving cells 
// viewed with fluorescence microscopy

clear all,

//write_files = 'y';

// Image size
Nx = 100;
Ny = Nx;
Nz = 40;

// Voxel aspect ratio
dzdx = Nx/Nz;

// Number of frames
Nframes = 1;

// Number of cells
Ncells = 0;

// Range of radii of the main disk
Rmin = 30;
Rmax = 35;
R0 = Rmin+rand()*(Rmax-Rmin);
R1 = Rmin+rand()*(Rmax-Rmin);

// Maximum displacement of cell center (center of the main disk)
drmax = Rmin;

// Noise parameters
noise_mean = 0;

// Create output directory
//cd ~/tmp/simdata;
//mkdir('original');
//mkdir('gauss_0.1');
//mkdir('gauss_0.5');
//mkdir('gauss_1.0');
//aux = datestr(now,30);
//outputdir = [aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];

//fig_montage = figure;
//fig_3d = figure;
for fr=1:Nframes
	Ibw = zeros(Nx,Ny,Nz);
    cellsOK = 0;
    
    while cellsOK == 0
    
        // FIRST CELL

        // position of the main sphere center
        phi = rand*pi - pi/2;
        theta = rand*2*pi;
        if ( fr == 1 ) // first time
            rho = R0 + rand*R0/3;
            tmpxc = Nx/2 + rho*cos(phi)*cos(theta);
            tmpyc = Ny/2 + rho*cos(phi)*sin(theta);
            tmpzc = Nz*dzdx/2 + rho*sin(phi);
        else
            rho = 5;
            tmpxc = xc + rho*cos(phi)*cos(theta);
            tmpyc = yc + rho*cos(phi)*sin(theta);
            tmpzc = zc + rho*sin(phi);
        end

        // SECOND CELL
        // position of the main sphere center
        phi2 = rand*pi - pi/2;
        theta2 = rand*2*pi;
        if ( fr == 1 ) // first time
            rho2 = R1 + rand*R1/3;
            tmpxc2 = Nx/2 + rho2*cos(phi2)*cos(theta2);
            tmpyc2 = Ny/2 + rho2*cos(phi2)*sin(theta2);
            tmpzc2 = Nz*dzdx/2 + rho2*sin(phi2);
        else
            rho2 = 5;
            tmpxc2 = xc2 + rho2*cos(phi2)*cos(theta2);
            tmpyc2 = yc2 + rho2*cos(phi2)*sin(theta2);
            tmpzc2 = zc2 + rho2*sin(phi2);
        end

        distance = sqrt( (tmpxc-tmpxc2)^2 + (tmpyc-tmpyc2)^2 + (tmpzc-tmpzc2)^2 );
        if ( distance < 1.5*(R0+R1) & distance > 0.9*(R0+R1) & tmpxc > R0 & tmpxc < Nx-R0 & tmpyc > R0 & tmpyc < Ny-R0 & tmpzc > R0 & tmpzc < Nz*dzdx-R0 & tmpxc2 > R1 & tmpxc2 < Nx-R1 & tmpyc2 > R1 & tmpyc2 < Ny-R1 & tmpzc2 > R1 & tmpzc2 < Nz*dzdx-R1 )
            xc = tmpxc;
            yc = tmpyc;
            zc = tmpzc;
            xc2 = tmpxc2;
            yc2 = tmpyc2;
            zc2 = tmpzc2;
            cellsOK = 1;
        end
    end
    
    // PSEUDOPODS
	Npseudopods = 2;
	for i=1:Npseudopods
		// radius of pseudopod
		Rp(i) = R0/3*rand;
		// center of pseudopod
		phip = rand*pi-pi/2;
		thetap = rand*2*pi;
		Raux = max(0,R0-dzdx) + rand*Rp(i);
		xp(i) = xc + Raux*cos(phip)*cos(thetap);
		yp(i) = yc + Raux*cos(phip)*sin(thetap);
		zp(i) = zc + Raux*sin(phip);
	end
	Npseudopods2 = 4;
	for i=1:Npseudopods2
		// radius of pseudopod
		Rp(i+Npseudopods) = R1/3*rand;
		// center of pseudopod
		phip2 = rand*pi-pi/2;
		thetap2 = rand*2*pi;
		Raux = max(0,R1-dzdx) + rand*Rp(i+Npseudopods);
		xp(i+Npseudopods) = xc2 + Raux*cos(phip2)*cos(thetap2);
		yp(i+Npseudopods) = yc2 + Raux*cos(phip2)*sin(thetap2);
		zp(i+Npseudopods) = zc2 + Raux*sin(phip2);
	end

	if Npseudopods>0 | Npseudopods2>0
		xs = [xc xc2 xp];
		ys = [yc yc2 yp];
		zs = [zc zc2 zp];
		Rs = [R0 R1 Rp];
	else
		xs = [xc xc2];
		ys = [yc yc2];
		zs = [zc zc2];
		Rs = [R0 R1];
	end

	// Walk through the different slices of the z-stack
	for iz = 1:Nz
		z = iz*dzdx;
		Ibwz = zeros(Nx,Ny);
		// Fill the disks corresponding to the intersections of each sphere
		// with the current slice
		angles = 0:0.01:2*pi;
		for i2 = 1:length(Rs)
			Raux = Rs(i2);
			xaux = xs(i2);
			yaux = ys(i2);
			zaux = zs(i2);
			aux = Raux^2-(z-zaux)^2;
			if aux>0
				Rdisk = sqrt(aux);
				xdisk = xaux + Rdisk*cos(angles);
				ydisk = yaux + Rdisk*sin(angles);
				Ibwz = max(Ibwz,roipoly(Ibwz,xdisk,ydisk));
			end
		end
		Ibw(:,:,iz) = Ibwz;
	end
	// Fill holes
	Ibw = imfill(Ibw,'holes');
	// Set object grey level
	Iobject = 128;
	Ibw = uint8(Ibw*Iobject);
	// Add noise
	Inoised_1 = imnoise(Ibw,'gaussian',0,0.1);
	Inoised_2 = imnoise(Ibw,'gaussian',0,0.5);
	Inoised_3 = imnoise(Ibw,'gaussian',0,1.0);

        // Compute SNR
        disp( std2(double(Inoised_1) - 128) );
        disp( std2(double(Inoised_2) - 128) );
        disp( std2(double(Inoised_3) - 128) );
    
	////// show current stack
	//figure(fig_montage); clf;
	//stack_bw = zeros(Nx,Ny,1,Nz);
	//stack_bw(:,:,1,:) = Ibw;
	//subplot(1,2,1), montage(stack_bw);
	//stack_I = stack_bw;
	//stack_I(:,:,1,:) = I;
	//subplot(1,2,2);montage(stack_I);
	////// Surface rendering of simulated cell 
	//figure(fig_3d); clf;
	//xx = (1:size(Ibw,2));
	//yy = (1:size(I,1));
	//zz = (1:size(I,3))*dzdx;
	//isosurface(xx,yy,zz,Ibw,0.5);
	//axis equal;
	//xlabel('x');
	//ylabel('y');
	//zlabel('z');
	//aux = [xx(1) xx(end) yy(1) yy(end) zz(1) zz(end)];
	//axis(aux); grid on;
	//////////////////////////////
	//title(['Frame #',num2str(fr),'/',num2str(Nframes)]);
	//disp(['Generated frame  #',num2str(fr),'/',num2str(Nframes)]);
	if write_files =='y'
		for iz = 1:Nz
			// Write grey-level image
			//outputfilename = ['gauss_0.1/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];//
			//imwrite(Inoised_1(:,:,iz),outputfilename,'tif','Compression','none');
			//disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
			// Write grey-level image
			//outputfilename = ['gauss_0.5/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];//
			//imwrite(Inoised_2(:,:,iz),outputfilename,'tif','Compression','none');
			//disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
			// Write binary image
			outputfilename= ['original/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];
			imwrite(Ibw(:,:,iz),outputfilename,'tif','Compression','none');
			disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
		end
    end
	clear Inoised_1 Inoised_2 Ibw Ibwz xp yp zp Rp;
end

disp('saving workspace ...');
save('simulated_cell_sequence');

disp('All finished');





