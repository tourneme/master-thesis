% Generate synthetic 3D images simulating spherical cells
% in fluorescence microscopy

clear all,close all;

write_files = 'y';
show_middle_slice = 'y';

% Digital image size
Nx = 300;
Ny = 300;
Nz = 40;

% Voxels size (microns)
dx = 0.327;    % microns (0.0645)
dy = 0.327;
dz = 0.98;

% Real image size
rNx = 300;
rNy = 300;
rNz = 120;

% Voxel aspect ratio
dzdx = 3;

% Range of radii of the main disk
Rmin = 10;
Rmax = 50;

% Create output directory
%cd /home/adufour/MAI2005/Matlab/simdata;
cd 'Z:\cells';
mkdir('original');
mkdir('poisson');

for nbstacks=1:5

    disp(['generating stack ',num2str(nbstacks),' / 5']);
    Ibw = zeros(rNx,rNy,rNz);
    cellsOK = 0;

    while cellsOK == 0

        % FIRST CELL
        % position of the main sphere center
        R1x = Rmin + rand * (Rmax - Rmin);
        R1y = Rmin + rand * (Rmax - Rmin);
        R1max = max(R1x,R1y) + 5;
        xc1 = R1max + ( rand * (rNx - 2 * R1max) );
        yc1 = R1max + ( rand * (rNy - 2 * R1max) );
        zc1 = R1max + ( rand * (rNz - 2 * R1max) );

        % SECOND CELL
        % position of the main sphere center
        R2x = Rmin + rand * (Rmax - Rmin);
        R2y = Rmin + rand * (Rmax - Rmin);
        R2max = max(R2x,R2y) + 5;
        xc2 = R2max + ( rand * (rNx - 2 * R2max) );
        yc2 = R2max + ( rand * (rNy - 2 * R2max) );
        zc2 = R2max + ( rand * (rNz - 2 * R2max) );

        distance = sqrt( (xc1 - xc2)^2 + (yc1 - yc2)^2 + (zc1 - zc2)^2 );

        if ( distance > 1.1 * (R1max + R2max) )
            cellsOK = 1;
        end

    end

    xs = [xc1 xc2];
    ys = [yc1 yc2];
    zs = [zc1 zc2];
    Rxs = [R1x R2x];
    Rys = [R1y R2y];

    % Walk through the different slices of the z-stack

    for z=1:rNz

        Ibwz = uint8(zeros(rNx,rNy));
        % Fill the disks corresponding to the intersections of each sphere
        % with the current slice
        angles = 0:0.01:2*pi;
        for i = 1:length(xs)
            Rxaux = Rxs(i);
            Ryaux = Rys(i);
            xaux = xs(i);
            yaux = ys(i);
            zaux = zs(i);
            alphax = Rxaux^2 - (z - zaux)^2;
            alphay = Ryaux^2 - (z - zaux)^2;
            if alphax > 0 && alphay > 0
                Rxdisk = sqrt(alphax);
                Rydisk = sqrt(alphay);
                xdisk = xaux + Rxdisk * cos(angles);
                ydisk = yaux + Rydisk * sin(angles);
                Ibwz = max(Ibwz,uint8(roipoly(Ibwz,xdisk,ydisk)));
            end
        end

        Ibw(:,:,z) = imfill(Ibwz,'holes');

    end

    clear i z Ibwz angles xaux yaux zaux Rxaux Ryaux alphax alphay ;
    clear xdisk ydisk Rxdisk Rydisk Rmin Rmax ;

    Ibw = uint8(Ibw);

    % ------------------------------------------------------------------ %

    im4 = 10 * Ibw + 10;

    im5 = convolve3D(single(im4),0.115/dx,0.519/dx);

    im6 = imnoise(uint8(im5),'poisson');

    im7 = double(im6) + 10 * randn(rNx,rNy,rNz);

    finalI = uint8( im7 .* ( 255 / max( im7(:) ) ) );
    if show_middle_slice == 'y'
        imtool(finalI(:,:,rNz/2));
    end

    % Keep 1/dzdx image along the Z-axis to match microscope resolution
    outputI = finalI(:,:,1:dzdx:rNz);
    outputIbw = Ibw(:,:,1:dzdx:rNz);

    %%% show current stack
    %figure(fig_montage); clf;
    %stack_bw = zeros(rNx,rNy,1,rNz);
    %stack_bw(:,:,1,:) = Ibwbg;
    %subplot(1,2,1), montage(stack_bw);
    %stack_I = stack_bw;
    %stack_I(:,:,1,:) = Inoised_1;
    %subplot(1,2,2);montage(stack_I);



    %%% Surface rendering of simulated cell
    %figure(fig_3d); clf;
    %xx = 1:size(Ibwbg,2);
    %yy = 1:size(Ibwbg,1);
    %zz = (1:size(Ibwbg,3));%*dzdx;
    %isosurface(xx,yy,zz,Ibwbg,0.5);
    %axis equal;
    %xlabel('x');
    %ylabel('y');
    %zlabel('z');
    %aux = [xx(1) xx(end) yy(1) yy(end) zz(1) zz(end)];
    %axis(aux); grid on;
    %%%%%%%%%%%%%%%

    %title(['Frame #',num2str(fr),'/',num2str(Nframes)]);
    %disp(['Generated frame  #',num2str(fr),'/',num2str(Nframes)]);
    if write_files =='y'

        aux = datestr(now,30);
        outputdir = [aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];
        mkdir(['original/',outputdir]);
        mkdir(['poisson/',outputdir]);

        for iz = 1:Nz
            % Write grey-level image
            outputfilename = ['poisson/',outputdir,'/cell_Z',int2str2(iz,4),'.tif'];%
            imwrite(outputI(:,:,iz),outputfilename,'tif','Compression','none');
            disp(['wrote slice #',num2str(iz),'/',num2str(Nz)]);
            % Write binary image
            outputfilename = ['original/',outputdir,'/cell_Z',int2str2(iz,4),'.tif'];
            imwrite(outputIbw(:,:,iz),outputfilename,'tif','Compression','none');
            disp(['wrote slice #',num2str(iz),'/',num2str(Nz)]);
        end

    end

end

disp('done');
