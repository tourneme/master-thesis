function string = int2str2(i,nbdigits)
% converts the input integer to a string as int2str, but pads
% with leading zeros in order to reach the desired number of digits
aux = int2str(i);
if length(aux)<nbdigits
    pz = int2str(10^(nbdigits-length(aux)));
    string = [pz(2:end),aux];
else
    string = aux;
end
