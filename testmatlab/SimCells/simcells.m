% Generate a synthetic image sequence simulating moving cells viewed with
% fluorescence microscopy

clear all,
close all;

write_files = input2('Write sequence of .tif files on the fly?','y');
store_images = input2('Store images ?','n');

% Image size
Nx = 250;
Ny = 250;

% Number of frames
Nframes = 10000;

% Number of cells
Ncells = 10;

% Range of radii of the main disk
Rmin = 25;
Rmax = 50;

% Maximum displacement of cell center (center of the main disk) 
drmax = Rmin;

% Noise parameters
noise_mean = 0;
noise_variance = 0.;


% Create output directory
cd /raid/czimmer/SyntheticData/simulated_amoebae;
aux = datestr(now,30);
outputdir = [aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];
mkdir(outputdir);
cd(outputdir);

if 1==0
    % Generate track first, to guarantee that the cell remains inside the image
    % borders
    good_track = 0;
    while(good_track==0)
        % Initial cell position
        xc = Nx/2;
        yc = Ny/2;    
        for fr=1:Nframes
            % Displacement step length (VERIFY THIS !!)
            dr = rand*drmax;
            % New Direction
            dtheta = rand*2*pi;
            dxc = dr*cos(dtheta);
            dyc = dr*sin(dtheta);
            % update cell position
            xc = xc + dxc;
            yc = yc + dyc;
            xcs(fr) = xc;
            ycs(fr) = yc;   
            inside_image = (xc>2*Rmax) && (xc<(Nx-2*Rmax)) && (yc>2*Rmax) && (yc<(Ny-2*Rmax));
            if ~inside_image
                break;
            end
        end    
        fr
        good_track = (fr==Nframes)
    end
end

figure;
for fr=1:Nframes
    Ibw = zeros(Nx,Ny);   
    % Define current position of the main disk center
    if 1==0
        xc = xcs(fr);
        yc = ycs(fr);
    else
        theta = rand*2*pi;
        rho = Rmin*0.9;
        xc = Nx/2 + rho*cos(theta);
        yc = Ny/2 + rho*sin(theta);
    end
    % radius of main circle
    R0 = Rmin+rand*(Rmax-Rmin);
    % make the central disk ("cell body")
    angles = 0:0.01:2*pi;
    xdisk = xc + R0*cos(angles);
    ydisk = yc + R0*sin(angles);
    Ibw = roipoly(Ibw,xdisk,ydisk);
    %%% add minor disks ("pseudopods")
    Npseudopods = round(rand*10);
    for i=1:Npseudopods
        % radius of pseudopod
        Rp = R0/2*rand;
        % center of pseudopod
        thetap = rand*2*pi;
        Raux = R0+rand*Rp;
        xp = xc + Raux*cos(thetap);
        yp = yc + Raux*sin(thetap);
        xdisk = xp + Rp*cos(angles);
        ydisk = yp + Rp*sin(angles);      
        Ibw = max(Ibw,roipoly(Ibw,xdisk,ydisk));
    end
    % Fill holes
    Ibw = imfill(Ibw,'holes');
    % Set object grey level
    Iobject = 128;
    Ibw = uint8(Ibw*Iobject);
    % Add noise    
    I = imnoise(Ibw,'gaussian',noise_mean,noise_variance);   
    % Compute SNR
    real_noise_variance = noise_variance*Iobject;
    SNR = Iobject/(sqrt(real_noise_variance));
    % show current frame
    subplot(1,2,1), imshow(Ibw);
    subplot(1,2,2);imshow(I);
    title(['Frame #',num2str(fr),'/',num2str(Nframes)]);
    disp(['Generated frame  #',num2str(fr),'/',num2str(Nframes)]);
    if write_files =='y'
        % Write grey-level image
        outputfilename = ['simcells_',int2str2(fr,6),'.tif'];
        imwrite(I,outputfilename,'tif','Compression','none');
        disp(['wrote image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
        % Write binary image
        outputfilename= ['bw_',int2str2(fr,6),'.tif'];
        imwrite(Ibw ,outputfilename,'tif','Compression','none');
        disp(['wrote image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
    end
    % Store simulated grey-level image and binary image
    if store_images == 'y'
        Isim_seq(:,:,1,fr) = I;
        Ibw_seq(:,:,1,fr) = Ibw;
    end
    clear I Ibw;
end

% Show movie
if size(Isim_seq,3)==3
    mov = immovie(Isim);
    movie(mov);
end

%%%% Write image sequence to disk

if 1==0   
    for fr = 1:Nframes
        % Write grey-level image
        outputfilename = ['simcells_',int2str2(fr,6),'.tif'];
        imwrite(Isim_seq(:,:,:,fr) ,outputfilename,'tif','Compression','none');
        disp(['wrote image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
        % Write binary image
        outputfilename= ['bw_',int2str2(fr,6),'.tif'];
        imwrite(Ibw_seq(:,:,:,fr) ,outputfilename,'tif','Compression','none');
        disp(['wrote image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
    end
end

disp('saving workspace ...');
save('simulated_cell_sequence');

disp('All finished');





