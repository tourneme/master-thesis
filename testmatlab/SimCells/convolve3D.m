function imageconv = convolve3D( image, sigmaXY, sigmaZ )

kernelXY = 1 ./ exp(((fix(-3*sigmaXY):fix(3*sigmaXY)) / sigmaXY).^2);

if sigmaXY == sigmaZ
    kernelZ = kernelXY;
else
    kernelZ = 1 ./ exp( ( (fix(-3*sigmaZ):fix(3*sigmaZ)) / sigmaZ ).^2 );
end

[nx ny nz] = size(image);

for i=1:nx
    for k=1:nz
        tmp1(i,:,k) = conv2( image(i,:,k), kernelXY ./ sum(kernelXY), 'same' );
    end
end

for j=1:ny
    for k=1:nz
        tmp2(:,j,k) = conv2( tmp1(:,j,k), kernelXY' ./ sum(kernelXY), 'same' );
    end
end

for i=1:nx
    for j=1:ny
        imageconv(i,j,:) = conv2( squeeze(tmp2(i,j,:)), kernelZ' ./ sum(kernelZ), 'same' );
    end
end
