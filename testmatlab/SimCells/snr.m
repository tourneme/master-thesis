function snr = snr(Signal, Noise)
% Computes the Signal/Noise ratio

ecart_type_signal = std2(Signal);
ecart_type_bruit = std2(Noise);

%snr = sqrt( ecart_type_signal^2 / ecart_type_bruit^2 );
snr = max(max(max(Signal))) / ecart_type_bruit;
