% Generate a synthetic 3D image sequence simulating a cell moving faster
% and faster

clear all,close all;

write_files = 'y';
show_middle_slice = 'n';

% Digital image size
Nx = 200;
Ny = Nx;
Nz = 50;

% Voxels size (microns)
dx = 0.03;

% Real image size
rNx = 200;
rNy = rNx;
rNz = rNx;

% Voxel aspect ratio
dzdx = 3.33;

% Number of frames
Nframes = 200;

% Range of radii of the main disk (microns)
R1 = 20;

% Create output directory
cd 'Z:\cells';
mkdir('singlecell');

%fig_montage = figure;
%fig_3d = figure;

for fr=1:Nframes
    Ibw = zeros(rNx,rNy,rNz);
    cellsOK = 0;

    while cellsOK == 0

        % position of the main sphere center
        phi = rand * pi - pi/2;
        theta = rand * 2 * pi;
        if ( fr == 1 ) % first time
            rho = rand * rNx;
            tmpxc = rNx/2 + rho * cos(phi) * cos(theta);
            tmpyc = rNy/2 + rho * cos(phi) * sin(theta);
            tmpzc = rNz/2 + rho * sin(phi);
        else
            rho = fr * 0.2;
            tmpxc = xc + rho * cos(phi) * cos(theta);
            tmpyc = yc + rho * cos(phi) * sin(theta);
            tmpzc = zc + rho * sin(phi);
        end

        if ( tmpxc > R1 && tmpxc < Nx-R1 && tmpyc > R1 && tmpyc < Ny-R1 && tmpzc > R1 && tmpzc < Nz*dzdx-R1 )
            xc = tmpxc;
            yc = tmpyc;
            zc = tmpzc;
            cellsOK = 1;
        end
    end

    xs = xc;
    ys = yc;
    zs = zc;
    Rs = R1;

    % Walk through the different slices of the z-stack
    
    for z=1:rNz
        
        Ibwz = zeros(rNx,rNy);
        % Fill the disks corresponding to the intersections of each sphere
        % with the current slice
        angles = 0:0.01:2*pi;
        for i2 = 1:length(Rs)
            Raux = Rs(i2);
            xaux = xs(i2);
            yaux = ys(i2);
            zaux = zs(i2);
            aux = Raux^2-(z-zaux)^2;
            if aux>0
                Rdisk = sqrt(aux);
                xdisk = xaux + Rdisk*cos(angles);
                ydisk = yaux + Rdisk*sin(angles);
                Ibwz = max(Ibwz,roipoly(Ibwz,xdisk,ydisk));
            end
        end
        Ibw(:,:,z) = imfill(Ibwz,'holes');
    end    
    
    clear Raux Rdisk Rmin Rmax angles xaux yaux zaux;
    clear theta thetap tmpxc tmpyc tmpzc xp yp zp Rp Rs Ibwz;
    clear xs ys zs rho phi phip drmax aux cellsOK xdisk ydisk;
    clear Npseudopods;

    disp('track done');

    % END OF TRACKS GENERATION
    % ------------------------------------------------------------------ %

    outputIbw = Ibw(:,:,1:dzdx:rNz);

    if write_files =='y'

        aux = datestr(now,30);
        outputdir = ['frame_',int2str2(fr,3),'_',aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];
        mkdir(['singlecell/',outputdir]);

        for iz = 1:Nz
            % Write binary image
            outputfilename = ['singlecell/',outputdir,'/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];
            imwrite(outputIbw(:,:,iz),outputfilename,'tif','Compression','none');
            disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
        end
    end

end


disp('All finished');
