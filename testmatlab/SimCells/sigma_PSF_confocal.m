% approximates the sigmas of a confocal PSF.
% Adapted from the Thomann Gaussian approximation of the PSF
% cf. Thomann et al. , J. of Microscopy 208, Oct. 2002, pp. 49-64

% lambda = emission wavelength
% NA = numerical aperture of the objective lens
% n = refractive index of the sample medium

function [sigma_xy,sigma_z] = sigma_PSF_confocal(lambda,NA,n)

sigma_xy = (0.21 * lambda / NA) / sqrt(2);
sigma_z = (0.66 * lambda * n / (NA*NA)) / sqrt(2);

% original version : remove the sqrt(2)s...