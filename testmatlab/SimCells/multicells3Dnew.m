% Generate a synthetic 3D image sequence simulating moving cells viewed with
% fluorescence microscopy

clear all,close all;

write_files = 'y';
show_middle_slice = 'n';

% Digital image size
Nx = 200;
Ny = Nx;
Nz = 50;

% Voxels size (microns)
dx = 0.03;

% Real image size
rNx = 200;
rNy = rNx;
rNz = rNx;

% Voxel aspect ratio
dzdx = 3.3;

% Number of frames
Nframes = 200;

% Range of radii of the main disk (microns)
Rmin = 20;
Rmax = 25;
R1 = Rmin+rand*(Rmax-Rmin);
R2 = Rmin+rand*(Rmax-Rmin);
R3 = Rmin+rand*(Rmax-Rmin);

% Maximum displacement of cell center (center of the main disk)
drmax1 = R1/2;
drmax2 = R2/2;
drmax3 = R3/2;

% Create output directory
%cd /home/adufour/MAI2005/Matlab/simdata;
cd 'Z:\cells';
mkdir('original');
mkdir('poisson');

%fig_montage = figure;
%fig_3d = figure;

for fr=1:Nframes
    Ibw = zeros(rNx,rNy,rNz);
    cellsOK = 0;

    while cellsOK == 0

        % FIRST CELL

        % position of the main sphere center
        phi = rand * pi - pi/2;
        theta = rand * 2 * pi;
        if ( fr == 1 ) % first time
            rho = rand * rNx;
            tmpxc = rNx/2 + rho * cos(phi) * cos(theta);
            tmpyc = rNy/2 + rho * cos(phi) * sin(theta);
            tmpzc = rNz/2 + rho * sin(phi);
        else
            rho = 5 + rand * drmax1;
            tmpxc = xc + rho * cos(phi) * cos(theta);
            tmpyc = yc + rho * cos(phi) * sin(theta);
            tmpzc = zc + rho * sin(phi);
        end

        % SECOND CELL
        % position of the main sphere center
        phi2 = rand*pi - pi/2;
        theta2 = rand*2*pi;
        if ( fr == 1 ) % first time
            rho2 = rand * rNx;
            tmpxc2 = rNx/2 + rho2 * cos(phi2) * cos(theta2);
            tmpyc2 = rNy/2 + rho2 * cos(phi2) * sin(theta2);
            tmpzc2 = rNz/2 + rho2 * sin(phi2);
        else
            rho2 = 5 + rand * drmax2;
            tmpxc2 = xc2 + rho2 * cos(phi2) * cos(theta2);
            tmpyc2 = yc2 + rho2 * cos(phi2) * sin(theta2);
            tmpzc2 = zc2 + rho2 * sin(phi2);
        end

        % THIRD CELL
        % position of the main sphere center
        phi3 = rand*pi - pi/2;
        theta3 = rand*2*pi;
        if ( fr == 1 ) % first time
            rho3 = rand * rNx;
            tmpxc3 = rNx/2 + rho3 * cos(phi3) * cos(theta3);
            tmpyc3 = rNy/2 + rho3 * cos(phi3) * sin(theta3);
            tmpzc3 = rNz/2 + rho3 * sin(phi3);
        else
            rho3 = 5 + rand * drmax3;
            tmpxc3 = xc3 + rho3 * cos(phi3) * cos(theta3);
            tmpyc3 = yc3 + rho3 * cos(phi3) * sin(theta3);
            tmpzc3 = zc3 + rho3 * sin(phi3);
        end

        distance12 = sqrt( (tmpxc-tmpxc2)^2 + (tmpyc-tmpyc2)^2 + (tmpzc-tmpzc2)^2 );
        distance13 = sqrt( (tmpxc-tmpxc3)^2 + (tmpyc-tmpyc3)^2 + (tmpzc-tmpzc3)^2 );
        distance23 = sqrt( (tmpxc2-tmpxc3)^2 + (tmpyc2-tmpyc3)^2 + (tmpzc2-tmpzc3)^2 );
        if ( fr == 1 )
            if ( distance12 < 3*(R1+R2) && distance12 > (R1+R2) && ...
                 distance13 < 3*(R1+R3) && distance13 > (R1+R3) && ...
                 distance23 < 3*(R2+R3) && distance23 > (R2+R3) && ...
                 tmpxc > R1 && tmpxc < Nx-R1 && tmpyc > R1 && tmpyc < Ny-R1 && tmpzc > R1 && tmpzc < Nz*dzdx-R1 && ...
                 tmpxc2 > R2 && tmpxc2 < Nx-R2 && tmpyc2 > R2 && tmpyc2 < Ny-R2 && tmpzc2 > R2 && tmpzc2 < Nz*dzdx-R2 && ...
                 tmpxc3 > R3 && tmpxc3 < Nx-R3 && tmpyc3 > R3 && tmpyc3 < Ny-R3 && tmpzc3 > R3 && tmpzc3 < Nz*dzdx-R3 ...
               )
                xc = tmpxc;
                yc = tmpyc;
                zc = tmpzc;
                xc2 = tmpxc2;
                yc2 = tmpyc2;
                zc2 = tmpzc2;
                xc3 = tmpxc3;
                yc3 = tmpyc3;
                zc3 = tmpzc3;
                cellsOK = 1;
            end
        else
            if ( distance12 < 2*(R1+R2) && distance12 > 0.9*(R1+R2) && ...
                 distance13 < 2*(R1+R3) && distance13 > 0.9*(R1+R3) && ...
                 distance23 < 2*(R2+R3) && distance23 > 0.9*(R2+R3) && ...
                 tmpxc > R1 && tmpxc < Nx-R1 && tmpyc > R1 && tmpyc < Ny-R1 && tmpzc > R1 && tmpzc < Nz*dzdx-R1 && ...
                 tmpxc2 > R2 && tmpxc2 < Nx-R2 && tmpyc2 > R2 && tmpyc2 < Ny-R2 && tmpzc2 > R2 && tmpzc2 < Nz*dzdx-R2 && ...
                 tmpxc3 > R3 && tmpxc3 < Nx-R3 && tmpyc3 > R3 && tmpyc3 < Ny-R3 && tmpzc3 > R3 && tmpzc3 < Nz*dzdx-R3 ...
               )
                xc = tmpxc;
                yc = tmpyc;
                zc = tmpzc;
                xc2 = tmpxc2;
                yc2 = tmpyc2;
                zc2 = tmpzc2;
                xc3 = tmpxc3;
                yc3 = tmpyc3;
                zc3 = tmpzc3;
                cellsOK = 1;
            end
        end
    end

    xs = [xc xc2 xc3];
    ys = [yc yc2 yc3];
    zs = [zc zc2 zc3];
    Rs = [R1 R2 R3];

    % Walk through the different slices of the z-stack
    %for iz = 1:Nz
    for z=1:rNz
        %z = iz*dzdx;
        Ibwz = zeros(rNx,rNy);
        % Fill the disks corresponding to the intersections of each sphere
        % with the current slice
        angles = 0:0.01:2*pi;
        for i2 = 1:length(Rs)
            Raux = Rs(i2);
            xaux = xs(i2);
            yaux = ys(i2);
            zaux = zs(i2);
            aux = Raux^2-(z-zaux)^2;
            if aux>0
                Rdisk = sqrt(aux);
                xdisk = xaux + Rdisk*cos(angles);
                ydisk = yaux + Rdisk*sin(angles);
                Ibwz = max(Ibwz,roipoly(Ibwz,xdisk,ydisk));
            end
        end
        %Ibw(:,:,iz) = Ibwz;
        Ibw(:,:,z) = imfill(Ibwz,'holes');
    end
    % Fill holes
    %Ibw = imfill(Ibw,'holes');

    clear Raux Rdisk Rmin Rmax angles xaux yaux zaux;
    clear theta thetap tmpxc tmpyc tmpzc xp yp zp Rp Rs Ibwz;
    clear xs ys zs rho phi phip drmax aux cellsOK xdisk ydisk;
    clear Npseudopods;

    disp('track done');

    % END OF TRACKS GENERATION
    % ------------------------------------------------------------------ %

    %     finalI = double(Ibw * 200);
    %
    %     finalI = finalI + (100.0 * (finalI==0));
    %
    %     %Add PSF blur to the original image
    %
    %     %Parameters of the gaussian PSF approximation
    %     lambda = 0.635; % Emission wavelength in microns
    %     NA = 1.4;       % Numerical aperture of the objective lens
    %     n = 1.33;       % refractive index of the sample medium
    %     [sigmaXY, sigmaZ] = sigma_PSF_confocal(lambda,NA,n);
    %
    %     finalI = convolve3D( finalI, sigmaXY / dx, sigmaZ / dx );
    %
    %     finalI = uint8(round(finalI));
    %
    %     finalI = imnoise(finalI, 'poisson');
    %
    %     finalI = double(finalI) + 5 * randn(size(finalI));
    %
    %     finalI = uint8( 255.0 * (finalI - min(finalI(:))) / (max(finalI(:)) - min(finalI(:))) );

    %imagesc(finalI(:,:,rNx/2));

    if 2 > 1

        %         figure, colormap(gray);
        %         finalI = finalI .* ( 255 / max( finalI(:) ) );
        %         if show_middle_slice == 'y'
        %             imshow(finalI(:,:,rNx/2));
        %             title('Image finale');
        %         end

        % Keep 1/dzdx image along the Z-axis to match microscope resolution
        %outputI = finalI(:,:,1:dzdx:rNz);
        outputIbw = Ibw(:,:,1:dzdx:rNz);

        %%% show current stack
        %figure(fig_montage); clf;
        %stack_bw = zeros(rNx,rNy,1,rNz);
        %stack_bw(:,:,1,:) = Ibwbg;
        %subplot(1,2,1), montage(stack_bw);
        %stack_I = stack_bw;
        %stack_I(:,:,1,:) = Inoised_1;
        %subplot(1,2,2);montage(stack_I);



        %%% Surface rendering of simulated cell
        %figure(fig_3d); clf;
        %xx = 1:size(Ibwbg,2);
        %yy = 1:size(Ibwbg,1);
        %zz = (1:size(Ibwbg,3));%*dzdx;
        %isosurface(xx,yy,zz,Ibwbg,0.5);
        %axis equal;
        %xlabel('x');
        %ylabel('y');
        %zlabel('z');
        %aux = [xx(1) xx(end) yy(1) yy(end) zz(1) zz(end)];
        %axis(aux); grid on;
        %%%%%%%%%%%%%%%

        %title(['Frame #',num2str(fr),'/',num2str(Nframes)]);
        %disp(['Generated frame  #',num2str(fr),'/',num2str(Nframes)]);
        if write_files =='y'

            aux = datestr(now,30);
            outputdir = ['frame_',num2str(fr),'_',aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];
            mkdir(['original/',outputdir]);
            %mkdir(['poisson/',outputdir]);

            for iz = 1:Nz
                % Write grey-level image
                %outputfilename = ['poisson/',outputdir,'/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];%
                %imwrite(outputI(:,:,iz),outputfilename,'tif','Compression','none');
                %disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
                % Write binary image
                outputfilename = ['original/',outputdir,'/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];
                imwrite(outputIbw(:,:,iz),outputfilename,'tif','Compression','none');
                disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
            end
            %save(outputdir);
        end

    end

end

%disp('saving workspace ...');

disp('All finished');
