% Generate a synthetic 3D image sequence simulating moving cells viewed with
% fluorescence microscopy

clear all,close all;

write_files = 'y';
show_middle_slice = 'n';

% Digital image size
Nx = 200;
Ny = Nx;
Nz = 40;

% Voxels size (microns)
dx = 0.05;    % microns (0.0645)
dy = dx;
dz = dx;

% Real image size
rNx = 200;
rNy = rNx;
rNz = rNx;

% Voxel aspect ratio
dzdx = Nx/Nz;

% Number of frames
Nframes = 1;

% Range of radii of the main disk (microns)
Rmin = 60;
Rmax = 70;
R0 = Rmin+rand*(Rmax-Rmin);
%R1 = Rmin+rand*(Rmax-Rmin);

% Maximum displacement of cell center (center of the main disk)
drmax = Rmin/2;

% Create output directory
%cd /home/adufour/MAI2005/Matlab/simdata;
cd 'C:\Documents and Settings\Alexandre\Desktop\cells';
mkdir('original');
mkdir('poisson');

%fig_montage = figure;
%fig_3d = figure;

for fr=1:Nframes
    Ibw = zeros(rNx,rNy,rNz);
    cellsOK = 0;

    while cellsOK == 0

        % FIRST CELL

        % position of the main sphere center
        phi = rand*pi - pi/2;
        theta = rand*2*pi;
        if ( fr == 1 ) % first time
            rho = R0 + rand*R0/3;
            tmpxc = rNx/2;% + rho*cos(phi)*cos(theta);
            tmpyc = rNy/2;% + rho*cos(phi)*sin(theta);
            tmpzc = rNz/2;% + rho*sin(phi);
        else
            rho = 5;
            tmpxc = xc + rho*cos(phi)*cos(theta);
            tmpyc = yc + rho*cos(phi)*sin(theta);
            tmpzc = zc + rho*sin(phi);
        end

        % to remove if second cell added
        xc = tmpxc;
        yc = tmpyc;
        zc = tmpzc;
        cellsOK = 1;

        if 1==0
            % SECOND CELL
            % position of the main sphere center
            phi2 = rand*pi - pi/2;
            theta2 = rand*2*pi;
            if ( fr == 1 ) % first time
                rho2 = R1 + rand*R1/3;
                tmpxc2 = Nx/2 + rho2*cos(phi2)*cos(theta2);
                tmpyc2 = Ny/2 + rho2*cos(phi2)*sin(theta2);
                tmpzc2 = Nz*dzdx/2 + rho2*sin(phi2);
            else
                rho2 = 5;
                tmpxc2 = xc2 + rho2*cos(phi2)*cos(theta2);
                tmpyc2 = yc2 + rho2*cos(phi2)*sin(theta2);
                tmpzc2 = zc2 + rho2*sin(phi2);
            end

            distance = sqrt( (tmpxc-tmpxc2)^2 + (tmpyc-tmpyc2)^2 + (tmpzc-tmpzc2)^2 );
            if ( distance < 1.5*(R0+R1) & distance > 0.9*(R0+R1) & tmpxc > R0 & tmpxc < Nx-R0 & tmpyc > R0 & tmpyc < Ny-R0 & tmpzc > R0 & tmpzc < Nz*dzdx-R0 & tmpxc2 > R1 & tmpxc2 < Nx-R1 & tmpyc2 > R1 & tmpyc2 < Ny-R1 & tmpzc2 > R1 & tmpzc2 < Nz*dzdx-R1 )
                xc = tmpxc;
                yc = tmpyc;
                zc = tmpzc;
                xc2 = tmpxc2;
                yc2 = tmpyc2;
                zc2 = tmpzc2;
                cellsOK = 1;
            end
        end
    end

    % PSEUDOPODS FOR FIRST CELL
    Npseudopods = 10;
    for i=1:Npseudopods
        % radius of pseudopod
        Rp(i) = R0/3*rand;
        % center of pseudopod
        phip = rand*pi-pi/2;
        thetap = rand*2*pi;
        Raux = max(0,R0-dzdx) + rand*Rp(i);
        xp(i) = xc + Raux*cos(phip)*cos(thetap);
        yp(i) = yc + Raux*cos(phip)*sin(thetap);
        zp(i) = zc + Raux*sin(phip);
    end

    % to remove if second cell added
    if Npseudopods>0
        xs = [xc xp];
        ys = [yc yp];
        zs = [zc zp];
        Rs = [R0 Rp];
    else
        xs = xc;
        ys = yc;
        zs = zc;
        Rs = R0;
    end

    % PSEUDOPODS FOR SECOND CELL
    if 1==0
        Npseudopods2 = 4;
        for i=1:Npseudopods2
            % radius of pseudopod
            Rp(i+Npseudopods) = R1/3*rand;
            % center of pseudopod
            phip2 = rand*pi-pi/2;
            thetap2 = rand*2*pi;
            Raux = max(0,R1-dzdx) + rand*Rp(i+Npseudopods);
            xp(i+Npseudopods) = xc2 + Raux*cos(phip2)*cos(thetap2);
            yp(i+Npseudopods) = yc2 + Raux*cos(phip2)*sin(thetap2);
            zp(i+Npseudopods) = zc2 + Raux*sin(phip2);
        end

        if Npseudopods>0 | Npseudopods2>0
            xs = [xc xc2 xp];
            ys = [yc yc2 yp];
            zs = [zc zc2 zp];
            Rs = [R0 R1 Rp];
        else
            xs = [xc xc2];
            ys = [yc yc2];
            zs = [zc zc2];
            Rs = [R0 R1];
        end
    end

    % Walk through the different slices of the z-stack
    %for iz = 1:Nz
    for z=1:rNz
        %z = iz*dzdx;
        Ibwz = zeros(rNx,rNy);
        % Fill the disks corresponding to the intersections of each sphere
        % with the current slice
        angles = 0:0.01:2*pi;
        for i2 = 1:length(Rs)
            Raux = Rs(i2);
            xaux = xs(i2);
            yaux = ys(i2);
            zaux = zs(i2);
            aux = Raux^2-(z-zaux)^2;
            if aux>0
                Rdisk = sqrt(aux);
                xdisk = xaux + Rdisk*cos(angles);
                ydisk = yaux + Rdisk*sin(angles);
                Ibwz = max(Ibwz,roipoly(Ibwz,xdisk,ydisk));
            end
        end
        %Ibw(:,:,iz) = Ibwz;
        Ibw(:,:,z) = imfill(Ibwz,'holes');
    end
    % Fill holes
    %Ibw = imfill(Ibw,'holes');

    clear Raux Rdisk Rmin Rmax angles xaux yaux zaux xc yc zc;
    clear theta thetap tmpxc tmpyc tmpzc xp yp zp Rp Rs Ibwz;
    clear xs ys zs rho phi phip drmax aux cellsOK xdisk ydisk;
    clear Npseudopods;

    % END OF TRACKS GENERATION
    % ------------------------------------------------------------------ %

    % Create structures in the whole cube
    im1 = ones( rNx, rNy, rNz )*0.5;
    im1 = imnoise( im1, 'gaussian', 0, 3 );
    im3 = convolve3D( im1, 3, 3 );

    Imaxtarget = 1;
    Imintarget = 0;
    im3mean = mean(im3(:));
    im3std = std(im3(:));
    im3max = im3mean+1.5*im3std;
    im3min = im3mean-1.5*im3std;

    % stretch the image gray levels
    im3bis = (im3-im3min)/im3max*(Imaxtarget-Imintarget) + Imintarget;

    Iobject = 0.01;
    im4 = im3bis * Iobject / mean(im3bis(:));
    im5 = im4;
    % Set background gray level (for Iobject = 0.01):
    % example : 0.00748 for a SNR ~ 1.5
    %           0.0088  for a SNR ~ 0.5
    im5(Ibw==0) = 0.006;
    %imagesc(im5(:,:,100));
    %figure,imshow(image(:,:,100),[]);

    % Add PSF blur to the original image

    % - Get parameters of the gaussian PSF approximation
    lambda = 0.5;   % Emission wavelength in microns
    NA = 1.4;       % Numerical aperture of the objective lens
    n = 1.33;       % refractive index of the sample medium
    [sigmaXY, sigmaZ] = sigma_PSF_confocal(lambda,NA,n);

    im6 = convolve3D( im5, sigmaXY./dx, sigmaZ./dz );
    %figure, imagesc(final(:,:,100));

    % Add noise
    finalI = imnoise( uint8(im6*255), 'poisson' );
    % Compute SNR = Object intensity / std dev( noise )
    %    disp(['snr = ', num2str(std(double(Inoised_1(Ibw > 0))))]);
    %diff = double(finalI(:))./255
    snr = ( mean(im5(Ibw>0)) - mean(im5(Ibw==0)) ) / std( double(finalI(:))/255 - im5(:) );
    disp(num2str(snr));

    if snr > 1

        %figure, colormap(gray);
        finalI = finalI .* ( 255 / max( finalI(:) ) );
        if show_middle_slice == 'y'
            imshow(finalI(:,:,rNx/2));
            title('Image finale');
        end

        % Keep 1/dzdx image along the Z-axis to match microscope resolution
        outputI = finalI(:,:,1:dzdx:rNz);
        outputIbw = Ibw(:,:,1:dzdx:rNz);

        %%% show current stack
        %figure(fig_montage); clf;
        %stack_bw = zeros(rNx,rNy,1,rNz);
        %stack_bw(:,:,1,:) = Ibwbg;
        %subplot(1,2,1), montage(stack_bw);
        %stack_I = stack_bw;
        %stack_I(:,:,1,:) = Inoised_1;
        %subplot(1,2,2);montage(stack_I);



        %%% Surface rendering of simulated cell
        %figure(fig_3d); clf;
        %xx = 1:size(Ibwbg,2);
        %yy = 1:size(Ibwbg,1);
        %zz = (1:size(Ibwbg,3));%*dzdx;
        %isosurface(xx,yy,zz,Ibwbg,0.5);
        %axis equal;
        %xlabel('x');
        %ylabel('y');
        %zlabel('z');
        %aux = [xx(1) xx(end) yy(1) yy(end) zz(1) zz(end)];
        %axis(aux); grid on;
        %%%%%%%%%%%%%%%

        %title(['Frame #',num2str(fr),'/',num2str(Nframes)]);
        %disp(['Generated frame  #',num2str(fr),'/',num2str(Nframes)]);
        if write_files =='y'
            
            aux = datestr(now,30);
            outputdir = [aux(7:8),'-',aux(5:6),'-',aux(1:4),'_',aux(10:11),'h',aux(12:13)];
            mkdir(['original/',outputdir]);
            mkdir(['poisson/',outputdir]);

            for iz = 1:Nz
                % Write grey-level image
                outputfilename = ['poisson/',outputdir,'/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];%
                imwrite(outputI(:,:,iz),outputfilename,'tif','Compression','none');
                disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
                % Write binary image
                outputfilename = ['original/',outputdir,'/cell_',int2str2(fr,6),'_',int2str2(iz,4),'.tif'];
                imwrite(outputIbw(:,:,iz),outputfilename,'tif','Compression','none');
                disp(['wrote slice #',num2str(iz),'/',num2str(Nz),' of image ',outputfilename,' (',num2str(fr),' /',num2str(Nframes),')']);
            end
            %save(outputdir);
        end

    end

end

%disp('saving workspace ...');

disp('All finished');





