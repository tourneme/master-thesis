global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, '../reusable_code');
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);
path(path,'/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/gim');
close all

name = 'bunny';


% Load the geometry image
M = read_gim('bunny-sph.gim');
imwrite(M,'bubunny.bmp','bmp');
%% 
% Next we create the semi regular mesh from the Spherical GIM.

% option for the load and display
options.func = 'mesh';
options.name = name;
options.use_elevation = 0;
options.use_color = 0;
J = 6;
% creation of the mesh
[vertex,face,vertex0] = compute_semiregular_gim(M,J,options);


%%
% We can display the semi-regular mesh.

selj = J-3:J;
clf;
for j=1:length(selj)
	vertices=vertex{j}';
	faces=face{j}';
	vertexo=vertex0{j}';
	[x,y]=cart2sph(vertexo(:,1),vertexo(:,2),vertexo(:,3));
	sph_verts=zeros(length(x),3);
	[sph_verts(:,1),sph_verts(:,2),sph_verts(:,3)]=sph2cart(x,y,ones(length(x),1));
	
	%we center the cell in order to get relevant norm.
	mean_vert=mean(vertices,1);
	vertices(:,1)=vertices(:,1)-mean_vert(1);
	vertices(:,2)=vertices(:,2)-mean_vert(2);
	vertices(:,3)=vertices(:,3)-mean_vert(3);
	
	%we calculate the coordinate of each point from the spherical
	%parameterization with the associate norm
	verts=sqrt(sum(vertices.^2,2));
	
	subplot(2,2,j);
	
	s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),verts);
	lighting phong;
	camproj('perspective');
	axis square;
	axis tight;
	axis off;
	axis equal;
	view(90, 0);
	camlight('headlight', 'infinite');
	view(270, 0);
	shading interp;
	camlight('headlight', 'infinite');
	set(s_handle, 'DiffuseStrength', 1);
	set(s_handle, 'SpecularStrength', 0);
	view(10,-40);
	%this very tiny line (cameramenu) gives you a perfect tool to make the
	%cell move, unfortunately, you will have to type it again after zooming
	%on the cell...
	cameramenu;   
	axis tight;
	title(['Subdivision level ' num2str(selj(j))]);
end
figure;
for j=1:length(selj)
	vertices=vertex{j}';
	faces=face{j}';
	vertexo=vertex0{j}';
	[x,y]=cart2sph(vertexo(:,1),vertexo(:,2),vertexo(:,3));
		sph_verts=zeros(length(x),3);
	[sph_verts(:,1),sph_verts(:,2),sph_verts(:,3)]=sph2cart(x,y,ones(length(x),1));
	
	%we center the cell in order to get relevant norm.
	mean_vert=mean(vertices,1);
	vertices(:,1)=vertices(:,1)-mean_vert(1);
	vertices(:,2)=vertices(:,2)-mean_vert(2);
	vertices(:,3)=vertices(:,3)-mean_vert(3);
	
	%we calculate the coordinate of each point from the spherical
	%parameterization with the associate norm
	verts=sqrt(sum(vertices.^2,2));
	
	subplot(2,2,j);
	
	s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),verts);
	lighting phong;
	camproj('perspective');
	axis square;
	axis tight;
	axis off;
	axis equal;
	shading interp;
	view(90, 0);
	camlight('headlight', 'infinite');
	view(270, 0);
	camlight('headlight', 'infinite');
	set(s_handle, 'DiffuseStrength', 1);
	set(s_handle, 'SpecularStrength', 0);
	view(10,-40);
	%this very tiny line (cameramenu) gives you a perfect tool to make the
	%cell move, unfortunately, you will have to type it again after zooming
	%on the cell...
	cameramenu;    
	axis tight;
	title(['Subdivision level ' num2str(selj(j))]);
end

keyboard;
