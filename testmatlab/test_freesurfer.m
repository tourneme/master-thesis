global RELATIV_ROOT
RELATIV_ROOT = '../';

path(path,[RELATIV_ROOT '/reusable_code']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/export_fig']);

load('/Users/cducroz/Documents/Robin/CODE MATLAB/DATA/data_sph/06-01-10_WT__1h00_#09_T30');

vertices=vertices-repmat(mean(vertices,1),length(vertices),1);

vertices=vertices/10000;

mne_write_surface([ RELATIV_ROOT 'DATA/data_sph_freesurfer/rh.white'],vertices,faces);


% [a,b]=unix('export FREESURFER_HOME=../../../../../Applications/freesurfer')
% path1 = getenv('FREESURFER_HOME')
% path1 = [path1 ':/usr/local/bin']
setenv('FREESURFER_HOME', '../../../../../../Applications/freesurfer')
setenv('SUBJ', '../DATA/data_sph_freesurfer')
!source $FREESURFER_HOME/SetUpFreeSurfer.sh; mris_smooth -n 3 -nw $SUBJ/rh.white $SUBJ/rh.smoothwm;mris_inflate $SUBJ/rh.smoothwm $SUBJ/rh.inflated;mris_sphere $SUBJ/rh.inflated $SUBJ/rh.sphere

[sph_verts,faces]=read_surf([RELATIV_ROOT 'DATA/data_sph_freesurfer/rh.sphere']);

faces=faces+1;

vertices=vertices*10000;

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

vertices=vertices+rand(length(vertices),3)*2;

%we calculate the coordinate of each point from the spherical
%parameterization with the associate norm
verts=sqrt(sum(vertices.^2,2));

figure;
s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),verts);
lighting phong;
camproj('perspective');

axis square;
% axis tight;
axis off;
% axis equal;
view(90, 0);
shading interp;
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(10,-40);
zoom(1.4);
%this very tiny line (cameramenu) gives you a perfect tool to make the
%cell move, unfortunately, you will have to type it again after zooming
%on the cell...
cameramenu;
export_fig('/Users/cducroz/Documents/Robin/rendu/rapport/latex/pictures/figureintro/erreurSPHARMMM.pdf','-pdf','-transparent')

% save([RELATIV_ROOT 'DATA/data_sph_freesurfer/results/06-01-10_WT__1h00_#09_T30.mat'],...
% 	'vertices','sph_verts','faces');




