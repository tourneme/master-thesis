close all
path(path, '../reusable_code');

global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, [RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code/']);


path(path, [RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);


path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);
path(path, [RELATIV_ROOT 'Toolboxes and functions/export_fig']);

options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;

j=4;

[vertices,faces] = compute_semiregular_sphere(j,options);
vertices=vertices{j}';
faces=faces{j}';


[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
theta=pi/2+theta;
phi=phi+pi;

disp('---------------------');
disp(['min theta ' num2str(min(theta))]);
disp(['min phi ' num2str(min(phi))]);
disp(['max theta ' num2str(max(theta))]);
disp(['max phi ' num2str(max(phi))]);
disp('---------------------');

for i=0:0
	for j=-i:i
		r=Ylm(i,j,theta,phi);
		
		% [vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi-pi,theta-pi/2,1+0.5*r);
		% [vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi-pi,theta-pi/2,1);
		
		figure;
		s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),r);
		lighting phong;
		camproj('perspective');
		
		axis square;
		% axis tight;
		axis off;
		% axis equal;
		view(90, 0);
		shading interp;
		camlight('headlight', 'infinite');
		view(270, 0);
		camlight('headlight', 'infinite');
		set(s_handle, 'DiffuseStrength', 1);
		set(s_handle, 'SpecularStrength', 0);
		view(10,-40);
		zoom(1.4);
		%this very tiny line (cameramenu) gives you a perfect tool to make the
		%cell move, unfortunately, you will have to type it again after zooming
		%on the cell...
		cameramenu;
		export_fig([ '/Users/cducroz/Documents/Robin/rendu/rapport/latex/pictures/figureintro/s_' num2str(i) '_' num2str(j) '.pdf'],'-pdf','-transparent')
	end
end


figure

x=0:0.01:2;

plot(x,sin(2*pi*8.*x),'k','LineWidth',5);
axis([0 2 -1.1 1.2]);
% axis tight;
axis off;
% axis equal;