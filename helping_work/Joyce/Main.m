%file showing how to load data easily from the DATA folder provided by Alex. 
%Sometimes, functions are really more complicated than what you need. 
%This is due to the fact that I am using many type of data. 

%Hope, this helps!

close all;


%if you wan to analyze tons of cells use the following function (you will
%have to set it up)
% cells=cell_choice;

%if you have a specific cell in mind, here is the naming you will have to
%follow  (digit number is critical):
cells{1}.date='06-01-10';   %format: ##-##-##
cells{1}.type='E64';        % E64 or WT
cells{1}.time='0h30';       % 0h30  or  1h00  or  1h30
cells{1}.number='00';       % ##
cells{1}.frames=17:33;         % ##:##
% cells{1}.set=1;             % commented   or   1   or   2

%data folder
Folder ='DATA';

%get the line number in the data folder from the cell you want to analyze
num=getNum(cells{1},Folder);

%list of name in the data folder
names=dir2cell(Folder);

figure;
x=0;
for i=num

    %this select the right cell
    name=names{i};

    %the data can be now loaded
    load([Folder '/' name]);
    
    x=x+1;
    subplot(4,5,x);
    %the task:
    s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3));
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
%     shading interp;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;
end

%Author: Robin tournemenne
%last mod. date: 07/16/2013