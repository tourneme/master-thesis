function currentName=cell_naming(act_cell,fr,correction)
%this function gives the string containing the right cell name.
%
%INPUT:
%   -act_cell:      the active cell you want to get the number (ONE cell with MANY frames)
%   -fr:            frame number you want the name of    
%   -correction:    set to one if you use the CALD optimization in the
%   spherical parameterization
%
%OUTPUT:
%   -currentName:   string containing the cell name
%
%Author: Robin tournemenne
%last mod. date: 07/16/2013


if (exist('correction','var') & correction)
    if isfield(act_cell, 'set')
        if act_cell.frames(fr)<10
            currentName=['cor_' act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T0' num2str(act_cell.frames(fr)) '_S' num2str(act_cell.set) '.mat'];
        else
            currentName=['cor_' act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T' num2str(act_cell.frames(fr)) '_S' num2str(act_cell.set) '.mat'];
        end
    else
        if act_cell.frames(fr)<10
            currentName=['cor_' act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T0' num2str(act_cell.frames(fr)) '.mat'];
        else
            currentName=['cor_' act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T' num2str(act_cell.frames(fr)) '.mat'];
        end             
    end
else
    if isfield(act_cell, 'set')
        if act_cell.frames(fr)<10
            currentName=[act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T0' num2str(act_cell.frames(fr)) '_S' num2str(act_cell.set) '.mat'];
        else
            currentName=[act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T' num2str(act_cell.frames(fr)) '_S' num2str(act_cell.set) '.mat'];
        end
    else
        if act_cell.frames(fr)<10
            currentName=[act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T0' num2str(act_cell.frames(fr)) '.mat'];
        else
            currentName=[act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
            act_cell.number '_T' num2str(act_cell.frames(fr)) '.mat'];
        end             
    end
end
end