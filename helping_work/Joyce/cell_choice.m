function cells=cell_choice
%This function let you select any data gathered in Institut
%Pasteur for amoeba 3D analysis between 2009 and 2010.
%
%The new set from 2013 are not implemented in this code yet !!!!!
%
%You have to use code folding in Matlab: MATLAB -> Preferences -> Editor/Debugger ->
%Code Folding -> (click on) Enable Cells
%
%and after to have a quick vision: right click -> Code Folding -> Fold All
%I usually use this code commenting or not cells that I want to study.
%
%Author: Robin tournemenne
%last mod. date: 07/16/2013

k=1;
%set to one if you want to use the new naming system
cells{k}.on=1;



%% 06-01-2010 WT    30min

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=2; %0-90
k=k+1; 

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:90; %0-90
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:90; %0-90
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:90; %0-90
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:90; %0-90
k=k+1;



%% 06-01-2010 WT    1h

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=10:91; %10-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=10:91; %10-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=2:91; %2-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='07';
cells{k}.frames=17:46; %17-46
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='08';
cells{k}.frames=17:91; %17-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=3; %2-91
k=k+1;



%% 06-01-2010 WT    1h30

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=0:88; %0-88
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=39:58; %39-58
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=0:69; %0-69
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=0:55; %0-55
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='04';
cells{k}.frames=0:79; %0-79
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='05';
cells{k}.frames=0:88; %0-88
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='06';
cells{k}.frames=0:27; %0-27
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='07';
cells{k}.frames=0:88; %0-88
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='08';
cells{k}.frames=0:69; %0-69
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='09';
cells{k}.frames=38:88; %38-88
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='10';
cells{k}.frames=0:15; %0-15
k=k+1;



%% 06-01-2010 E64   30min

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=0:45; %0-45
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:30; %0-30
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:70; %0-70
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='05';
cells{k}.frames=0:30; %0-30
k=k+1;



%% 06-01-2010 E64   1h00

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:43; %0-43
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=15:66; %15-66
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=15:66; %15-66
k=k+1;



%% 06-01-2010 E64   1h30   S1

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=0:87; %0-87
cells{k}.set=1;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=0:87; %0-87
cells{k}.set=1;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=1:87; %1-87
cells{k}.set=1;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=0:76; %0-76
cells{k}.set=1;
k=k+1;



%% 06-01-2010 E64   1h30   S2

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=4:82; %4-82
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=4:91; %4-91
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=4:45; %4-45
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=4:91; %4-91
cells{k}.set=2;
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='04';
cells{k}.frames=4:85; %4-85
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='05';
cells{k}.frames=4:91; %4-91
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='06';
cells{k}.frames=4:91; %4-91
cells{k}.set=2;
k=k+1;


cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='07';
cells{k}.frames=4:91; %4-91
cells{k}.set=2;
k=k+1;



%% 19-06-2009 E64   1h30



cells{k}.date='19-06-09';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=0:9; %0-9
k=k+1;


cells{k}.date='19-06-09';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=50:55; %44-87
k=k+1;



%% 07-01-2010 WT    1h30

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=0:81; %0-81
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=1:83; %1-83
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=0:83; %0-83
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=0:82; %0-82
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='04';
cells{k}.frames=0:64; %0-64
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='05';
cells{k}.frames=0:77; %0-77
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='06';
cells{k}.frames=0:82; %0-82
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='07';
cells{k}.frames=0:82; %0-82
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='08';
cells{k}.frames=0:52; %0-52
k=k+1;



% 07-01-2010 E64   30min

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=10; %0-81
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:71; %0-71
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='05';
cells{k}.frames=0:55; %0-55
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='06';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='07';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='08';
cells{k}.frames=66:91; %66-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='09';
cells{k}.frames=66:91; %66-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='10';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='11';
cells{k}.frames=14:91; %14-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='12';
cells{k}.frames=14:75; %14-75
k=k+1;



%% 07-01-2010 E64   1h

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=0:34; %0-34
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=0:36; %0-36
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=62:91; %62-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='07';
cells{k}.frames=0:28; %0-28
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=77:91; %77-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='10';
cells{k}.frames=77:91; %77-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='11';
cells{k}.frames=57:91; %57-91
k=k+1;



% 15-01-2010 WT    30min

cells{k}.on=1;
cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:90; %0-90
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=8; %0-90
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=2:91; %2-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='05';
cells{k}.frames=0:91; %0-91
k=k+1;



%% 15-01-2010 WT    1h

cells{k}.on=1;
cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=3:91; %3-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=3:91; %3-91
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=3:91; %3-91
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=3:91; %3-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=9:91; %9-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=9:91; %9-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=57:91; %57-91
k=k+1;



%% 15-01-2010 WT    1h30

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=0:29; %0-29
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='04';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='05';
cells{k}.frames=0:55; %0-55
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='06';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='07';
cells{k}.frames=0:91; %0-91
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
k=k+1;



% 15-01-2010 E64   30min   S1

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:51; %0-51
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=81; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:65; %0-65
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='05';
cells{k}.frames=0:69; %0-69
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='06';
cells{k}.frames=0:39; %0-39
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='07';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='09';
cells{k}.frames=1:91; %1-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='10';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;



cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='11';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='12';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='13';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='14';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;



cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='13';
cells{k}.frames=3:45; %3-45
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='14';
cells{k}.frames=27:91; %27-91
cells{k}.set=1;
k=k+1;



%% 15-01-2010 E64   30min   S2

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=1:82; %1-82
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='05';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='06';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='07';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;



%% 15-01-2010 E64   30min   S3

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=0:24; %0-24
cells{k}.set=3;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
cells{k}.set=3;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
cells{k}.set=3;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
cells{k}.set=3;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=0:91; %0-91
cells{k}.set=3;
k=k+1;



%% 15-01-2010 E64   1h   S1

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=0:79; %0-79
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='07';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=7:91; %7-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='10';
cells{k}.frames=7:91; %7-91
cells{k}.set=1;
k=k+1;



cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='11';
cells{k}.frames=0:91; %0-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='12';
cells{k}.frames=6:91; %6-91
cells{k}.set=1;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='13';
cells{k}.frames=6:91; %6-91
cells{k}.set=1;
k=k+1;



%% 15-01-2010 E64   1h   S2

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;

cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=0:53; %0-53
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=0:86; %0-86
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='07';
cells{k}.frames=0:25; %0-25
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='08';
cells{k}.frames=0:91; %0-91
cells{k}.set=2;
k=k+1;


cells{k}.date='15-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=0:24; %0-24
cells{k}.set=2;
k=k+1;



%% 06-10-2010 WT    1h

cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:19; %0-19
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='01';
cells{k}.frames=41:89; %41-89
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=41:89; %41-89
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=0:59; %0-59
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=0:89; %0-89
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=0:79; %0-79
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='06';
cells{k}.frames=0:89; %0-89
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='07';
cells{k}.frames=56:68; %56-68
k=k+1;

cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='08';
cells{k}.frames=0:40; %0-40
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=40:77; %40-77
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='10';
cells{k}.frames=41:70; %41-70
k=k+1;


cells{k}.date='06-10-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='11';
cells{k}.frames=60:89; %60-89
k=k+1;



