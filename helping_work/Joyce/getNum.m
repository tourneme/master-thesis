function num=getNum(act_cell,Folder,correction)
%function giving you the line number of the cell in the data folder
%
%INPUT: 
%   -act_cell:      the active cell you want to get the number (ONE cell with MANY frames)
%   -Folder:        the data folder path
%   -correction:    set to one if you use the CALD optimization in the
%   spherical parameterization
%
%OUTPUT:
%   -num:   cell number in the data folder
%
%Author: Robin tournemenne
%last mod. date: 07/16/2013

names_real=dir2cell(Folder);
num=zeros(1,length(act_cell.frames));

for j=1:length(act_cell.frames) 

    %check if you want the name with CALD correction or not
    if exist('correction','var')
        %cell naming create the string with the right cell name
        currentName=cell_naming(act_cell,j,correction);
    else
        currentName=cell_naming(act_cell,j);
    end
    
    if isempty(find(~cellfun('isempty',strfind(names_real,currentName)), 1))
        display(['the data entered are wrong. the possible guess is that the frame ' num2str(act_cell.frames(j)) ' doesn t exists']);
    else
        %collect the cell line number in the data folder
        num(j)=find(~cellfun('isempty',strfind(names_real,currentName)));
    end
end
end