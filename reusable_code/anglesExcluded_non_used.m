function isexcluded=anglesExcluded(zones,angles)

isexcluded=0;

for i=1:size(zones,1)
	
	alpha=zones(i,1);
	beta=zones(i,2);
	gamma=zones(i,3);
	sigma1=zones(i,4);
	sigma2=zones(i,5);
	
	if (angles(1)<alpha+sqrt(sigma1)*2*pi/3+sin(gamma)*tan(angles(2)) & angles(1)>alpha-(sqrt(sigma1)*2*pi/3+sin(gamma)*tan(angles(2))) & angles(2)<beta+sqrt(sigma2)*2*pi/3+sin(gamma)*tan(angles(1)) & angles(2)>beta+-(sqrt(sigma2)*2*pi/3+sin(gamma)*tan(angles(1))))
		
		isexcluded=1;
		break;
	end	
end
end