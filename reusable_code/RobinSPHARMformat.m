function vec=RobinSPHARMformat(data)
%
%function used to have this format :
%f00  f1-1  f10  f11  f2-2  f2-1  f20  f21  f22 ....
%starting with this format:
%f00  f10  f11  f20  f21  f22  f30 ...



global RELATIV_ROOT

path(path,[RELATIV_ROOT 'Toolboxes_and_functions/yawtb/interfaces/spharmonickit']);


for x=1:3
        mat=ConvertMyFormat2S2kitFormat(data(:, x));

        mat_readable=lmshape(mat);
        
        m=size(mat_readable,1);
        n=size(mat_readable,2);
        
        
        ind=1;
        for j=2:n 
            for i=[m-(j-2):m 1:j]
                ind=[ind (j-1)*m+i];
            end
        end
        vec(:,x)=mat_readable(ind);
end
end