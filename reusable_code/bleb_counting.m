function [groupBleb,soleBleb]=bleb_counting(zones,gaussians,varargin)

if varargin{1}
	disp(zones);
end

dists=zeros(gaussians,gaussians);
ctDists=zeros(gaussians,gaussians);
soleBleb=0;
groupBleb=0;

for i=1:gaussians-1
	
	Ri=multRotation(-zones,i,1);
	
	GOIcenter=Ri'/[0,0,1];
	SOI=min(1.05*min(zones(i,4),zones(i,5)),max(zones(i,4),zones(i,5)));

	for j=i+1:size(zones,1)
	
		
		Rj=multRotation(-zones,j,1);
		
		otherGcenter=Rj'/[0,0,1];
		
		centerDist=acos(GOIcenter'*otherGcenter);
		
		otherGS=min(1.05*min(zones(j,4),zones(j,5)),max(zones(j,4),zones(j,5)));
		
		dsig=max(SOI/otherGS,otherGS/SOI);


		% 		div=0.5*dsig+1.2;
		div=((2.2-1.62)/0.6)*dsig+(1.62-(2.2-1.62)/0.6);
		cor=-0.95.*((dsig-1.3).^2-0.089);
		div=div+cor;
		
		minDist=2*(atan(SOI/div)+atan(otherGS/div));
		
		dists(i,j)=centerDist-minDist;
		ctDists(i,j)=centerDist;
		
		if varargin{1}
			disp([num2str(i) ' ' num2str(j)]);
			disp(['dsig: ' num2str(dsig)]);
			disp(['div: ' num2str(div)]);
			disp(['angle centers: ' num2str(centerDist*180/pi)]);
			disp(['mindist: ' num2str(minDist*180/pi)]);
			disp(['diff: ' num2str(abs((minDist-centerDist)*180/pi))]);
			disp(['angle GOI: ' num2str(2*atan(SOI/div)*180/pi)]);
			disp(['angle otherG: ' num2str(2*atan(otherGS/div)*180/pi)]);
			if dists(i,j)<0
				disp('-1');
			end
			disp('-----------------------');
		end
		
	end
end

x=1;
mark=[];
for i=1:size(zones,1)
	if min(min(dists))>=0
		
		[idx1,idx2]=find(dists*180/pi<2.5 & dists>0);
		
		if ~isempty(idx1)
			if length(idx1)==1
				ambiguities=1;
			else
				ambiguities=1;
				for j=1:length(idx1)
					if isempty(find(idx1(1:j-1)==idx1(j) | idx2(1:j-1)==idx1(j)...
							| idx1(1:j-1)==idx2(j) | idx2(1:j-1)==idx2(j), 1)) 
						
						ambiguities=ambiguities+1;
					end
				end
			end
		else
			ambiguities=0;
		end
		soleBleb=soleBleb+size(dists,1);
		groupBleb=size(dists,1)-ambiguities;
	
		if varargin{1}
			disp(['the min number of bleb according ref method: ' num2str(groupBleb)]);
			disp(['the max number of bleb according ref method: ' num2str(soleBleb)]);
		end
		
		return;
	else
		[idx1,idx2]=find(dists==min(min(dists)));

		
		%this prenvent the stacking of two gaussians
		sig_mean=mean([zones(idx1,4),zones(idx1,5),zones(idx2,4),zones(idx2,5)]);
		smallest_val=((20-13)/(0.33-0.25))*sig_mean+17-0.33*(20-13)/(0.33-0.25);
		if ctDists(idx1,idx2)*180/pi<smallest_val
			soleBleb=0;
			groupBleb=0;
			return;
		end
		
		if abs(dists(idx1,idx2)*180/pi)<2.5
			soleBleb=soleBleb+1;
		end
		
		if zones(idx1,6)>zones(idx2,6)
			%if we have no group
			if isempty(find(mark==idx1 | mark==idx2, 1))
				if idx1<idx2
					mark(x)=idx1;
				else
					mark(x)=idx1-1;
				end
				x=x+1;
			%but if we have already a group with this index
			else
				if ~isempty(find(mark==idx2, 1))
					mark(mark==idx2)=idx1;
				end
			end
			dists=removeLineCol(dists,idx2,idx2);
			ctDists=removeLineCol(ctDists,idx2,idx2);
			if varargin{1}
				disp(['the gaussian ' num2str(idx2) ' becomes the gaussian ' num2str(idx1)]);
			end
		else
			if idx2<idx1
				mark(x)=idx2;
			else
				mark(x)=idx2-1;
			end
			x=x+1;
			dists=removeLineCol(dists,idx1,idx1);
			ctDists=removeLineCol(ctDists,idx1,idx1);
			if varargin{1}
				disp(['the gaussian ' num2str(idx1) ' becomes the gaussian ' num2str(idx2)]);
			end
		end
	end
end
end