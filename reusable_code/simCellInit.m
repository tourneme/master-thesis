function [vertices,radius]=simCellInit(vertices)
		
[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
theta=pi/2-theta;

new_radius=Gaussian2D(0.6,0.9,0.6,theta,phi);
radius=radius+new_radius;

[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,pi/2-theta,radius);


xmin=-pi/6;
xmax=pi/6;
ag_x=xmin+(xmax-xmin)*rand(1,1);

ymin=-pi/6;
ymax=pi/6;
ag_y=ymin+(ymax-ymin)*rand(1,1);

zmin=-pi;
zmax=pi;
ag_z=zmin+(zmax-zmin)*rand(1,1);

R=rotate_mat(ag_x,pi+ag_y,ag_z);

vertices=vertices*R;

[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
theta=pi/2-theta;

new_radius=Gaussian2D(0.5,0.8,0.6,theta,phi);
radius=radius+new_radius;

radius=radius./max(radius);

[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,pi/2-theta,radius);

% 			rotation of the cell
m_min=-pi;
m_max=pi;
angles=m_min+(m_max-m_min)*rand(3,1);

R=rotate_mat(angles(1),angles(2),angles(3));

vertices=vertices*R;
		
end
