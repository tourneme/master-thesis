function linking(h,num_data)
lgt=length(num_data);
if lgt<7
	%the following lines are used to make every cell move together.
	hlink = linkprop(h,{'CameraPosition','CameraUpVector'});
	key = 'graphics_linkprop';
	% Store link object on first subplot axes
	setappdata(h(1),key,hlink);
	%command to get the moving tool for 3D surfaces
else 
	hlink = linkprop(h(1:12),{'CameraPosition','CameraUpVector'});
	key = 'graphics_linkprop';
	setappdata(h(1),key,hlink);
	if lgt>6 & lgt<13
		hlink = linkprop(h(13:end),{'CameraPosition','CameraUpVector'});
		key = 'graphics_linkprop';
		setappdata(h(13),key,hlink);
	else
		hlink = linkprop(h(13:24),{'CameraPosition','CameraUpVector'});
		key = 'graphics_linkprop';
		setappdata(h(13),key,hlink);
		if lgt>12 & lgt<19
			hlink = linkprop(h(25:end),{'CameraPosition','CameraUpVector'});
			key = 'graphics_linkprop';
			setappdata(h(25),key,hlink);
		else
			hlink = linkprop(h(25:36),{'CameraPosition','CameraUpVector'});
			key = 'graphics_linkprop';
			setappdata(h(25),key,hlink);
			if lgt>18 & lgt<25
				hlink = linkprop(h(37:end),{'CameraPosition','CameraUpVector'});
				key = 'graphics_linkprop';
				setappdata(h(37),key,hlink);
			end
		end
	end
end
end