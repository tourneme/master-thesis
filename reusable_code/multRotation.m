function R=multRotation(zones,idx,inv)
%I still don't know if this function works... according to the order you do
%the unit rotations you iwll have different result. That's why there is an
%inv parameter.
%warning, you have to make the following calculation at the end: Ur=R*U and
%not U*R!!!!!!!

global RELATIV_ROOT;

path(path,[ RELATIV_ROOT 'Toolboxes_and_functions/SPHARM-MAT-v1-0-0/code']);


if ~exist('inv','var')
	inv=0;
end

R=eye(3);


	if inv
		if idx<size(zones,1)
			js=size(zones,1):-1:idx+1;
			for j=js
				R=rotate_mat(zones(j,1),zones(j,2),zones(j,3),inv)*R;
			end
		end	
	else
		if idx>1
			js=2:idx;
			for j=js
				R=R*rotate_mat(zones(j,1),zones(j,2),zones(j,3),inv);
			end
		end
	end
	
	

end