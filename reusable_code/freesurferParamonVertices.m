function [sph_verts,faces,vertices] = freesurferParamonVertices(vertices,faces)

global RELATIV_ROOT

path(path,[RELATIV_ROOT '/reusable_code']);

vertices=vertices-repmat(mean(vertices,1),length(vertices),1);

vertices=vertices/10000;

mne_write_surface([ RELATIV_ROOT 'DATA/data_sph_freesurfer/rh.white'],vertices,faces);

setenv('FREESURFER_HOME', '../../../../../../../../../../../Applications/freesurfer')
setenv('SUBJ', '../DATA/data_sph_freesurfer')
!source $FREESURFER_HOME/SetUpFreeSurfer.sh; mris_smooth -n 3 -nw $SUBJ/rh.white $SUBJ/rh.smoothwm;mris_inflate $SUBJ/rh.smoothwm $SUBJ/rh.inflated;mris_sphere $SUBJ/rh.inflated $SUBJ/rh.sphere

[sph_verts,faces]=read_surf([RELATIV_ROOT 'DATA/data_sph_freesurfer/rh.sphere']);

faces=faces+1;

vertices=vertices*10000;
end
