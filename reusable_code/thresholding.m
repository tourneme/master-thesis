function fw_final=thresholding(fw,level,method)

switch method
    %hard_thresholding
    case 1
        max_fw=max(max(abs(fw)));
        min_fw=min(min(abs(fw)));

        if level==1
            ind_fw=find(fw<min_fw+0.65*(max_fw-min_fw));
        elseif level==2

            %good choice for 6/12 WT 1h 9 T30
        %             ind_fw=find(fw<min_fw+0.45*(max_fw-min_fw));
            ind_fw=find(fw<min_fw+0.55*(max_fw-min_fw));
        elseif level==3
            %perfect for 6/12 WT 1h 9 T30
        %             ind_fw=find(fw<min_fw+0.41*(max_fw-min_fw));
            ind_fw=find(fw<min_fw+0.41*(max_fw-min_fw));

        else

        end

        fw_final=fw;

        fw_final(ind_fw)=0;
    
    %universal_threshold
    case 2
        lambda=mad(fw)/0.6745;
        lambda=lambda*sqrt(2*log10(length(fw)));
%         lambda=lambda*3.4641;
             


        fw_final=fw;
        fw_final(fw<lambda)=0;
        
%         figure;
%         plot(fw);
%         hold on;
%         plot([0 length(fw)],[lambda, lambda],'-g');
%         plot([0 length(fw)],[lambda+mean(fw), lambda+mean(fw)],'-r');
%         plot([0 length(fw)],[mean(fw), mean(fw)],'-r');
%         axis([0 length(fw) 0 0.3]);
end




end