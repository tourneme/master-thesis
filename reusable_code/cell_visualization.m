function cell_visualization(cells)
        
%function organizing the plotting of simple cell surface. According to the
%number of surface to display the size of the plot will change.

global RELATIV_ROOT
Folder =[RELATIV_ROOT 'DATA/data_sph'];

%split_fr stands for splitting frame dividing the vector num into all the
%analyzed cells

on=cells{1}.on;
split_fr=zeros(length(cells),1);   
for i=1:length(split_fr)
	num_calc=getNum(cells{i},Folder,'');
    if length(num_calc)<=25
        if length(num_calc)<=16
            if length(num_calc)<=9
                if length(num_calc)<=4                  
                    plot_cell_surface(num_calc,2);
                else                
                plot_cell_surface(num_calc,3);
                end
            else
            plot_cell_surface(num_calc,4);
            end    
        else
        plot_cell_surface(num_calc,5);
        end
    else            
        n=floor(length(num_calc)/5);
        charts=floor(n/5)+1;
        for chart=1:charts
            if chart==charts;
                if length(num_calc)<=16
                    if length(num_calc)<=9
                        if length(num_calc)<=4                  
                            plot_cell_surface(num_calc,2);
                        else                
                        plot_cell_surface(num_calc,3);
                        end
                    else
                    plot_cell_surface(num_calc,4);
                    end    
                else
                plot_cell_surface(num_calc,5);
                end            
            else
                plot_cell_surface(num_calc,5);  
                num_calc=num_calc(26:end);           
            end
        end
    end
end
end

    