function [fw_clean,threshold,sig] = WAT(coefs,Level,add,j,k,plot_thresh)

% Wavelet adaptive threshold: 
%   will comput a specific threshold for every scale of the wavelet
%   transform
%   
%   fw=coefficient of the wavelet transform
%   vertex vector the position of every vertex of each polyhedron from each
%   scale (only used to know the number of coefficient per class
%
%   Copyright (c) 2013 Robin Tournemenne XD

if size(coefs,2)>3
    coefs=coefs';
end
fw_clean=coefs;
sig=zeros(size(coefs,2),1);
threshold=zeros(size(coefs,2),1);

for i=1:size(coefs,2)
    fw_scale(:,i)=coefs(:,i);

    dist=fw_scale(:,i)-mean(fw_scale(:,i));

    %computation of the variance multiplied by a factor k user-dependant   
%     sig(i)=k*sqrt(2*log(length(coefs)))*sum(abs(dist))/(length(dist)*0.6745);

    sig(i)=k*sum(abs(dist))/(length(dist)*0.6745);

    if mean(fw_scale(:,i))>2
        threshold(i)=sig(i)+mean(fw_scale(:,i));
    else
        threshold(i)=sig(i);
    end
    
%     disp(['the mean is : ' num2str(mean(fw_scale))]);
    
    %finding the indexes where coefs has to be set at 0
    [tmp,I] = sort(abs(fw_scale(:,i)));
    ind=find(tmp>=threshold(i),1,'first');

    if ~isempty(ind)
        if ind~=1 
            fw_clean(I(1:ind-1),i) = 0;
        end
    else
        if j==1
            fw_clean(I,i) = 0;
        else
            fw_clean(I+add,i) = 0;    
        end    
    end
end

if size(coefs,2)==1
    display(['number of accepted values : ' num2str(length(find(fw_clean(:)~=0)))]);
else 
    display({'number of accepted values on x: ' num2str(length(find(fw_clean(:,1)~=0)));...
        'nnumber of accepted values on y: ' num2str(length(find(fw_clean(:,2)~=0)));...
        'number of accepted values on z: ' num2str(length(find(fw_clean(:,3)~=0)))});
end

if plot_thresh
    figure;
    i=1;
    fplot=coefs(:,i);
    fplot(coefs(:,i)<0)=0;
    plot(fplot,'-b');
    hold on
    plot(-fw_clean(:,i),'-r');
    plot(threshold(i)*ones(1,Level{j}(end)-add),'-g');
    legend('coef value without coef under 0','coef value after thresholding','thresholds');  
    title('adaptive threshold observation')
end
end
