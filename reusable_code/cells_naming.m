function [cells_name,cells_base_name]=cells_naming(names,fr_init,fr_end)
% this function provide two strings interesting for naming graphs when
% saving them.
%
% INPUT:
%   -names:     the list of name given by dir2cell function
%   -fr_init:   the index in names of the first frame of interest (this of
%   course suppose that you are working on only one cell)
%   -fr_end:    the index of the last frame of interest
%
% OUTPUT:
%   -cells_name:     the complete cell name wiht the starting and ending
%   frames
%   -cells_bae_name: the name of the cell without the frame information
%
%Author: Robin tournemenne
%last mod. date: 07/16/2013


name=names{fr_init};
name_end=names{fr_end};

if strcmp(name(1:3),'cir')
frame_init=name(end-6:end-4);
frame_end=name_end(end-6:end-4);	
	
cells_base_name=['simulated cell with ' name(10:11) ' blebs'];
	
else

frame_init=name(24:25);


name_end=names{fr_end};
frame_end=name_end(24:25);

cell_name=name(1:21);

[t o]=strtok(cell_name,'_');
[o k]=strtok(o(2:end),'_');

if k(2)=='_'
    [k e]=strtok(k(3:end),'_');
else
    [k e]=strtok(k(2:end),'_');
end

e=e(2:end);

cells_base_name=[t '    ' o '    ' k '    ' e];

end

cells_name=[cells_base_name '  T  ' frame_init ' <-> ' frame_end];

end