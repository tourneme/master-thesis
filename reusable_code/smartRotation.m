function [vertices,zones]=smartRotation(vertices,zones)

ref_abs=zones(end,1:3);

%random choice of the rotation angles: the rotation is done firstly on x
%and on y and finally on z.
m_min=-pi;
m_max=pi;
angles=m_min+(m_max-m_min)*rand(3,1);

% fixed values for testing
% angles=[2*pi/3;2*pi/3;0];

ref_abs=ref_abs+angles';
zones(end+1,1:3)=angles';
R=rotate_mat(angles(1),angles(2),angles(3));

vertices=R*vertices';
vertices=vertices';
end