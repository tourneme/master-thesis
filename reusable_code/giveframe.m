function frame=giveframe(act_cell,filter)

global RELATIV_ROOT;

names=dir2cell([ RELATIV_ROOT 'DATA/data_sph_simu/' filter '/']);

if act_cell.gaus==1
	namesofinterest=[act_cell.type '_bleb#'...
		strblebs(act_cell.gaus) '_k' num2str(act_cell.k) '_sigma1_'...
		num2str(act_cell.sigma1) '_sigma2_' num2str(act_cell.sigma2)];
else
	namesofinterest=[act_cell.type '_gaus#' strblebs(act_cell.gaus) ...
		'_bleb#' strblebs(act_cell.minblebs) '_' strblebs(act_cell.maxblebs)];
end	


idx=find(~cellfun('isempty',strfind(names,namesofinterest)));


if isempty(idx)
	frame=1;
else
	for i=1:length(idx)
		names_idx(i,:)=names{idx(i)};
	end
	frames=str2num(names_idx(:,end-6:end-4));
	frame=max(frames)+1;
end




end