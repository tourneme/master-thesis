function [vertices,faces,sph_verts,zones,radius,frame,gaussians,minblebs,maxblebs,inFinal]=...
	randomGaussianSimu(act_cell,vertices,faces,filter,freesurfer,fast,varargin)
	
global RELATIV_ROOT

gaussians=act_cell.gaus;

%if there is only one gaussian (obsolete)
if gaussians==1
	[phi,theta,radius] = cart2sph(vertices(1,:),vertices(2,:),vertices(3,:));
	theta=pi/2-theta;
	new_radius=Gaussian2D(act_cell.k,act_cell.sigma1,act_cell.sigma2,theta,phi);
	radius=radius+new_radius;
	[vertices(1,:),vertices(2,:),vertices(3,:)]=sph2cart(phi,pi/2-theta,radius);
	m_min=-pi;
	m_max=pi;
	angles=m_min+(m_max-m_min)*rand(3,1);
	R=rotate_mat(angles(1),angles(2),angles(3));
	vertices=vertices'*R;
	
%for the normal case
else
	%first lets modify the initial shape
	[vertices,radius]=simCellInit(vertices);
	%preparing the bleb recognition zone; each line corresponds to a zone
	%with a gaussian: from left to right: rot_alpha,beta,gamma,sigma1,sigma2
	zones=[0,0,0,0,0];
	
	for i=1:gaussians
		%compute every gaussian-like blebs
		[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
		theta=pi/2-theta;
		
		%the very effort in simulating cells comes in the next 9 lines:
		kmin=0.25;%0.1
		kmax=0.4;
		k=kmin+(kmax-kmin)*rand(1,1);
		
		s1min=max(kmin,k/1.2);%2
		s1max=k;%2
		
		if ~fast
			disp(['s1min: ' num2str(s1min)]);
			disp(['s1max: ' num2str(s1max)]);
			disp('---------------------');
		end
		sigma1=s1min+(s1max-s1min)*rand(1,1);
		
		s2min=max(kmin,sigma1-(s1max-s1min)/2);%s2min=max(kmin,sigma1-k/kmax*(s1max-s1min)/2);
		s2max=min(sigma1+(s1max-s1min),kmax);%s2max=min(sigma1+k/kmax*(s1max-s1min)/2,kmax);
		sigma2=s2min+(s2max-s2min)*rand(1,1);
		
		zones(i,4)=sigma1;
		zones(i,5)=sigma2;
		zones(i,6)=k;
		
		new_radius=Gaussian2D(k,sigma1,sigma2,theta,phi);
		radius=radius+new_radius;
		
		[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,pi/2-theta,radius);
		
		%rotation of the cell in order not to have mountains of blebs (joke
		%attempt)
		if i~=gaussians
			[vertices,zones]=smartRotation(vertices,zones);
		end
	end
end

if freesurfer
	[sph_verts,faces,vertices] = freesurferParamonVertices(vertices,faces);
else
	%RADIAL projection
	[phi,theta] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));

	%computation of the spherical parameterization (here we
	%are in an exception because our cell is star-shaped, so we can "cheat")
	[sph_verts(:,1),sph_verts(:,2),sph_verts(:,3)]=sph2cart(phi,theta,ones(length(theta),1));
end

%%
if ~fast
	subplot(sqrt(varargin{1}),sqrt(varargin{1}),varargin{2});
	[act_cell.minblebs,act_cell.maxblebs]=plotSimCell(faces,vertices,radius,act_cell.gaus,zones);
else
	[act_cell.minblebs,act_cell.maxblebs]=bleb_counting(zones,gaussians,0);
end
minblebs=act_cell.minblebs;
maxblebs=act_cell.maxblebs;
frame=giveframe(act_cell, filter);

%%
if ~exist([ RELATIV_ROOT 'DATA/data_sph_simu/' filter],'dir')
	mkdir([ RELATIV_ROOT 'DATA/data_sph_simu/' filter]);
	mkdir([ RELATIV_ROOT 'DATA/data_sph_simu/' filter '/zones']);
end

if act_cell.gaus==1
	inFinal=[ RELATIV_ROOT 'DATA/data_sph_simu/' filter act_cell.type '_bleb#'...
		strblebs(act_cell.blebs) '_k' num2str(act_cell.k) '_sigma1_'...
		num2str(act_cell.sigma1) '_sigma2_' num2str(act_cell.sigma2)...
		'_#' right_frame(frame) '.mat' ];
else
	inFinal=[ RELATIV_ROOT 'DATA/data_sph_simu/' filter '/' act_cell.type  ...
		'_gaus#' strblebs(act_cell.gaus) ...
		'_bleb#' strblebs(act_cell.minblebs) '_' strblebs(act_cell.maxblebs) '_#' right_frame(frame)...
		'.mat'];
end
end