function [theta, phi, values]=create_SphWavelets(BW,kernel,scale,plot_)
%
%This function calculates the spherical wavelets according to parameters and
%plot the computed wavelets if plot_ is TRUE
%
%INPUT:
%   -BW: bandwith of the filter
%   -kernel: vector containing the dilation factor of each scale
%   -scale:index of the scale
%   -plot_: 0 or 1 if you want to plot or not the filters.
%
%OUTPUT:
%   -theta: grid on theta
%   -phi:   grid on phi
%   -values:values obtained on the grid
%
%modifications list:
%
%-Robin Tournemenne 26/07/2013 "creation"

disp(['kernel: ' num2str(kernel(scale))]);
%create the spherical laplacian of gaussian
[theta, phi, values] = sampFnS2kitCompatible(@LaplacianOfGaussian, BW, 0, '', 1, kernel(scale),1);

global RELATIV_ROOT

path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes']);
path(path,[RELATIV_ROOT 'Toolboxes and functions/SWave1.2.2/overcomplete_wavelets']);



if plot_
    subplot(1,size(kernel,2),scale);

	options.use_color = 1;
	options.rho = .3;
	options.color = 'rescale';
	options.use_elevation = 0;
	% options for the multiresolution mesh
	options.base_mesh = 'ico';
	options.relaxation = 1;
	options.keep_subdivision = 1;

	j=6;
	[vertices,faces] = compute_semiregular_sphere(j,options);
	vertices=vertices{j}';
	faces=faces{j}';


	[phi,theta] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));

	value = feval(@LaplacianOfGaussian, theta+pi/2, phi, kernel(scale),1);
	[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,theta,1+0.1*value);
	
    trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),value);

    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    shading interp;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    view(10,-40);
    zoom(1.4);
    cameramenu;
end
end