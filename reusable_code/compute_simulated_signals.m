function [minblebs,maxblebs,frame]=compute_simulated_signals(act_cell,freesurfer,fast,varargin)
%
%This function computes simulated cells giving the vertice, the faces, and
%the spherical parameterization.
%The function bleb_counting should give the right number of blebs after the
%gaussian addition, but still doesn't work. This has to be fixed in a near
%future to do automatic bleb detection to grade our overcomplete method.


global RELATIV_ROOT
path(path, [ RELATIV_ROOT 'Toolboxes_and_functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes_and_functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes_and_functions/SPHARM-MAT-v1-0-0/code']);

%creating the base mesh

% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;
[vertex,face] = compute_semiregular_sphere(5,options);
vertices = vertex{end};
faces = face{end}';
vertices=vertices';

kernel_sizes=varargin{5};
kernel1=num2str(kernel_sizes(1));
kernelend=num2str(kernel_sizes(end));

filter=['LoG/BW' num2str(varargin{3}) 'max_invert'  num2str(varargin{4}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{6}) ];

%cell creation
[vertices,faces,sph_verts,zones,radius,frame,gaussians,act_cell.minblebs,act_cell.maxblebs,inFinal]=...
	randomGaussianSimu(act_cell,vertices,faces,filter,freesurfer,fast,varargin{1},varargin{2});

if act_cell.maxblebs>gaussians/2
	save(inFinal,'vertices','faces','sph_verts');
	save([inFinal(1:71) 'zones/' inFinal(72:end)],'zones','radius');
else
	maxblebs=0;
	minblebs=0;
	return;
end

% 	SPHARM extraction following Shen method
%%
%Spherical harmonic calculation on the BW.
BW=varargin{3};

confs.MaxSPHARMDegree = BW;
confs.OutDirectory = [RELATIV_ROOT 'DATA/data_sph_simu/' filter '/_des/'];

if ~exist(confs.OutDirectory,'dir')
	mkdir(confs.OutDirectory);
end

% 	outDes = SpharmMatExpansion(confs, inSmo, 'ExpLSF');
inFinalCell=cell(1);
inFinalCell{1}=inFinal;
outDes = SpharmMatExpansion(confs, inFinalCell, 'ExpLSF');
newoutDes=[inFinal(1:71) '_des/' inFinal(72:end)];
java.io.File(outDes{1}).renameTo(java.io.File(newoutDes))
movefile(newoutDes,inFinal(1:71));

minblebs=act_cell.minblebs;
maxblebs=act_cell.maxblebs;

end