function mat=removeLineCol(mat,i,j)

if i==1
	mat=mat(2:end,:);
elseif i==size(mat,1)
	mat=mat(1:end-1,:);
else
	mat=[mat(1:i-1,:);mat(i+1:end,:)];
end

if j==1
	mat=mat(:,2:end);
elseif j==size(mat,2)
	mat=mat(:,1:end-1);
else
	mat=[mat(:,1:i-1),mat(:,i+1:end)];
end

end
