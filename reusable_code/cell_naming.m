function currentName=cell_naming(act_cell,fr,type)
%this function gives the string containing the right cell name.
%
%INPUT:
%   -act_cell:      the active cell you want to get the number (ONE cell with MANY frames)
%   -fr:            frame number you want the name of    
%   -type:          according to what type of data you want to gather
%
%OUTPUT:
%   -currentName:   string containing the cell name
%
%Author: Robin tournemenne
%last mod. date: 07/16/2013

if strfind(type,'simu')
	if strfind(type,'amoeba')
		if strfind(type,'cor')
			type='amoeba_cor_';
		else
			type='amoeba_';
		end
	else
		type='';
	end
	
    if act_cell.gaus==1
        currentName=[type act_cell.type '_bleb#' strblebs(act_cell.gaus) '_k'...
			num2str(act_cell.k) '_sigma1_' num2str(act_cell.sigma1) '_sigma2_'...
			num2str(act_cell.sigma2) '_#' right_frame(fr) '.mat'];
    else
        currentName=[type act_cell.type '_gaus#' strblebs(act_cell.gaus) ...
			'_bleb#' strblebs(act_cell.minblebs) '_' strblebs(act_cell.maxblebs) '_#' right_frame(fr) '.mat']; 
    end
else

    if isfield(act_cell, 'set') 
        set=['_S' num2str(act_cell.set)];
    else
        set=[];
    end
    if fr<10
        time_marker='_T0';
    else
        time_marker='_T';
    end

    currentName=[type act_cell.date '_' act_cell.type '_' act_cell.time '_#'...
        act_cell.number time_marker num2str(fr) set '.mat'];
end









end








