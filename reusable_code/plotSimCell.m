function [minblebs, maxblebs]=plotSimCell(faces,vertices,radius,gaussians,zones)
%function plotting the obtained simulated cell and giving the right bleb
%number with the function bleb_counting that has to be fixed! Besides, I
%still don't succeed to put the bleb number above the blebs.... to be fixed
%also


s_handle=trisurf(faces,vertices(:,1),vertices(:,2),vertices(:,3),radius);
hold on
plot3([0 2],[0 0],[0 0],'-b');
plot3([0 0],[0 2],[0 0],'-r');
plot3([0 0],[0 0],[0 2],'-g');
hold off
lighting phong;
camproj('perspective');
axis square;
axis tight;
axis off;
axis equal;
% shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
view(0, 0);
cameramenu;

Tx=zeros(size(zones,1)+1,1);
Ty=zeros(size(zones,1)+1,1);
Tz=zeros(size(zones,1)+1,1);
Txt=zeros(size(zones,1)+1,1);

% for i=1:size(zones,1)
for i=size(zones,1):-1:1
	% 	R=rotate_mat(-zones(2,2),-zones(2,2),-zones(2,3),1);
	
	R=multRotation(-zones,i,1);
	
	Gauscenter=R'/[0,0,0.7];

	

	Tx(i)=Gauscenter(1);
	Ty(i)=Gauscenter(2);
	Tz(i)=Gauscenter(3);
	Txt(i)=i;	
end

text(Tx,Ty,Tz,num2str(Txt));


%WARNING even if you wanted ten blebs, gaussians may be layered,
%reducing the bleb number. The job of the following function is to
%determine how the layering is.
[minblebs,maxblebs]=bleb_counting(zones,gaussians,1);


text(1.5,0,1.5,['Bleb number between: ' num2str(minblebs) ' and ' num2str(maxblebs)]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end