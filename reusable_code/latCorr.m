function sph_verts_new=latCorr(faces,vertices,sph_verts,plot_)

global RELATIV_ROOT

path(path,[RELATIV_ROOT 'Toolboxes and functions/GridSphere']);

mean_vert=mean(vertices,1);
vertices(:,1)=vertices(:,1)-mean_vert(1);
vertices(:,2)=vertices(:,2)-mean_vert(2);
vertices(:,3)=vertices(:,3)-mean_vert(3);

verts=sqrt(sum(vertices.^2,2));
xx=sph_verts(:,1).*(verts);
xy=sph_verts(:,2).*(verts);
xz=sph_verts(:,3).*(verts);

[theta,phi]=Cartesian2Spherical(sph_verts(:,1),sph_verts(:,2),sph_verts(:,3));

x=-90:10:90;
[n,xout]=hist(theta,x);

[sig,mu]=gaussfit(xout/max(abs(xout)),n/max(n));


thn=theta/max(abs(xout));

modif=exp(-(thn-mu).^2/(2*sig^2));

modif=modif.*(mu-thn)./(sig^3*sqrt(2*pi));

theta_modif=theta-10*modif;

[n2,xout2]=hist(theta_modif,x);
thr=theta_modif*pi/180;
phr=phi*pi/180;
[sph_verts_new(:,1),sph_verts_new(:,2),sph_verts_new(:,3)]=sph2cart(phr,thr,ones(length(thr),1));

%density map of the spherical projection
%bon je reprendrai ?a bient?t le probl?me c'est de r?cup?rer le nombre de
%points dans une sph?re autour du point ?tudi?, ?a foire complet!!!!


dist=0.2;
for i=1:length(theta)
    if i==311
        
        figure
s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),ones(1,length(sph_verts)));
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;
hold on
plot3(sph_verts(idx_dens,1),sph_verts(idx_dens,2),sph_verts(idx_dens,3),'*r')
plot3([0 0],[0 0],[-1 1],'.b');
        
        
        keyboard;
    end
    phi_rot=(phi-phi(i))*pi/180;
    theta_rot=(theta-theta(i))*pi/180;
%     idx_dens=find(sign(phi(i))*sin(phi_rot).^2+cos(theta_rot).^2<=dist & abs(mod(theta_rot,pi/2))<pi/2 & abs(mod(phi_rot,pi/2))<pi/2);
    idx_dens=find(sign(phi(i))*sin(phi_rot).^2+cos(theta_rot).^2<=dist);

    if ~isempty(idx_dens)
        density=length(idx_dens)/(2*pi*cos(theta(i)*pi/180));
        rho(i)=density;
    end
end
rho=rho/max(rho);


if plot_

    figure;
    subplot(2,2,1);
    bar(xout,n/max(n));
    hold on

    plot(x,normpdf(xout/max(abs(xout)),mu,sig),'.r');
    
    subplot(2,2,3);
    s_handle=trisurf(faces,xx,xy,xz,verts);
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;

    subplot(2,2,2);
    bar(xout2,n2/max(n2));

    subplot(2,2,4);

    yx=sph_verts_new(:,1).*(verts);
    yy=sph_verts_new(:,2).*(verts);
    yz=sph_verts_new(:,3).*(verts);

    s_handle=trisurf(faces,yx,yy,yz,verts);
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;
    
    figure;
    
    subplot(1,2,1);
    s_handle=trisurf(faces,xx,xy,xz,rho);
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;
    
    subplot(1,2,2);
    s_handle=trisurf(faces,sph_verts(:,1),sph_verts(:,2),sph_verts(:,3),rho);
    lighting phong;
    camproj('perspective');
    axis square; 
    axis tight;
    axis off;
    axis equal;
    view(90, 0);
    camlight('headlight', 'infinite');
    view(270, 0);
    camlight('headlight', 'infinite');
    set(s_handle, 'DiffuseStrength', 1);
    set(s_handle, 'SpecularStrength', 0);
    view(10,-40);
    zoom(1.4);
    cameramenu;
    
end
