function [minblebs,maxblebs,frame]=compute_simulated_signals_fast_computation(act_cell,varargin)
%
%This function computes simulated cells giving the vertice, the faces, and
%the spherical parameterization.
%The function bleb_counting should give the right number of blebs after the
%gaussian addition, but still doesn't work. This has to be fixed in a near
%future to do automatic bleb detection to grade our overcomplete method.


global RELATIV_ROOT
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/toolbox']);
path(path, [ RELATIV_ROOT 'Toolboxes and functions/SPHARM-MAT-v1-0-0/code']);

%creating the base mesh

% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;
% options for the multiresolution mesh
options.base_mesh = 'ico';
options.relaxation = 1;
options.keep_subdivision = 1;
[vertex,face] = compute_semiregular_sphere(5,options);
vertices = vertex{end};
faces = face{end}';
vertices=vertices';

%choice of the method type=cir is the former one with random gaussian
%positionning, type=stack is the one with controlled gaussian
%positionning with random stacking.
if strcmp(act_cell.type,'cir')
	
	gaussians=act_cell.gaus;
	
	if gaussians==1
		
		[phi,theta,radius] = cart2sph(vertices(1,:),vertices(2,:),vertices(3,:));
		
		theta=pi/2-theta;
		
		new_radius=Gaussian2D(act_cell.k,act_cell.sigma1,act_cell.sigma2,theta,phi);
		
		radius=radius+new_radius;
		
		[vertices(1,:),vertices(2,:),vertices(3,:)]=sph2cart(phi,pi/2-theta,radius);
		
		m_min=-pi;
		m_max=pi;
		angles=m_min+(m_max-m_min)*rand(3,1);
		
		R=rotate_mat(angles(1),angles(2),angles(3));
		
		vertices=vertices'*R;
		
	else
		%first lets modify the initial shape
		[vertices,radius]=simCellInit(vertices);
		%preparing the bleb recognition zone; each line corresponds to a zone
		%with a gaussian: from left to right: rot_alpha,beta,gamma,sigma1,sigma2
		zones=[0,0,0,0,0];
		
		for i=1:gaussians
			% 			compute every blebs
			
			[phi,theta,radius] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
			theta=pi/2-theta;
			
			%the very effort in simulating cells comes in the next 9 lines:
			kmin=0.25;%0.1
			kmax=0.4;
			k=kmin+(kmax-kmin)*rand(1,1);
			
			s1min=max(kmin,k/1.2);%2
			s1max=min(k,kmax);%2
			
			sigma1=s1min+(s1max-s1min)*rand(1,1);
			
			s2min=max(kmin,sigma1-(s1max-s1min)/2);%s2min=max(kmin,sigma1-k/kmax*(s1max-s1min)/2);
			s2max=min(sigma1+(s1max-s1min),kmax);%s2max=min(sigma1+k/kmax*(s1max-s1min)/2,kmax);
			sigma2=s2min+(s2max-s2min)*rand(1,1);
			
			zones(i,4)=sigma1;
			zones(i,5)=sigma2;
			zones(i,6)=k;
			
			new_radius=Gaussian2D(k,sigma1,sigma2,theta,phi);
			radius=radius+new_radius;
			
			[vertices(:,1),vertices(:,2),vertices(:,3)]=sph2cart(phi,pi/2-theta,radius);
			
			%rotation of the cell in order not to have mountains of blebs
			if i~=gaussians
				[vertices,zones]=smartRotation(vertices,zones);
			end
		end
	end
	
	[phi,theta] = cart2sph(vertices(:,1),vertices(:,2),vertices(:,3));
	
	%has to be replaced by the right spherical parameterization (here we
	%are in an exception because our cell is convex, so we can "cheat")
	[sph_verts(:,1),sph_verts(:,2),sph_verts(:,3)]=sph2cart(phi,theta,ones(length(theta),1));
	
	
	%%
	[act_cell.minblebs,act_cell.maxblebs]=bleb_counting(zones,gaussians,0);
	minblebs=act_cell.minblebs;
	maxblebs=act_cell.maxblebs;
	frame=giveframe(act_cell);
	
	if act_cell.gaus==1
		inFinal=[ RELATIV_ROOT 'DATA/data_sph_simu/' act_cell.type '_bleb#'...
			strblebs(act_cell.blebs) '_k' num2str(act_cell.k) '_sigma1_'...
			num2str(act_cell.sigma1) '_sigma2_' num2str(act_cell.sigma2)...
			'_#' right_frame(frame) '.mat' ];
	else
		inFinal=[ RELATIV_ROOT 'DATA/data_sph_simu/' act_cell.type  ...
			'_gaus#' strblebs(act_cell.gaus) ...
			'_bleb#' strblebs(act_cell.minblebs) '_' strblebs(act_cell.maxblebs) '_#' right_frame(frame)...
			'.mat'];
	end
end


if act_cell.maxblebs>gaussians/2
	save(inFinal,'vertices','faces','sph_verts');
	save([inFinal(1:22) 'zones/' inFinal(23:end)],'zones','radius');
else
	maxblebs=0;
	minblebs=0;
	return;
end

% 	SPHARM extraction following Shen method
%%
%Spherical harmonic calculation on the BW.
BW=varargin{3};

confs.MaxSPHARMDegree = BW;
confs.OutDirectory = [RELATIV_ROOT 'DATA/data_sph_simu/_des/'];

if ~exist(confs.OutDirectory,'dir')
	mkdir(confs.OutDirectory);
end

% 	outDes = SpharmMatExpansion(confs, inSmo, 'ExpLSF');
inFinalCell=cell(1);
inFinalCell{1}=inFinal;
outDes = SpharmMatExpansion(confs, inFinalCell, 'ExpLSF');
newoutDes=[inFinal(1:22) '_des/' inFinal(23:end)];
java.io.File(outDes{1}).renameTo(java.io.File(newoutDes))
movefile(newoutDes,inFinal(1:21));

minblebs=act_cell.minblebs;
maxblebs=act_cell.maxblebs;

end