function wavelet_coefficient_analysis(num,k_mean,otsu,wat,levels,k_wat)


% First add to the path additional scripts
clear options;
base_path='../Toolboxes and functions/toolbox_wavelet_meshes/';    




% options for the display
options.use_color = 1;
options.rho = .3;
options.color = 'rescale';
options.use_elevation = 0;

Folder = [base_path 'ResultsWaveSph'];
d = dir(Folder);   

%split_fr stands for splitting frame dividing the vector num into all the
%analyzed cells
split_fr=find(num(2:end)-num(1:end-1)~=1);
split_fr=[0,split_fr];

for i=1:length(split_fr)
    if split_fr==0
        num_calc=num;
    else
        if i==length(split_fr)
            num_calc=num(split_fr(i)+1:end);
        else
            num_calc=num(split_fr(i)+1:split_fr(i+1));
        end
    end






    %j is the coefficient level we want to analyze
    for j = levels

        %indexes in coefficient vectors according to the level depth
        Level=cell(5,1);
        Level{1}=1:12; Level{2}= 13:42; Level{3}= 43:162; 
        Level{4}= 163:642; Level{5}= 643:2562; Level{6}= 2563:10242;

        %vector giving a color to each class
        colorstyle=cell(15,1);

        colorstyle{1}=[1 0 0];
        colorstyle{2}=[1 .35 0];
        colorstyle{3}=[1 .55 0];
        colorstyle{4}=[1 .65 0];
        colorstyle{5}=[1 .75 0];
        colorstyle{6}=[1 .9 0];
        colorstyle{7}=[.81 1 0];
        colorstyle{8}=[.65 1 0];
        colorstyle{9}=[.42 1 0];
        colorstyle{10}=[.28 1 0];
        colorstyle{11}=[.16 1 .2];
        colorstyle{12}=[0 .89 .5];
        colorstyle{13}=[0 .64 .75];
        colorstyle{14}=[0 .33 1];
        colorstyle{15}=[0 0 1];            

        if k_mean
            %%%%%%%%%%%%%%%%%%%%%%%%   K-mean analysis   %%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


            %maximal class number allowed for the computation of K-mean
            %analysis
            if j==3
                maxCl = 10;
            elseif j==2
                maxCl = 6;
            elseif j==1
                maxCl = 3;
            elseif j==4
                maxCl=2;
            elseif j==5
                maxCl=2;
            end

            k_mean_fig=figure();

            %subplot index
            sub_ind=0;

            for idx=num_calc

                % load the file
                name =  d(idx).name ;                
%                 load([Folder,'/',name]);
                load('../results/wavelet_coefs/Lazy_wavelet/06-01-10_WT 1H_#0000_T0011_CALD_LSF_SphWav.mat');

                disp(['The following cell is gonna be k-meaned : ' name]); 

                % Computation of the sum of the square of the components to get one
                % unique descriptor vector rotation independant:               
                obs = Level{j};
                fwav= fw(obs,:);

                fwavTot = fwav(:,1).^2 + fwav(:,2).^2 + fwav(:,3).^2;

                if j>3
                    %if the level of interest is above 3 we have to supress
                    %penton influence
                    out_vtx=Outvtx(vertex,face,j);

                    fwavTot(out_vtx-Level{j-1}(end))=0;
                end
                %n is the class number and each column of the matrix cluster
                %contains the indexes from points of the class
                [ n, cluster ] = optimClassNum( fwavTot, maxCl, 0);
                disp(['the optimal number of clusters at the level ',num2str(j),' is ',num2str(n)]);
                disp('the corresponding cluster is :');
                disp(cluster);

                %find the right color according to the importance of the class
                max_class=zeros(n,1);
                for class=1:n
                    max_class(class)=max(diag(cluster(:,class))*fwavTot);
                    sorted=sort(max_class);
                end
                high=find(max_class==sorted(end));
                mid=find(max_class==sorted(end-1));



                sub_ind = sub_ind+1;
                options.name = name;
                %f will contain the cartesian component of the cell mesh
                f_disp = f1;
                %gives the number to add to the computed indexes to reach
                %the right class
                add=obs(1)-1;


                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                end

                hold on;

                fw_clean_high=diag(cluster(:,high))*fwavTot;

                fw_clean_high_sorted=sort(fw_clean_high(fw_clean_high~=0));
                th_max=fw_clean_high_sorted(1);

                fw_clean_mid=diag(cluster(:,mid))*fwavTot;
                fw_clean_mid_sorted=sort(fw_clean_mid(fw_clean_mid~=0));
                th_min=fw_clean_mid_sorted(1);

                hold on;
                fplot=fwavTot;
                fplot(fwavTot<0)=0;
                plot(fplot,'-b');
                hold on
                plot(-fw_clean_high,'-r');
                plot(-fw_clean_mid,'-k');
                plot(th_max*ones(1,length(vertex{j})-add),'-g');
                plot(th_min*ones(1,length(vertex{j})-add),'-g');

                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind+6);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind+6);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end                        
                end

                hold on
                %we plot the mesh of the amoeba cell        
                plot_mesh(f_disp,face{j+1},options); shading faceted; axis tight;

                %we plot the class of each point (only the two highest class in
                %terms of values (n and n-1))
                for class=[high,mid]
                    %find the position of points to display in the TOTAL
                    %coefficient vector.
                    pos_vec=find(cluster(:,class)')+add;
                    if class==high 
                        plot3( f_disp(pos_vec,1),f_disp(pos_vec,2),f_disp(pos_vec,3),'*','Color',colorstyle{1},'lineWidth',3);
                    else
                        plot3( f_disp(pos_vec,1),f_disp(pos_vec,2),f_disp(pos_vec,3),'*','Color',colorstyle{13},'lineWidth',3);
                    end
                end

            end

            %giving a title to the graph
            name =  d(num_calc(1)).name ;
            name2 = name(1:length(name)-20);
            suplabel(['k-mean clustering beginning at the frame ' name2],'t');


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end

        if otsu      
            %%%%%%%%%%%%%%%%%%%%%% OTSU ANALYSIS %%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            otsu_fig=figure();

            %subplot index
            sub_ind=0;
            for idx=num_calc

                % load the file
                name =  d(idx).name ;
                load([Folder,'/',name]);
%                 load('../results/wavelet_coefs/Lazy_wavelet/06-01-10_WT 1H_#0000_T0011_CALD_LSF_SphWav.mat');

                disp(['The following cell is gonna be Otsued : ' name]); 

                % Computation of the sum of the square of the components to get one
                % unique descriptor vector rotation independant:
                obs = Level{j};
                fwav1 = fw(obs,:);

                % Computation of the sum of the square of the components to get one
                % unique descriptor vector :
                fwavTot = fwav1(:,1).^2 + fwav1(:,2).^2 + fwav1(:,3).^2;

                    if j>3
                        %if the level of interest is above 3 we have to supress
                        %penton influence
                        out_vtx=Outvtx(vertex,face,j);

                        fwavTot(out_vtx-Level{j-1}(end))=0;
                    end

                % Apply a threshold on the spherical wavelet coefficients to highlight the
                % features of interests:

                % Definition of the thresholds computed from otsu's method with three classes:
                [~, th] = thresh_signal(fwavTot,10,3);
                th_max = max(th);
                th_min = min(th);

                %gives the number to add to the computed indexes to reach
                %the right class
                add=obs(1)-1;

                % Find the coefficients above this threshold
                % (unused in the current code)
                %Spe_max =  find(fwavTot > th_max);

                % Find the coefficients between these thresholds
                % (unused in the current code)
                %Spe_min = find((fwavTot <= th_max).*(fwavTot > th_min));

                % Find the coefficients under the highest threshold
                Spe_exclu =  fwavTot <= th_max;


                sub_ind = sub_ind+1;
                options.name = name;
                %f will contain the cartesian component of the cell mesh                
                f_disp = f1;  

                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                end

                hold on;
                fplot=fwavTot;
                fplot(fwavTot<0)=0;
                plot(fplot,'-b');
                hold on
                fw_clean=fwavTot;
                fw_clean(Spe_exclu)=0;
                plot(-fw_clean,'-r');
                plot(th_max*ones(1,length(vertex{j})-add),'-g');
                plot(th_min*ones(1,length(vertex{j})-add),'-g');


                %plot of the most important point with the
                %corresponding plotting on the 3d mesh

                if length(find(fw_clean))>14
                    [~,index_main]=sort(fw_clean);
                    for l=0:14
                        plot(index_main(end-l),fw_clean(index_main(end-l)),'.','Color',colorstyle{l+1,1});
                    end
                elseif find(fw_clean)
                    [~,index_main]=sort(fw_clean);
                    for l=0:length(find(fw_clean))-1
                        plot(index_main(end-l),fw_clean(index_main(end-l)),'.','Color',colorstyle{l+1,1});                                             
                    end
                end

                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind+6);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind+6);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end                        
                end

                %plot the amoeba surface
                if j~=6
                    plot_mesh(f_disp,face{j+1},options); shading faceted; axis tight;        
                else
                    plot_mesh(f_disp,face{j},options); shading faceted; axis tight;        

                end
                hold on;

                % Highlight the corresponding vertices on the corresponding surface level:
                if length(find(fw_clean))>14
                    [fw_clean_sorted,index_main]=sort(fw_clean);
                    for l=0:14
                        plot3( f_disp(index_main(end-l)+add,1),f_disp(index_main(end-l)+add,2),...
                    f_disp(index_main(end-l)+add,3),'*','Color',colorstyle{l+1,1},'lineWidth',3);
                    end
                    if length(find(fw_clean))>15
                        index_least=find(fw_clean<fw_clean_sorted(end-14) & fw_clean>0);
                        plot3( f_disp(index_least+add,1),f_disp(index_least+add,2),...
                            f_disp(index_least+add,3),'*','Color',colorstyle{15,1},...
                            'lineWidth',3);
                    end
                elseif find(fw_clean)
                    [~,index_main]=sort(fw_clean);
                    for l=0:length(find(fw_clean))-1
                        plot3( f_disp(index_main(end-l)+add,1),f_disp(index_main(end-l)+add,2),...
                    f_disp(index_main(end-l)+add,3),'*','Color',colorstyle{l+1,1},'lineWidth',3);
                    end
                end
            end

            %giving a title to the graph
            name =  d(num_calc(1)).name ;
            name2 = name(1:length(name)-20);
            suplabel(['Otsu thresholding beginning at the frame ' name2],'t');

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end

        if wat
            %%%%%%%%%%%%%%%%%%%%%% WAT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            wat_fig=figure();

            %subplot index
            sub_ind=0;
            for idx=num_calc

                % load the file

                name =  d(idx).name ;
%                 load([Folder,'/',name]);
                load('../results/wavelet_coefs/Lazy_wavelet/06-01-10_WT 1H_#0000_T0011_CALD_LSF_SphWav.mat');

                disp(['The following cell is gonna be wated : ' name]); 



                obs = Level{j}; 
                add = obs(1)-1;
                fwav = fw(obs,:);


                % Computation of the sum of the square of the components to get one
                % unique descriptor vector :
                fwavTot = fwav(:,1).^2 + fwav(:,2).^2 + fwav(:,3).^2;

                if j>3
                    %if the level of interest is above 3 we have to supress
                    %penton influence
                    out_vtx=Outvtx(vertex,face,j);

                    fwavTot(out_vtx-Level{j-1}(end))=0;
                end

                % Definition of the thresholds computed from WAT method :
                [fw_clean,thresh] = WAT(fwavTot,Level,add,j,k_wat,0);

                sub_ind = sub_ind+1;
                options.name = name;
                %f will contain the cartesian component of the cell mesh                
                f_disp = f1;
                %gives the number to add to the computed indexes to reach
                %the right class
                add=obs(1)-1;

                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6))+mod(sub_ind,6));
                    end
                end

                hold on;
                fplot=fwavTot;
                fplot(fwavTot<0)=0;
                plot(fplot,'-b');
                hold on
                plot(-fw_clean,'-r');
                plot(thresh*ones(1,length(vertex{j})-add),'-g');

                %plot of the most important point with the
                %corresponding plotting on the 3d mesh
                if length(find(fw_clean))>14
                    [~,index_main]=sort(fw_clean);
                    for l=0:14
                        plot(index_main(end-l),fw_clean(index_main(end-l)),'.','Color',colorstyle{l+1,1});
                    end
                elseif find(fw_clean)
                    [~,index_main]=sort(fw_clean);
                    for l=0:length(find(fw_clean))-1
                        plot(index_main(end-l),fw_clean(index_main(end-l)),'.','Color',colorstyle{l+1,1});                                             
                    end
                end

                if length(num_calc)>6                                           
                    if sub_ind==6 || sub_ind==12
                        subplot(4,6,sub_ind+6);
                    else
                        subplot(4,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end
                else
                    if sub_ind==6
                        subplot(2,6,sub_ind+6);
                    else
                        subplot(2,6,6*(2*floor(sub_ind/6)+1)+mod(sub_ind,6));
                    end                        
                end

                %plot the amoeba surface
                plot_mesh(f_disp,face{j+1},options); shading faceted; axis tight;        
                hold on;

                % Highlight the corresponding vertices on the corresponding surface level:
                if length(find(fw_clean))>14
                    [fw_clean_sorted,index_main]=sort(fw_clean);
                    for l=0:14
                        plot3( f_disp(index_main(end-l)+add,1),f_disp(index_main(end-l)+add,2),...
                    f_disp(index_main(end-l)+add,3),'*','Color',colorstyle{l+1,1},'lineWidth',3);
                    end
                    if length(find(fw_clean))>15
                        index_least=find(fw_clean<fw_clean_sorted(end-14) & fw_clean>0);
                        plot3( f_disp(index_least+add,1),f_disp(index_least+add,2),...
                            f_disp(index_least+add,3),'*','Color',colorstyle{15,1},...
                            'lineWidth',3);
                    end
                elseif find(fw_clean)
                    [~,index_main]=sort(fw_clean);
                    for l=0:length(find(fw_clean))-1
                        plot3( f_disp(index_main(end-l)+add,1),f_disp(index_main(end-l)+add,2),...
                    f_disp(index_main(end-l)+add,3),'*','Color',colorstyle{l+1,1},'lineWidth',3);
                    end
                end
            end            

            %giving a title to the graph            
            name =  d(num_calc(1)).name ;
            name2 = name(1:length(name)-20);
            suplabel(['WAT thresholding beginning at the frame ' name2],'t');


            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end

        %saving all needed graphs
        clk=clock;

        if k_mean
            saveas(k_mean_fig,['../rec_graph/' name2 '_K-means_analysislevel_' num2str(j)...
                    '_recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4)) ...
                    'h' num2str(clk(5)) ], 'fig');
        end
        if otsu
            saveas(otsu_fig,['../rec_graph/' name2 '_Otsu_analysislevel_' num2str(j)...
                '_recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4))...
                'h' num2str(clk(5)) ], 'fig');
        end
        if wat
            saveas(wat_fig,['../rec_graph/' name2 '_WAT_analysislevel_' num2str(j)...
                '_recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4))...
                'h' num2str(clk(5)) ], 'fig');
        end
    end
end
end
