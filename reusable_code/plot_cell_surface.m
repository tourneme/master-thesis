function plot_cell_surface(num,mtx)
% function subplotting  length(num) cell surfaces num in a mtx*mtx matrix.
% The subplot is based on the wavelet reconstructed points.

global RELATIV_ROOT
base_path=[RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/'];
path(path, [base_path 'toolbox/']);
Folder = [RELATIV_ROOT 'DATA/data_sph'];
names=dir2cell(Folder);

%index of subplot
i=0; 

figure;
%used for the naming in the save function
fr_init=num(1);

if length(num)<=25
    frames=num;
    fr_end=num(end);    
else
    frames=num(1:25);
    fr_end=num(25);   
end

if size(frames,1)>1
    frames=frames';
end

h=zeros(1,length(frames));

for idx=frames   
    i=i+1;
    % load the file
    subject =  names{idx} ;

    load([Folder,'/',subject]);
    disp(subject); 

    options.name = subject;
    h(i)=subplot(mtx,mtx,i);    
    %we plot the mesh of the amoeba cell    

	mean_vert=mean(vertices,1);
	vertices(:,1)=vertices(:,1)-mean_vert(1);
	vertices(:,2)=vertices(:,2)-mean_vert(2);
	vertices(:,3)=vertices(:,3)-mean_vert(3);
	plot_mesh(vertices,faces,options);
	shading faceted;
	text(15,15,15,['T ' subject(24:25)],'FontSize',14);
    axis tight;
end

hlink = linkprop(h,{'CameraPosition','CameraUpVector'});
key = 'graphics_linkprop';
% Store link object on first subplot axes
setappdata(h(1),key,hlink);

[cells_name,cells_base_name]=cells_naming(names,fr_init,fr_end);
suplabel(cells_base_name,'t');

clk=clock;
saveas(gcf,[RELATIV_ROOT 'rec_graph/' cells_name '  recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4)) 'h' num2str(clk(5)) ], 'fig');
end