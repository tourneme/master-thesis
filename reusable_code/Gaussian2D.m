function value = Gaussian2D(k,sigma1,sigma2,theta,phi)
% calculate gausian over a sphere using stereographic projection
%theta runs along 0 and pi and phi along -pi and pi

value=k.*exp(-((2*cos(phi).*...
	tan(theta/2)).^2./(2*sigma1^2)+(2*sin(phi).*tan(theta/2)).^2./(2*sigma2^2)));


end
