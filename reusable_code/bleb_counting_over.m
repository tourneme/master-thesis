function [bleb_number,bleb_list]=bleb_counting_over(fw,faces,varargin)
bleb_list=zeros(length(fw),1);
bleb=0;
%start at two because 1 is modified in the previous code for scaling
%purpose.
for i=2:length(fw)
	if fw(i)>0 & bleb_list(i)==0
		bleb=bleb+1;
		bleb_list(i)=bleb;
		bleb_list=areNeighborsPartof(i,fw,faces,bleb,bleb_list,varargin{1});
	end
end
bleb_number=bleb;
end