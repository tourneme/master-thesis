function [ Out_vtx ] = findNeighbors(vertices,faces)

Out_vtx=[];

for v = 1:length(vertices)
	[~,J] = find(faces == vertices(v));
	for n = 1: length(J)
		for i=1:3
			if isempty(find(Out_vtx==faces(i,J(n)), 1))
				if faces(i,J(n))~=vertices(v)
					Out_vtx=[Out_vtx, faces(i,J(n))];
				end
			end
		end
	end
end

end