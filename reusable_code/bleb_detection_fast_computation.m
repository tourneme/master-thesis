function bleb_detection_fast_computation(cells,varargin)
%rapid analysis + excel creation

global RELATIV_ROOT

%%%%%%%%%%%%%%%%%%    DATA preperation    %%%%%%%%%%%%%%%%%%%%
%%
BW=varargin{1};
max_invertibility=varargin{2};
kernel=varargin{3};
kernel1=num2str(kernel(1));
kernelend=num2str(kernel(end));
sigma=varargin{4};
			
Folder_data_pre= [RELATIV_ROOT 'results/overcomplete_coef/LoG/BW' ...
				num2str(BW) 'max_invert'  num2str(max_invertibility) ...
				'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
				'sigma' num2str(sigma)];

for k=1:length(cells)

	switch cells{1}.on
		case 1
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph' ];	
			Folder_data=[Folder_data_pre '/CALD'];
		case 2
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph_gu/results' ];
			Folder_data=[Folder_data_pre '/gu'];	
		case 3
			
			Folder_names =[ RELATIV_ROOT 'DATA/data_sph_freesurfer/results' ];
			Folder_data=[Folder_data_pre '/freesurfer'];		
		case 4
			
			Folder_names = [RELATIV_ROOT 'DATA/data_sph_simu/LoG/BW' ...
				num2str(BW) 'max_invert'  num2str(max_invertibility) ...
				'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
				'sigma' num2str(sigma)];
			Folder_data= [RELATIV_ROOT 'results/overcomplete_coef_simu/LoG/BW' ...
				num2str(BW) 'max_invert'  num2str(max_invertibility) ...
				'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
				'sigma' num2str(sigma)];
	end
% 	%getting the line number of the frame to analyze. num_name reaches the
% 	%spherical parameterization
% 	if cells{1}.on==4
% 		%getting the line number of the frame to analyze. num_name reaches the
% 		%spherical parameterization
% 		num_name=getNum(cells{k},Folder_names,'simu');
% 		type='simu_amoeba_';
% 	else
% 		num_name=getNum(cells{k},Folder_names,'');
% 		type='amoeba_';
% 	end
% 	%num_data reaches the wavelet coefficients
% 	num_data=getNum(cells{k},Folder_data,type);

names=dir2cell(Folder_names);
names_data=dir2cell(Folder_data);

%%%%%%%%%%%%%%%%%%%WARNING FOR THE SIMULATION
num_data=4:length(names_data)-2;

fid = fopen([RELATIV_ROOT 'results/numeric/simu.csv'],'w');
		fprintf(fid,'cell name;gaussian number;number of blebs mini;number of blebs maxi;number of blebs (overcomplete method);is good\n');

%i is the level of analysis
for i=3
	%j the data to analyze
    for j=num_data
        name =  names_data{j};
        disp(name);
        load([Folder_data,'/',name]);
        a=analysis_cell{i};
        b=recon_cell{i};
        t=sqrt(sum(a.vertices.^2,1));
        


        %applying a certain thresholding method
        fw_final=thresholding(t,i,2);
		

		% counting blebs
		[bleb_number,bleb_list]=bleb_counting_over(fw_final,b.faces,0);
		
		ref_numb1=str2num(name(25:26));
		ref_numb2=str2num(name(28:29));
		gaus=str2num(name(17:18));
		if bleb_number<=ref_numb2 & bleb_number>=ref_numb1
			good=1;
		else
			good=0;
		end
		
		fprintf(fid,'%s;%u;%u;%u;%u;%u\n',name,gaus,ref_numb1,ref_numb2,bleb_number,good);
        
	end
	fprintf(fid,'-----;-----;-----;-----;-----\n');
end
fclose(fid);

end