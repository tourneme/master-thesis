function bleb_list=areNeighborsPartof(vertex,fw,faces,bleb,bleb_list,varargin)

[Neighbor_list]=findNeighbors(vertex,faces);

while ~isempty(Neighbor_list)
	
	bleb_points= fw(Neighbor_list)>0;

	pre_Neighbor_list=Neighbor_list(bleb_points);
	
	idx_new_points=bleb_list(pre_Neighbor_list)==0;
	
	new_bleb_points=pre_Neighbor_list(idx_new_points);

	bleb_list(new_bleb_points)=bleb;

	Neighbor_list=findNeighbors(new_bleb_points,faces);
	
	
end
if varargin{1}
	disp([num2str(bleb) ' bleb detected']);
end
end