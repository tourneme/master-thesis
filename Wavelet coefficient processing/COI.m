function cells=COI

k=1;
cells{k}.on=1;

%% 06-01-2010 WT    30min

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=21:25; %0-90
k=k+1; 


cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=78; %0-90
k=k+1;


%% 06-01-2010 WT    1h

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='00';
cells{k}.frames=0:4; %0-91
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='WT_';
cells{k}.time='1h00';
cells{k}.number='09';
cells{k}.frames=30; %2-91
k=k+1;

%% 06-01-2010 E64   30min
 
cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='04';
cells{k}.frames=8:15; %0-70
k=k+1;

%% 06-01-2010 E64   1h30   S2

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=32:42; %4-82
cells{k}.set=2;
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=32:38; %4-91
cells{k}.set=2;
k=k+1;

cells{k}.date='06-01-10';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='07';
cells{k}.frames=6; %4-91
cells{k}.set=2;
k=k+1;

%% 19-06-2009 E64   1h30

cells{k}.date='19-06-09';
cells{k}.type='E64';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=2; %0-9
k=k+1;

%% 07-01-2010 WT    1h30

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='00';
cells{k}.frames=[17,19]; %0-81
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='01';
cells{k}.frames=82; %1-83
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='02';
cells{k}.frames=04:10; %0-83
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='03';
cells{k}.frames=62:67; %17-82
k=k+1;
 
cells{k}.date='07-01-10';
cells{k}.type='WT_';
cells{k}.time='1h30';
cells{k}.number='06';
cells{k}.frames=27:37; %0-82
k=k+1;

%% 07-01-2010 E64   30min

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='00';
cells{k}.frames=[34,59]; %0-81
k=k+1;
 
cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='02';
cells{k}.frames=31; %0-91
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='06';
cells{k}.frames=0:15; %0-91
k=k+1;

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='0h30';
cells{k}.number='08';
cells{k}.frames=74; %66-91
k=k+1;

%% 07-01-2010 E64   1h

cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='02';
cells{k}.frames=7:16; %0-34
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='03';
cells{k}.frames=27:31; %0-91
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='04';
cells{k}.frames=24; %0-36
k=k+1;


cells{k}.date='07-01-10';
cells{k}.type='E64';
cells{k}.time='1h00';
cells{k}.number='05';
cells{k}.frames=5; %0-91
k=k+1;

%% 15-01-2010 WT    30min


cells{k}.date='15-01-10';
cells{k}.type='WT_';
cells{k}.time='0h30';
cells{k}.number='01';
cells{k}.frames=77; %0-90
k=k+1;