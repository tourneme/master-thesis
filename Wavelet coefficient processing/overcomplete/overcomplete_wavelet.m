function [num,cells]=overcomplete_wavelet(levels,cells,varargin)
%
%This function handles the wavelet coefficient extraction from a surface
%and call the function for blab analysis called bleb_detection.m
%
%INPUT:
%   -levels: max scale till where you wantt to extract the coefficient and
%   make the analysis
%   -cells: array of cells (matlab type: you will have to take care of the 
%   ambiguity that can rise from the fact that in our experiment a cell is
%   at the same type our subject of interest and a basic matlab type). Each
%   cell of the array represents one cell containing multiple frame. For
%   the analysis, the list can be infinite, but for the analysis only one
%   cell with 6 frames can be taken into account (limit placed only to have
%   understandable data).
%   -varargin: containing parameters for the creation of new filters:
%       -BW: bandwith
%       -max_invertibility: SPHARM frequency till which the filters will be
%       invertible (??) trou de m?moire
%       -kernel_size:  dilation factor
%       -sigma: shape of the LoG
%
%OUTPUT
%   -the output are file and folder creation + cell visualization
%   -num, cells: arguments that have to be passed to the function
%   bleb_detection
%
%
%modifications list:
%
%-Robin Tournemenne 26/07/2013 "creation"
%-Robin Tournemenne 25/09/2013 cleaning

global RELATIV_ROOT

path(path, [RELATIV_ROOT 'Toolboxes_and_functions/toolbox_wavelet_meshes/toolbox/']);
path(path,[RELATIV_ROOT 'reusable_code']);
path(path,[RELATIV_ROOT 'Toolboxes_and_functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode']);

%creation of wavelets at every required scale:
%%
%security check parameter (mainly useless for rapid application)
check_bool=0;
kernel_sizes=varargin{3};
kernel1=num2str(kernel_sizes(1));
kernelend=num2str(kernel_sizes(end));

if ~exist([RELATIV_ROOT 'results/axisymmetric_filter/LoG/LaplacianBW' num2str(varargin{1}) 'inv'...
		num2str(varargin{2}) 'kernel' kernel1(3:end) 'to' ...
		kernelend(3:end) 'sigma' num2str(varargin{4}) '.mat'],'file')
	
	%create filters if not already existing
	CreateLaplacianAnalysisSynthesisFilters(1,varargin{1:end});
end

%load the filters used for the wavelet transform
load([RELATIV_ROOT 'results/axisymmetric_filter/LoG/LaplacianBW' num2str(varargin{1}) 'inv'...
	num2str(varargin{2}) 'kernel' kernel1(3:end) 'to' ...
	kernelend(3:end) 'sigma' num2str(varargin{4}) '.mat']);
%%

%prepare data gathering
%%
max_decomposition=max(levels);

switch cells{1}.on
		
	case 1
		num=[];
		Folder =[ RELATIV_ROOT 'DATA/data_sph' ];
		
		
		kernel1=num2str(varargin{3}(1));
		kernelend=num2str(varargin{3}(end));
		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/overcomplete_coef/LoG/BW' ...
			num2str(varargin{1}) 'max_invert'  num2str(varargin{2}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{4}) '/CALD'];
		
		for j=1:length(cells)
			%retrieve the line numbe of each cell to analyze at every frame
			num=[num getNum(cells{j},Folder,'')];
		end
		
	case 2
		num=[];
		
		Folder =[ RELATIV_ROOT 'DATA/data_sph_gu/results' ];
		
		
		kernel1=num2str(varargin{3}(1));
		kernelend=num2str(varargin{3}(end));
		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/overcomplete_coef/LoG/BW' ...
			num2str(varargin{1}) 'max_invert'  num2str(varargin{2}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{4}) '/gu' ];
		
		for j=1:length(cells)
			
			for jj=1:length(cells{j}.frames)
				currentName=cell_naming(cells{j},cells{j}.frames(jj),'');
				if ~exist([Folder '/' currentName(1:end-4) '.mat'],'file')
					if ~exist([Folder(1:end-8) '/' currentName(1:end-4) '.m'],'file')
						guParam(1,0,currentName(1:end-4));
					end
				end
			end
			
		end
		
		for j=1:length(cells)
			
			for jj=1:length(cells{j}.frames)
				currentName=cell_naming(cells{j},cells{j}.frames(jj),'');
				if ~exist([Folder '/m/' currentName(1:end-4) '.sphere.m'],'file')
						error('now you must undertake the Gu parameterization on a Windows computer cause it lacks a .sphere.m');
				end
				if ~exist([Folder '/' currentName(1:end-4) '.mat'],'file')
					guParam(0,1,currentName(1:end-4));
				end
			end
		end
			for j=1:length(cells)
				%retrieve the line numbe of each cell to analyze at every frame
				num=[num getNum(cells{j},Folder,'')];
			end
			
	case 3
		num=[];
		
		Folder =[ RELATIV_ROOT 'DATA/data_sph_freesurfer/results' ];
		
		
		kernel1=num2str(varargin{3}(1));
		kernelend=num2str(varargin{3}(end));
		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/overcomplete_coef/LoG/BW' ...
			num2str(varargin{1}) 'max_invert'  num2str(varargin{2}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{4}) '/freesurfer' ];
		
		for j=1:length(cells)
			
			for jj=1:length(cells{j}.frames)
				currentName=cell_naming(cells{j},cells{j}.frames(jj),'');
				if ~exist([Folder '/' currentName(1:end-4) '.mat'],'file')
					freesurferParam(currentName(1:end-4));
				end
			end
			
		end
		
		for j=1:length(cells)
			%retrieve the line numbe of each cell to analyze at every frame
			num=[num getNum(cells{j},Folder,'')];
		end
	
	case 4
		num=[];
		
		kernel1=num2str(varargin{3}(1));
		kernelend=num2str(varargin{3}(end));
			
		Folder =[ RELATIV_ROOT 'DATA/data_sph_simu/LoG/BW' ...
			num2str(varargin{1}) 'max_invert'  num2str(varargin{2}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{4}) ];
		
		%folder where to put the wavelet coefficients
		Folder_final=[RELATIV_ROOT 'results/overcomplete_coef_simu/LoG/BW' ...
			num2str(varargin{1}) 'max_invert'  num2str(varargin{2}) ...
			'kernel_from_' kernel1(3:end) '_to_' kernelend(3:end) ...
			'sigma' num2str(varargin{4}) ];
		
		% 		figure;
		for j=1:length(cells)
			%retrieve the line numbe of each cell to analyze at every
			%frame
			if cells{j}.gaus==1
				type='simu_sigma';
			else
				type='simu';
			end
			if ~isfield(cells{j},'frames')
				if length(cells)>9
					fast=1;
				else
					fast=0;
				end
				[cells{j}.minblebs,cells{j}.maxblebs,cells{j}.frames]=compute_simulated_signals(cells{j},...
					cells{1}.freesurfer,fast,length(cells),j,varargin{1},varargin{2},varargin{3},varargin{4});
			end
		end
		%at the end (cause the list is dynamic), finding index in the folder:
		for j=1:length(cells)
			if cells{j}.maxblebs==0
				%to avoid problem with cells not having bleb enough in
				%comparison with gaussian number
				continue;
			end
			num=[num getNum(cells{j},Folder,type)];
		end
		
end

names=dir2cell(Folder);
% wavelet coefficients calculation
%%
for i=num
	
	%we are checking that data are not already calculated
	if cells{1}.on==1 | cells{1}.on==2 | cells{1}.on==3 | cells{1}.on==4
		
		currentName=names{i};
		if exist(Folder_final,'dir')
			names_final=dir2cell(Folder_final);
			type_final='amoeba_';
			if find(strcmp([type_final currentName],names_final))
				disp([currentName(1:end-4) '  has already been waveleted with the filter LoG: BW '...
					num2str(varargin{1}) ' max_invert '  num2str(varargin{2}) ...
					' kernel_from ' kernel1(3:end) ' to ' kernelend(3:end) ...
					' sigma ' num2str(varargin{4})]);
				continue;
			end
		else
			mkdir(Folder_final);
		end
	end
	
	%loading surface data (actual mesh and spherical mesh)
	name =  names{i};
	disp(name);
	load([Folder,'/',name]);
	
	disp('Performing Wavelet Decomposition');
	
	%%%%%%%%%%% DON'T TOUCH MY BANANA%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	vertices=vertices-repmat(mean(vertices,1),length(vertices),1);
	
	%computing pretreatments
	vertexFaces =  MARS_convertFaces2FacesOfVert(int32(faces), int32(size(sph_verts, 1)));
	num_per_vertex = length(vertexFaces)/size(sph_verts,1);
	vertexFaces = reshape(vertexFaces, size(sph_verts,1), num_per_vertex);
	
	vertexNbors = MARS_convertFaces2VertNbors(int32(faces), int32(size(sph_verts,1)));
	num_per_vertex = length(vertexNbors)/size(sph_verts,1);
	vertexNbors = reshape(vertexNbors, size(sph_verts,1), num_per_vertex);
	
	sbjMesh.vertices=sph_verts';
	sbjMesh.metricVerts=vertices';
	sbjMesh.faces=faces';
	sbjMesh.vertexFaces=vertexFaces';
	sbjMesh.vertexNbors=vertexNbors';
	
	if exist('fvec','var')
		sbjMesh.fvec=fvec;
	end
	
	%calculation of coefficients
	[recon_cell, analysis_cell] = PartiallyReconMeshModif(sbjMesh, analysis_filters_my, synthesis_filters_my, check_bool,max_decomposition,0,1,7);
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	%saving files
	save([Folder_final '/amoeba_' currentName],'recon_cell','analysis_cell');
end

end