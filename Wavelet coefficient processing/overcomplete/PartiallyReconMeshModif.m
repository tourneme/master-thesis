% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
%
% This function decomposes a closed 2D subject mesh in a multiresolution fashion.  
% The bulk of the work is done by WaveletTransformMesh.m
%
% WaveletTransformMesh returns the outputs of the analysis filters (wc_x, wc_y, wc_z) 
% and the outputs of the synthesis filters (rc_x,rc_y, rc_z)
%
% Since wc_x, wc_y, wc_z and rc_x,rc_y, rc_z are expressed in spherical harmonic coefficients, the function goes on to
% perform an inverse spherical harmonic transform and returns a series of partially reconstructed surfaces 
% and the wavelet coefficients at each spatial level.
%
%This version has been slightly modified by Robin Tournemenne in order to

%
%modification list: 
%07/16/2013 Robin Tournemenne:   undertake the wavelet decomposition only untill a certain level and not on
%the total number of available filters
%
%01/08/2013 Robin Tournemenne: old_tech variable created to use the true
%power of SPHARM+icosahedrons (i also created RobinSPHARMformat.m): 
%
%old_tech=0 AND interpol=0 is completely equivalent to the previous
%technique and takes a lot less computationnal time.



function [recon_cell, analysis_cell] = PartiallyReconMeshModif(MARS_sbjMesh, analysis_filters, synthesis_filters, check_bool, max_decomposition,old_tech,interpol,icosa)

% [recon_cell] = PartiallyReconMesh(MARS_sbjMesh, analysis_filters,
% synthesis_filters)

global RELATIV_ROOT;
path(path, [RELATIV_ROOT 'Toolboxes_and_functions/SPHARM-MAT-v1-0-0/code/']);


if(size(analysis_filters, 2) ~= size(synthesis_filters, 2))
   error('We expect analysis and synthesis filters to be of same bandwidth');
end

if(size(analysis_filters, 1) ~= size(synthesis_filters, 1))
   error('We expect the number of analysis and synthesis filters to be the same');
end

%part of the modified code taking into account only the first filters. 
if ~exist('max_decomposition','var')
    num_filters = size(analysis_filters, 1);
else
    num_filters=max_decomposition;
end


% wavelet transformation

[wc_x, wc_y, wc_z, rc_x, rc_y, rc_z] = WaveletTransformMesh(MARS_sbjMesh, analysis_filters, synthesis_filters, check_bool);

% reconstruct brain
coeffs_mat_x = zeros(size(rc_x));
coeffs_mat_y = zeros(size(rc_x));
coeffs_mat_z = zeros(size(rc_x));
recon_cell = cell(num_filters, 1);
sbjMesh = MARS_sbjMesh;
object_mesh=sbjMesh.vertices;

BW = 1/2 *(-1 + sqrt(1+8*size(analysis_filters, 2)));

%the following part, do an inverse SPHARM to go back in the spherical
%domain for the wavelet coefs and for the reconstructed brain

if old_tech
    % Compute theta,phi of lat-lon grid
    [phi, theta] = sphgrid(BW*2);
    phi = [phi 2*pi*ones(size(phi, 1), 1)];
    phi = [phi(1,:)   ; phi;  phi(1,:)]; 
    theta = [theta theta(:, 1)];
    theta = [zeros(1, size(theta, 2)) ; theta; pi*ones(1, size(theta, 2))];

    % Compute theta,phi of cortical surface mesh (which had been spherically parameterized)
    radius = sqrt(sum(MARS_sbjMesh.vertices.^2, 1));
    offset_vec = zeros(1, size(MARS_sbjMesh.vertices, 2)); 
    offset_vec(abs(MARS_sbjMesh.vertices(3, :) - 100) < 1e-5) = 1e-5; % These offsets arise due to numerical issues so that we won't get cos or sin larger than 1
    offset_vec(abs(MARS_sbjMesh.vertices(3, :) + 100) < 1e-5) = -1e-5;% Or else I will get complex numbers for theta and phi.
    theta_vec = mod(single(acos(MARS_sbjMesh.vertices(3,:)./radius)) + offset_vec, single(pi));
    phi_vec = mod(single(atan2(MARS_sbjMesh.vertices(2,:),MARS_sbjMesh.vertices(1,:))), single(2*pi));
else
    if interpol
        if ~exist([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_' num2str(icosa) '.mat'],'file')
            options.use_color = 1;
			options.rho = .3;
			options.color = 'rescale';
			options.use_elevation = 0;
			% options for the multiresolution mesh
			options.base_mesh = 'ico';
			options.relaxation = 1;
			options.keep_subdivision = 1;
            [vertex,face] = compute_semiregular_sphere(7,options);

            sph_verts = vertex{end}';
            faces = face{end}';
        else
            %calculated thanks to the file test_geticosa.m
            load([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_' num2str(icosa)]);
        end
        
        if ~exist([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_' num2str(icosa) '_SPHARMed.mat'],'file')
            Z = calculate_SPHARM_basis(sph_verts, BW-1);
            %Z gives Y00 Y-11 Y01 Y11 Y-22 Y-12 Y02 Y12 Y22...
        else
            load([RELATIV_ROOT 'results/subdivided_icosahedron/icosa_' num2str(icosa) '_SPHARMED.mat']);
        end
        
        sbjMesh.faces = faces';
        
    else
        Z=calculate_SPHARM_basis(object_mesh', BW-1); 
    end


end

for i = 1:num_filters
    
%     disp(['Performing Inverse Spherical Harmonic Transform of Wavelet Coefficients level ' num2str(i) ' followed by interpolation onto original mesh points']);
    
    if ~old_tech
        
        wc=[wc_x(i,:)',wc_y(i,:)',wc_z(i,:)'];
        
		wc=RobinSPHARMformat(wc);    
        vertices = real(Z*wc);
        sbjMesh.vertices = vertices';
    
    else 
        mat = ConvertMyFormat2S2kitFormat(transpose(wc_x(i, :)));
        mesh_s2kit_coeff_x = s2ifst(mat);
        mesh_s2kit_coeff = [mesh_s2kit_coeff_x mesh_s2kit_coeff_x(:, 1)];
        mesh_s2kit_coeff = [repmat(mean(mesh_s2kit_coeff(1,:)), 1, size(mesh_s2kit_coeff, 2)); mesh_s2kit_coeff; repmat(mean(mesh_s2kit_coeff(end,:)), 1, size(mesh_s2kit_coeff, 2))];
        sbjMesh.vertices(1, :) = MARS_interp2(phi, theta, real(mesh_s2kit_coeff), phi_vec, theta_vec, '*linear');

        mat = ConvertMyFormat2S2kitFormat(transpose(wc_y(i, :)));
        mesh_s2kit_coeff_y = s2ifst(mat);
        mesh_s2kit_coeff = [mesh_s2kit_coeff_y mesh_s2kit_coeff_y(:, 1)];
        mesh_s2kit_coeff = [repmat(mean(mesh_s2kit_coeff(1,:)), 1, size(mesh_s2kit_coeff, 2)); mesh_s2kit_coeff; repmat(mean(mesh_s2kit_coeff(end,:)), 1, size(mesh_s2kit_coeff, 2))];
        sbjMesh.vertices(2, :) = MARS_interp2(phi, theta, real(mesh_s2kit_coeff), phi_vec, theta_vec, '*linear');

        mat = ConvertMyFormat2S2kitFormat(transpose(wc_z(i, :)));
        mesh_s2kit_coeff_z = s2ifst(mat);
        mesh_s2kit_coeff = [mesh_s2kit_coeff_z mesh_s2kit_coeff_z(:, 1)];
        mesh_s2kit_coeff = [repmat(mean(mesh_s2kit_coeff(1,:)), 1, size(mesh_s2kit_coeff, 2)); mesh_s2kit_coeff; repmat(mean(mesh_s2kit_coeff(end,:)), 1, size(mesh_s2kit_coeff, 2))];
        sbjMesh.vertices(3, :) = MARS_interp2(phi, theta, real(mesh_s2kit_coeff), phi_vec, theta_vec, '*linear');
    end
    
    analysis_cell{i} = sbjMesh;
end


for i = 1:num_filters
    
%     disp(['Performing Inverse Spherical Harmonic Transform of Reconstruction level ' num2str(i) ' followed by interpolation onto original mesh points']);
    coeffs_mat_x(i, :) = sum(rc_x(1:i, :), 1);
    coeffs_mat_y(i, :) = sum(rc_y(1:i, :), 1);
    coeffs_mat_z(i, :) = sum(rc_z(1:i, :), 1);
    
    if ~old_tech
        
        rc=[coeffs_mat_x(i,:)',coeffs_mat_y(i,:)',coeffs_mat_z(i,:)'];
        rc=RobinSPHARMformat(rc);    
        vertices = real(Z*rc);
        sbjMesh.vertices = vertices';
    
    else 
        mat = ConvertMyFormat2S2kitFormat(transpose(coeffs_mat_x(i, :)));
        mesh_s2kit_recon_x = s2ifst(mat);
        mesh_s2kit_recon = [mesh_s2kit_recon_x mesh_s2kit_recon_x(:, 1)];
        mesh_s2kit_recon = [repmat(mean(mesh_s2kit_recon(1,:)), 1, size(mesh_s2kit_recon, 2)); mesh_s2kit_recon; repmat(mean(mesh_s2kit_recon(end,:)), 1, size(mesh_s2kit_recon, 2))];
        sbjMesh.vertices(1, :) = MARS_interp2(phi, theta, real(mesh_s2kit_recon), phi_vec, theta_vec, '*linear');

        mat = ConvertMyFormat2S2kitFormat(transpose(coeffs_mat_y(i, :)));
        mesh_s2kit_recon_y = s2ifst(mat);
        mesh_s2kit_recon = [mesh_s2kit_recon_y mesh_s2kit_recon_y(:, 1)];
        mesh_s2kit_recon = [repmat(mean(mesh_s2kit_recon(1,:)), 1, size(mesh_s2kit_recon, 2)); mesh_s2kit_recon; repmat(mean(mesh_s2kit_recon(end,:)), 1, size(mesh_s2kit_recon, 2))];
        sbjMesh.vertices(2, :) = MARS_interp2(phi, theta, real(mesh_s2kit_recon), phi_vec, theta_vec, '*linear');

        mat = ConvertMyFormat2S2kitFormat(transpose(coeffs_mat_z(i, :)));
        mesh_s2kit_recon_z = s2ifst(mat);
        mesh_s2kit_recon = [mesh_s2kit_recon_z mesh_s2kit_recon_z(:, 1)];
        mesh_s2kit_recon = [repmat(mean(mesh_s2kit_recon(1,:)), 1, size(mesh_s2kit_recon, 2)); mesh_s2kit_recon; repmat(mean(mesh_s2kit_recon(end,:)), 1, size(mesh_s2kit_recon, 2))];
        sbjMesh.vertices(3, :) = MARS_interp2(phi, theta, real(mesh_s2kit_recon), phi_vec, theta_vec, '*linear');
    end
    
    recon_cell{i} = sbjMesh;
end

