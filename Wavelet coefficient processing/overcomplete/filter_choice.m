function [BW, max_invertibility, kernel_sizes, sigma]=filter_choice
%
BW = 25;
max_invertibility = 25; %Degree above which we create a high pass filter.
kernel_sizes = [0.004*(2^7) 0.004*(2^6) 0.004*(2^5) 0.004*(2^4) 0.004*(2^3) 0.004*(2^2)]; %512 to 016 % number of filters will be equal to kernel_sizes + 1.
% % % % % kernel_sizes = [0.1920 0.2240 0.004*(2^5)]; % number of filters will be equal to kernel_sizes + 1.
% kernel_sizes =[0.006*(2^7) 0.004*(2^6) 0.16 0.004*(2^4) 0.004*(2^3) 0.004*(2^2)]; % 768 to 016 % le filtre num?ro 3 est le filtre retenu pour les analyses ? une ?chelle.

sigma=1;

end