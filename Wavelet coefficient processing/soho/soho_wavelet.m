function soho_wavelet(coef_extraction,J,r,num,plot_base_mesh,plot_recons,plot_repartition)

    % Application of the toolbox to three spherical functions describing the
    % surface of our amoebas. The three spherical functions correspond to the
    % evolution of the coordinnates X, Y and Z. 
    % This is one way of analyzing
    % the surface of our cells, another method should use the wavelet
    % transform on the meshed surface directly. 
    %
    % Wavelet Transform of Functions Defined on Surfaces
    % A wavelet transform can be used to compress a function defined on a
    % surface. Here we take the example of a 3D sphere. The wavelet transform
    % is implemented with the SOHO basis of Lessig, on icosahedron meshes
    %
    %for the analysis you need for num a vector of MAXIMUM 12 components.

base_path_1='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
path(path, base_path_1);
path(path, [base_path_1 'toolbox/']);
path(path, '../reusable_code');
base_path_2='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/';
path(path, [base_path_2 'utility/']);
path(path, [base_path_2 'rotation/']);
path(path, [base_path_2 'experiments/']);
path(path, [base_path_2 'dswt/']);
path(path, base_path_2);

clear options;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% COMPUTATION OF WAVELET COEFFICIENTS  %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if coef_extraction
    
    %%%%%%%%%%%%%%% COMPUTE THE SEMI-REGULAR SPHERE %%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % options for the display
    options.use_color = 1;
    options.rho = .3;
    options.color = 'rescale';
    options.use_elevation = 0;
    % options for the multiresolution mesh
    options.base_mesh = 'ico';
    options.relaxation = 1;
    options.keep_subdivision = 1;

    % basis defined over a partition derived from an octahedron
    platonic_solid = 'icosahedron';

    % other bases are 'bioh', 'pwh' (pseudo Haar), 'bonneau1', ...
    basis = 'osh';

    % get function handles for basis
    fhs = getFunctionHandlesBasis( basis); 


    tic;

    % construct forest of partition trees
    forest = getForestPlatonicSolid( platonic_solid, J, fhs.enforce_equal_area);
    
    % We can display the semi-regular sphere.
    if plot_base_mesh
        
%         selj = 1:J;
%         figure
%         for j=1:length(selj)
%             subplot(3,3,j);
%             plot_mesh(vertex{selj(j)},face{selj(j)}, options);
%             shading faceted; axis tight;
%             title(['Subdivision level ' num2str(selj(j))]);
%         end
%         colormap gray(256);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    

    %%%%%%% GET THE DATA AND PERFORM THE SPHERICAL WAVELET TRANSFORM %%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % load the file
    Folder = [base_path_2 'data_sph/'];
    
    %the following suffix helps to go to very interesting cells
    Folder=[Folder 'Amoebas treated/06012010_Wt-1H_smo/06012010_Wt-1H_#0009_smo/'];
    
    d = dir([Folder '*.mat']);
    
    %split_fr stands for splitting frame dividing the vector num into all the
    %analyzed cells
    split_fr=find(num(2:end)-num(1:end-1)~=1);
    split_fr=[0,split_fr];
    for i=1:length(split_fr)
        if split_fr==0
            num_calc=num;
        else
            if i==length(split_fr)
                num_calc=num(split_fr(i)+1:end);
            else
                num_calc=num(split_fr(i)+1:split_fr(i+1));
            end
        end
    
        for idx = num_calc
            name =  d(idx).name ;
            disp(name);
            disp('------------');
            disp(['percentage of coef retained : ' num2str(r*100) '%']);
            disp('------------');
            load([Folder name]);
%             load('../Toolboxes and functions/soho/data_sph/Amoebas treated/06012010_Wt-1H_smo/06012010_Wt-1H_#0009_smo/06012010_Wt-1H_#0009_T0030_CALD_smo.mat');

            signal.sp_vt=sph_verts;
            signal.faces=faces;
            signal.surf_vt=vertices;
            
            f=[];
            Mat_points=cell(J+1,1);
            for j=0:J
                
                sig_f=[];
                
                % sample signal onto the domains on the finest level of the partition trees
                [forest_sampled,faces_nb,Mat_points_new] = sampleSphericalObjoverMap( forest, signal, j, plot_repartition);
                                 
                Mat_points{j+1}=Mat_points_new;

                for m = 1 : numel( forest_sampled)
                    if j==0
                        sig_f = [sig_f getData(forest_sampled(m))]; 
                    else
                        sig_f=[sig_f getSignal(forest_sampled(m),j)];
                    end    
                end
                f=[f ;Mat_points{j+1}'*sig_f'];
            end
                
            
            % perform forward transform
            forest_analysed = dswtAnalyseFull( forest_sampled, J, ...
                                               fhs.filters_analysis, fhs.normalize);

            % find thresholds so that 512 coefficients are non-zero after approximation
            thresholds = getThresholdLargestK( forest_analysed, J, round(r*faces_nb), fhs.approx );

            % set all coefficients smaller than 'thresholds' to zero
            forest_approx = approxSWT( forest_analysed, J, thresholds, fhs.approx );

            % reconstruct the approximated signal
            forest_synth = dswtSynthesiseFull( forest_approx, J, ...
                                             fhs.filters_synthesis, fhs.denormalize, 0, 1);

            toc;                               

            % compute error
            [err_l1, err_l2, ~, sig_approx, sig_ref] = getError( forest_synth, forest_sampled, J);
            fprintf( 'L1 error: %f / %f / %f \n', err_l1(1),err_l1(2),err_l1(3));
            fprintf( 'L2 error: %f / %f / %f \n', err_l2(1),err_l2(2),err_l2(3));

            % display ref signal, values of
            % values of synthesis results and approx signal
            if plot_recons          
                
                figure;
        
                subplot(1,3,1);
%                 signal.surf_vt

                points_ref=sig_ref*Mat_points{J+1};

                plot_mesh(points_ref,signal.faces);shading faceted; axis tight;
                
                subplot(1,3,2);
                plotDataFast( forest_synth, J);shading faceted; 
                
                subplot(1,3,3);
                
                points_approx=sig_approx*Mat_points{J+1};   
                
                plot_mesh(points_approx,signal.faces);shading faceted; axis tight;
                
                suplabel(['J= ' num2str(J) ', number of retained coefs: ' num2str(r*100) '%, recons error L1: ' num2str(err_l1(1),2) '  /  ' num2str(err_l1(2),2) '  /  ' num2str(err_l1(3),2)],'t');

                clk=clock;            

                saveas(gcf,['../rec_graph/reconst_sf_soho_' name(1:end-13) '_recdate_' num2str(clk(3)) '-' num2str(clk(2)) '_' num2str(clk(4)) ...
                    'h' num2str(clk(5))],'fig');
            end

            f1=points_approx;
            
            
            
            for l=1:6    
                face{l}=signal.faces;
            end
        
            save(['../../results/wavelet_coefs/Soho_wavalet/',name(1:length(name)-23),'_SphWav.mat'],...
                'f','vertex','face','fw','fwT','f1','r');
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end



