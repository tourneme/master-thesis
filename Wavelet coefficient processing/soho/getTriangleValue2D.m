function [samples,data]=getTriangleValue2D(verts, map,visualize,plot_debug)

posmesh=verts;

posobj=map.sp_vt';

uv_mesh = mapPointFromSphereToTex( posmesh);

uv_obj = mapPointFromSphereToTex( posobj);

AB=[uv_mesh(1,2)-uv_mesh(1,1),uv_mesh(2,2)-uv_mesh(2,1)];
BC=[uv_mesh(1,3)-uv_mesh(1,2),uv_mesh(2,3)-uv_mesh(2,2)];
CA=[uv_mesh(1,1)-uv_mesh(1,3),uv_mesh(2,1)-uv_mesh(2,3)];
OM=uv_obj;
OA=[uv_mesh(1,1),uv_mesh(2,1)];
OB=[uv_mesh(1,2),uv_mesh(2,2)];
OC=[uv_mesh(1,3),uv_mesh(2,3)];

if abs(AB(1))>0.5
    lft_pt=min([OA(1),OB(1)]);

idx_data=find(((OM(1,:)-OA(1))/AB(1)-(OM(2,:)-OA(2))/AB(2))*((OC(1)-OA(1))/AB(1)-(OC(2)-OA(2))/AB(2))>=0 & ...
    ((OM(1,:)-OB(1))/BC(1)-(OM(2,:)-OB(2))/BC(2))*((OA(1)-OB(1))/BC(1)-(OA(2)-OB(2))/BC(2))>=0 & ...
((OM(1,:)-OC(1))/CA(1)-(OM(2,:)-OC(2))/CA(2))*((OB(1)-OC(1))/CA(1)-(OB(2)-OC(2))/CA(2))>=0);

if ~isempty(idx_data)
    data=sum(map.surf_vt(idx_data))/length(idx_data);
    samples=uv_obj(:,idx_data);
    
    if (uv_obj(1,idx_data(1))<0.9997 & uv_obj(2,idx_data(1))>0.9990 & uv_obj(2,idx_data(1))<0.3420 & uv_obj(2,idx_data(1))>0.3405)
        keyboard;
    end
else
    data=0;
    samples=0;
end

if visualize
    plot([uv_mesh(1,:),uv_mesh(1,1)],[uv_mesh(2,:),uv_mesh(2,1)],'r');
end

if plot_debug
    figure;
    plot(uv_mesh(1,:),uv_mesh(2,:),'*r');
    hold on;
    plot(uv_obj(1,idx_data),uv_obj(2,idx_data),'*b');

    figure
    plot(uv_mesh(1,:),uv_mesh(2,:),'*r');
    hold on;
    plot(uv_obj(1,:),uv_obj(2,:),'*b');
end
end






