function [samples,data,M_fin,partialMat_points,idx_unused]=getTriangleValue(verts,map,visualize,idx_unused,plot_debug)


%this function gives the values of the scaling function for the beginning
%of the analysis calculation. We basically decides to give to the triangle
%at the finest level the average value of the cald points above the
%considered triangle.

%the vector verts contain the three points of the triangle on each column
%In the structure map the vector sp_vt contains all the points representing the spherically
%parameterized surface (points are in lines).


%following lines for debugging purposes
% colorr=0;
% for i=1:3
% %     if find(verts(1,i)>0.1623 & verts(1,i)<0.1627) & find(verts(2,i)<0.9515 & verts(2,i)>0.9509) & find(verts(3,i)<-0.2627 & verts(3,i)>-0.2632)
%     if find(verts(1,i)>0.2710 & verts(1,i)<0.2716) & find(verts(2,i)<0.9627 & verts(2,i)>0.9620) & find(verts(3,i)<0.001 & verts(3,i)>-0.001)
%         colorr=1;
% %         keyboard;
%     end
% end

% for i=1:3
%     perm=[1 2 3 1 2];
%     if(verts(perm(i),1)>-0.2631 & verts(perm(i),1)<-0.2627 & verts(perm(i),2)<0.1629 & verts(perm(i),2)>0.1622...
%             & verts(perm(i),3)<0.9513 & verts(perm(i),3)>0.9509 &...
%             verts(perm(i+1),1)>-0.2631 & verts(perm(i+1),1)<-0.2627 & verts(perm(i+1),2)>-0.1629 & verts(perm(i+1),2)<-0.1622...
%             & verts(perm(i+1),3)<0.9513 & verts(perm(i+1),3)>0.9509 &...
%             verts(perm(i+2),1)>-0.5259 & verts(perm(i+2),1)<-0.5255 & verts(perm(i+2),2)<0.001 & verts(perm(i+2),2)>-0.001...
%             & verts(perm(i+2),3)<0.8513 & verts(perm(i+2),3)>0.8503)
% 
%         keyboard;
%     end
% end

if idx_unused==-1

    data=[0,0,0];
    samples=0;
    M_fin=0;
    partialMat_points=zeros(1,length(map.sp_vt));

else
    
    path(path,'/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/toolbox');

    %Legland toolbox to handle 3D geometry calculation
    path(path,'/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/geom3d/geom3d');

    %definition of three vector in the plane of the triangle

    AB=verts(:,2)-verts(:,1);
    BC=verts(:,3)-verts(:,2);
    CA=verts(:,1)-verts(:,3);
    % 
    % X = [verts(1,1);verts(1,2);verts(1,3)];
    % Y=[verts(2,1);verts(2,2);verts(2,3)];
    % Z=[verts(3,1);verts(3,2);verts(3,3)] ; % X, Y et Z have to be in column
    % nx = length(X) ; % point number
    % U = ones(nx, 1) ;
    % M = [U X Y] ;
    % K=M\Z;
    % 
    % %normal vector of the plane
    % n=[K(2);K(3);-1];
    % 
    % if n(1)==-inf || n(1)==inf
    %     Z=[0;0;0;1];
    %     M=[M;1,0,0];
    %     K=M\Z;
    % 
    %     %normal vector of the plane
    %     n=[K(2);K(3);0];   
    % 
    % end
    % 
    % plane_side=map.sp_vt(idx_unused,:)*n+K(1);
    % 
    % %distance between points of the surface and the plan (used to get the
    % %projection vector)
    % dist=abs(plane_side)/norm(n);
    % 
    % %we know that the points above triangles are very very close to them, so we
    % %reduce the number of points we will look at
    % idx_pts=find(dist<0.1);
    % 
    % %we calculate the projection of each interesting point on the plane
    % proj=map.sp_vt(idx_unused(idx_pts),:)+repmat(-sign(plane_side(idx_pts)).*dist(idx_pts),1,3).*repmat(n'/norm(n),length(idx_pts),1);
    % 
    % 
    % AM=proj-repmat(verts(:,1)',size(proj,1),1);
    % BM=proj-repmat(verts(:,2)',size(proj,1),1);
    % CM=proj-repmat(verts(:,3)',size(proj,1),1);

    P=createPlane(verts');

    dist=distancePointPlane(map.sp_vt(idx_unused,:),P);

    idx_pts=find(dist<0.1);

    M=intersectLinePlane([repmat([0 0 0] ,size(map.sp_vt(idx_unused(idx_pts),:),1),1) map.sp_vt(idx_unused(idx_pts),:)],P);




    AM=M-repmat(verts(:,1)',size(M,1),1);
    BM=M-repmat(verts(:,2)',size(M,1),1);
    CM=M-repmat(verts(:,3)',size(M,1),1);

    %calculation to find the index of points above the triangles
    idx_data=find(dot(cross(repmat(AB,1,size(AM,1)),AM'),cross(AM',repmat(-CA,1,size(AM,1))))>=0 &...
        dot(cross(repmat(BC,1,size(BM,1)),BM'),cross(BM',repmat(-AB,1,size(BM,1))))>=0 &...
        dot(cross(repmat(CA,1,size(CM,1)),CM'),cross(CM',repmat(-BC,1,size(CM,1))))>=0);


    partialMat_points=zeros(1,length(map.sp_vt));

    if ~isempty(idx_data)
        data=sum(map.surf_vt(idx_unused(idx_pts(idx_data)),:),1)/length(idx_data);

        samples=map.sp_vt(idx_unused(idx_pts(idx_data)),:);
        M_fin=M(idx_data,:);

        %definition of the matrix that will tell to which triangle pertains each points of the surface

        partialMat_points(idx_unused(idx_pts(idx_data)))=1;
        idx_unused(idx_pts(idx_data))=0;

        if idx_unused==0
            idx_unused=-1;
        else
            idx_unused=idx_unused(idx_unused~=0);
        end
    else
        data=[0,0,0];
        samples=0;
        M_fin=0;
    end

    if visualize
        options.face_color=0;
        plot_mesh(verts,[1,2,3],options);shading faceted; axis tight;
    %     if colorr
    %         plot3([verts(1,:),verts(1,1)],[verts(2,:),verts(2,1)],[verts(3,:),verts(3,1)],'g');
    %     else
            plot3([verts(1,:),verts(1,1)],[verts(2,:),verts(2,1)],[verts(3,:),verts(3,1)],'r');
    %     end

    end

    if plot_debug

        figure
        plot3([verts(1,:),verts(1,1)],[verts(2,:),verts(2,1)],[verts(3,:),verts(3,1)],'r');
        hold on;
        plot3(map.sp_vt(idx_unused(idx_pts),1),map.sp_vt(idx_unused(idx_pts),2),map.sp_vt(idx_unused(idx_pts),3),'.b');

        for j=1:length(idx_pts)
            plot3([map.sp_vt(idx_unused(idx_pts(j)),1),M(j,1)],[map.sp_vt(idx_unused(idx_pts(j)),2),M(j,2)],[map.sp_vt(idx_unused(idx_pts(j)),3),M(j,3)],'b');
        end
        plot3(M(:,1),M(:,2),M(:,3),'xb');


        figure;
        plot3([verts(1,:),verts(1,1)],[verts(2,:),verts(2,1)],[verts(3,:),verts(3,1)],'r');
        hold on;
        plot3(map.sp_vt(idx_unused(idx_pts(idx_data)),1),map.sp_vt(idx_unused(idx_pts(idx_data)),2),map.sp_vt(idx_unused(idx_pts(idx_data)),3),'.b');
        for j=1:length(idx_data)
            plot3([map.sp_vt(idx_unused(idx_pts(idx_data(j))),1),M(idx_data(j),1)],[map.sp_vt(idx_unused(idx_pts(idx_data(j))),2),M(idx_data(j),2)],[map.sp_vt(idx_unused(idx_pts(idx_data(j))),3),M(idx_data(j),3)],'b');
        end
        plot3(M(idx_data,1),M(idx_data,2),M(idx_data,3),'xb');

    end
end
end
