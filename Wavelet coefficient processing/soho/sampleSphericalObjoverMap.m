function [stris,faces_nb,Mat_points] = sampleSphericalObjoverMap( stris, map, level, ...
                                     visualize )
% 
% stris = sampleSphericalMap( stris, map, num_samples, level, visualize ) 
%
% Resample texture onto spherical triangles on level \a level of the wavelet
% tree
% @return forest of wavelet trees where the nodes on level \a level contain
%         a resampled version of \a map
% @param  stris  root nodes of forest of wavelet trees
% @param  map    source map  for resampling
% @param  num_samples  number of samples per domain on level \a level
% @param  level   level of the wavelet trees on which to resample the data
% @param  visualize   {0,1}{default = 0} visualize the samples 

  


    
  % draw map of all the points that have to be taken into account
  if( nargin == 4)
    if( visualize > 0)
%         uv_obj = mapPointFromSphereToTex( map.sp_vt' );
        figure;
%         plot( uv_obj(1,:)+max(uv_obj(1,:))/1000,uv_obj(2,:)+max(uv_obj(2,:))/1000,'*g');
        hold on;  
    end
  end
  
  empty=0;
  faces_nb=0;
  
  faces_nb_icosa{1}=20;
  faces_nb_icosa{2}=80;
  faces_nb_icosa{3}=320;
  faces_nb_icosa{4}=1280;
  faces_nb_icosa{5}=5120;
  faces_nb_icosa{6}=20480;
 
  
  h=waitbar(0,['sampling on icosahedron at level' num2str(level)]);
  
  Mat_points=zeros(faces_nb_icosa{level+1},length(map.sp_vt));
  idx_unused=1:length(map.sp_vt);
  [stris ,empty,faces_nb,Mat_points,idx_unused,h]= sampleSphericalMapPrivate( stris, map, level, visualize, empty,faces_nb,Mat_points,idx_unused,h);
  
  close(h);
  
  if idx_unused==-1
      disp('all the points are on triangles')
  else
      disp('these are the points that are not in triangles: ')
      for i=1:length(idx_unused)
          disp(num2str(idx_unused(i)));
      end
      if visualize
      
      for i=1:size(Mat_points,2)
          if ~find(Mat_points(:,i))
              plot3(map.sp_vt(i,1),map.sp_vt(i,2),map.sp_vt(i,3),'.g');
          end
      end
        
      saveas(gcf,['../rec_graph/initialization_at_level ' num2str(level + 1)],'fig');
      end
           
  end
  
  disp(['number of empty triangles: ' num2str(empty)]);
  disp(['corresponding to ' num2str(empty*100/faces_nb) ' % of the number of triangles available']);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [stris,empty,faces_nb,Mat_points,idx_unused,h] = sampleSphericalMapPrivate( stris, map, level, ...
                                            visualize,empty,faces_nb,Mat_points,idx_unused,h )                                   
    
  % do for all triangles
  for t = 1 : numel( stris)
    if numel(stris)==20
        waitbar(t/20);
    end
    if( getLevel(stris(t)) == level)

      verts = getVertsEC( stris(t));

      [samples,data,proj,partialMat_points,idx_unused]=getTriangleValue(verts,map,visualize,idx_unused,0);

      stris(t) = setData( stris(t), data');
            
      faces_nb=faces_nb+1;

      Mat_points(faces_nb,:)=partialMat_points;
      
      
      if samples==0
        empty=empty+1;
      end
          
      % visualize samples 
      if( visualize > 0 & samples~=0)
        plot3(samples(:,1),samples(:,2),samples(:,3),'.b');
        for j=1:size(samples,1)
            plot3([samples(j,1),proj(j,1)],[samples(j,2),proj(j,2)],[samples(j,3),proj(j,3)],'b');
        end
        plot3(proj(:,1),proj(:,2),proj(:,3),'xb');
                
      end

    elseif( getLevel(stris(t)) < level)

      childs = getChilds( stris(t));

      % recursively traverse wavelet tree
      for( i = 1 : numel( childs))
        [childs(i), empty,faces_nb,Mat_points,idx_unused,h]= sampleSphericalMapPrivate( childs(i), map, ...
                                               level, ...
                                               visualize ,empty,faces_nb,Mat_points,idx_unused,h);
      end

      stris(t) = setChilds( stris(t), childs);

    else  
      warning( 'Invalid input level.');
    end

  end % end for all input triangles
  
  
end
