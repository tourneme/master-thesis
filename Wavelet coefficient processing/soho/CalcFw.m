function [sig_fw, fw_part]=CalcFw(stris, Mat_points, level, sig_fw)

for t = 1 : numel( stris)

    if( getLevel(stris(t)) == level-1)
        
        sig_fw=[sig_fw ; [0 0 0]];
        for p=1:3
            sig_fw=[sig_fw ; getWaveletCoeff(stris(p))'];
        end

    elseif( getLevel(stris(t)) < level-1)

        childs = getChilds( stris(t));

        % recursively traverse wavelet tree
        for( i = 1 : numel( childs))
            [sig_fw]= CalcFw(childs(i),level,sig_fw);
        end
    end

end % end for all input triangles

fw_part=Mat_points{1}*sig_fw;

end 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% function fw_part=CalcFwPart(stris)
% 
% fw=[];
% for j=0:J
%     if j==0
%         for p=1:20
%         sig_fw=[sig_fw ; getScaleCoeff(forest_analysed(p))'];
%         end
%         fw=Mat_points{1}*sig_fw;
%     end
%     for p=1:2
%         fw=[fw getWaveletCoeffs(forest_analysed)];
%     end
% end
% end