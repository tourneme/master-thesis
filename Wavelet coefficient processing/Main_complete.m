close all
clear cells overtransform sohotransform lazytransform coef_extraction coef_analysis levels visu num num_smo num_over

global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');
path(path, [RELATIV_ROOT 'reusable_code']);





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% LAZY WAVELET TRANSFORM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%set to 1 if you want to use the Lazy wavelet transform from Schr?der and
%Sweldens
lazytransform=0;

if lazytransform
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT EXTRACTION %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %set to 1 if you want to extract the wavelet coefficient from your
    %SPHERICAL representation of the cartesian functions
    coef_extraction=0;
    
        %if the extraction is on you can plot the basic meshes
        plot_base_mesh=0;
        %max l
		evel of wavelet transform
        J = 6;
        
        %threshold for surface reconstruction according to wavelet coefficients. 
        %Correspond to the number of coefficients from the transform we want to keep.    
        r = .1;

        %if you want to plot the reconstructed cell
        plot_recons=0;
        
        
        
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

    %set to 1 if you want to do the k-mean, Otsu or WAT analysis
    coef_analysis=1;

        %%%%%%%%%%%%%%%%%%%% K-Mean %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        k_mean=0;
        
        %%%%%%%%%%%%%%%%%%%% OTSU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        otsu=0;
    
        %%%%%%%%%%%%%%%%%%%% WAT METHOD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        wat=1;
        
        %influence of variance in WAT 
        k=1;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %levels of depth you want to dig in
        levels=3;

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% SOHO WAVELET TRANSFORM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%set to 1 if you want to use the SOHO wavelet transform from Lessig
sohotransform=0;

if sohotransform
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT EXTRACTION %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %set to 1 if you want to extract the wavelet coefficient from your
    %SPHERICAL representation of the cartesian function
    coef_extraction=0;
    
        %if the extraction is on you can plot the basic meshes
        plot_base_mesh=0;
        
        %(max-1) level of wavelet transform !!! level of basic icosahedron
        %is 0?not 1 like in the lazy wavelet.
        J = 1;
        
        %threshold for surface reconstruction according to wavelet coefficients. 
        %Correspond to the number of coefficients from the transform we want to keep.    
        r = 0.95;

        %if you want to plot the reconstructed cell
        plot_recons=1;
        
        %let you see how the CALD parameterization has scattered the point
        %on the icosahedron on a 2D representation of the sphere
        %!!!!! because of 3D plotting on a long loop, this method adds a
        %lot of computation time (approx. 15min for J=3)
        plot_repartition=0;

        %NO COEFFICIENT ANALYSIS BECAUSE NO POSSIBILITIES TO DO IT EASILY
        %very big limitation XD

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% OVERCOMPLETE WAVELET TRANSFORM %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

%set to 1 if you want to use the Overcomplete wavelet transform from Yeo
overtransform=1;

if overtransform
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT EXTRACTION %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %set to 1 if you want to extract the wavelet coefficient from your
    %SPHERICAL representation of the cartesian function
    coef_extraction=1;
    
    %Laplacian filter parameters (you directly have to change the function
    %filter_choice
    [BW, max_invertibility, kernel_sizes, sigma]=filter_choice;
    
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

    %set to 1 if you want to do the k-mean, Otsu or WAT analysis
    coef_analysis=1;

        %levels of depth you want to dig in
        levels=3;
        
        
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% SHAPE VISUALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%set to 1 if you want to see cell shapes
visu=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% PARAMETERIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%CALD correction on latitude
correction=0;
       
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% DATA TO WORK ON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%Data you want to work on: there is now three possibilities to load data:
%the two first one handle actual data and the third one simulated ones.

%The first one uses the index settled in the very ugly array made of the
%names of amoebas given by several generation of biologist XD. To use this
%one you will need to set the num vector.
%
%The second one has been done renaming every file in an understandable 
%manner in terms of databases (in order to rename files with random names,
%I invite you to use and modify the AppleScript I made in the folder 
%ROBIN/AppleScript/renaming_....). In order to use this one you will need
%to set the array of structures cells
%
%The third one uses a the file simulation simulating cells with different
%type of blebs after the CALD parameterization.
%
%
%The following three sections set parameters according to which data you
%want to use.
%
%has to be set to 0 to use the former real data format, to one to use the
% actual data format, to two to use the simulated data
cells{1}.on=2;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% NUM CHOICE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%%%%%%%%%%%%%%%%%%%%% FOR LAZY WAVELET %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num=0;
if lazytransform || visu

%%%%%%%%%%%%%%%%%%%%%% COEFFICIENT EXTRACTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%NUM IS TAKEN FROM THE FOLDER: Toolboxes and functions/toolbox_wavelet_meshes/data_sph
%the minimum value of num is 3
%between to set of cells you have to add -1. It is though useless because 
%the display won't tell you when it changes (to be implemented) 


%%%%%%%%%%%%%%%%%%%%% COEFFICIENT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%num: number of amoeba you want to analyze (direct file number in the folder
%Toolboxes and functions/toolbox_wavelet_meshes/ResultsWaveSph
%(num can begins at 6 and finish at 1718, besides these 
%frames separates cells and are non usable numbers 94, 137, 226, 309, 
%398, 487, 576, 621, 697, 758, 806, 1440, 1532, 1625, 1626, 1719, 1720)

%between two sets of cells you have to add a -1 in num.

    %num=[6:93,-1,95:136,-1,138:225,,-1227:308];
    %num=[310:397,-1,399:486];
    %num=[488:575,-1,577:620,-1,622:696];
    %num=[698:757,-1,759:805];
    %num=[807:858,-1,859:878,-1,879:927,-1,928:976,-1,977:1036];
    %num=[1037:1126,-1,1127:1206,-1,1207:1296];
    %num=[1297:1309,-1,1310:1350,-1,1351:1379,-1,1380:1409,-1,1410:1439];
    %num=[1441:1531,-1,1533:1624,-1,1627:1718];
    %num=1410;


%%%%%%%%%%%%%%%%%%%%%%%%%%     ROI    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%(first annotation taking care of the most prominant blebs)%%%%%%%%%%%%%

    num=17:21;
    % num=188:194;
%     num=247:252; %resorbtion
    % num=366:370; %resorbtion
%     num=506:513; %bof
    % num=853:857; %resorbtion
%     num=895:900; 
%     num=928:938; %two moves very beautiful
    % num=1017:1022; %bad resolution
%     num=1058:1067; %bof
%     num=1079:1083;
    % num=1098:1104; %to be understood
    % num=1112:1119; %wonderful
    % num=1128:1134; %resorbtion
%     num=1138:1145; %beautiful move
%     num=1195:1198; %gros bleb
%     num=1207:1296; %dead cell
    % num=1307:1309;
%     num=1381:1383; %bad resolution
    % num=1390:1394;
    % num=1462:1466; %bof
%     num=1486:1491; %bof
%     num=1632:1638; %resorbtion
    % num=1654:1656;
% 
%     num=19;
%     
%     num=10;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%% FOR SOHO WAVELET %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if sohotransform
    %this number is only for very interesting cells
    num_smo=29;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%% FOR OVERCOMPLETE WAVELET %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%apparently interesting amoebas (to find the right serie, just plot witht the function visu to see it again):

%06012010_Wt-1H_#0009_T0030_CALD_smo.mat      THE CELL FOR DETECTION   
%20100107_E64 1H_#0004_T0008_CALD_smo.mat     is nothing but a dead cell
%20100106_E64-1H30_#0001_T0015_CALD_smo.mat   baddddd
%06102010_Wt-1H_#0005_T0071_CALD_smo.mat      baddddd
%20100106_E64-1H30_#0001_T0019_CALD_smo.mat   c'est la m?me cellule qu'au
%dessus cazzo
%15-01-10_WT_30min_#02_T08_CALD_ini.mat       good big bleb

if overtransform
    %this number is only for a very interesting cells (cannot be lmonger
    %than 6 samples (plotting constraint)

%     num_over=5184:5189; %big bleb very prominent
    num_over=1210:1215; %my best example
%     num_over=9164:9169; %dead cell
%     num_over=6664:6669;
%     num_over=1611:1616;
%     num_over=6668:6673;
% num_over=2460:10000;

%     num_over=29;
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CELLS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%if on is 1 then you will use the cells-system if it is 0 then you will use
%the overugly num method.

% the date has to be written in this way: day-month-year
% example:   06-01-10

%number has to contain 2 digits consequently the cell 1 has to be written
%01 (you only have to define it in the first cell
%type can be either WT_ or E64 (donc forget the underscore for WT!!!!!!!!!)
%time can be 0h30 or 1h or 1h30
%frames is a vector of integers.

%all these parameters have to be strings unless frames.

if cells{1}.on==1

    %the function cell choice define the aray of structure cells in a general way
%     cells=cell_choice;
	
	%study of few cells tri? sur le volet
% 	cells=COI;


    %the following lines has to be used if you want to work on one cell only
    %(faster than modifying the cell_choice function
	cells{1}.date='19-06-09';
	cells{1}.type='E64';
	cells{1}.time='1h30';
	cells{1}.number='00';
	cells{1}.frames=2; %4-82
	% cells{1}.set=2;
	
elseif cells{1}.on==3
	
	%the function cell choice define the aray of structure cells in a general way
	%     cells=cell_choice;
	
	%study of few cells tri? sur le volet
	% 	cells=COI;
	% 	cells{1}.on=3;
	%the following lines has to be used if you want to work on one cell only
	%(faster than modifying the cell_choice function
	cells{1}.date='19-06-09';
	cells{1}.type='E64';
	cells{1}.time='1h30';
	cells{1}.number='00';
	cells{1}.frames=2; %4-82
	% cells{1}.set=2;
	
elseif cells{1}.on==4
	
	%the function cell choice define the aray of structure cells in a general way
	%     cells=cell_choice;
	
	%study of few cells tri? sur le volet
	% 	cells=COI;
	% 	cells{1}.on=4;
	%the following lines has to be used if you want to work on one cell only
	%(faster than modifying the cell_choice function
	cells{1}.date='19-06-09';
	cells{1}.type='E64';
	cells{1}.time='1h30';
	cells{1}.number='00';
	cells{1}.frames=2; %4-82
	% cells{1}.set=2;

end

%%
%%%%%%%%%%%%%%%%%%%%%%%%% SIMULATED SIGNALS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%in order to define simulated signals, you have to give the basic shape and
%the number of gaussians you want (different of the final bleb number)
%if you set up sigma1 and sigma2, it means that you are analyzing one bleb,
%and consequently, you have to have one bleb. 
%if the field number doesn't exists a simulated cell will be created anyway. If
%you want to analyze an already computed simulated cell, you have to give
%its number in the field number.

if cells{1}.on==2

% 	cells=simutest;
	cells{1}.type='cir';
	cells{1}.gaus=5;
	cells{1}.minblebs=5;
	cells{1}.maxblebs=5;
% 	cells{1}.k=0.2;
% 	cells{1}.sigma1=0.2;
% 	cells{1}.sigma2=0.1;
	cells{1}.frames=32;
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% THE MAIN PROCESSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

if visu
	set(0,'DefaultFigureWindowStyle','normal');
else
	set(0,'DefaultFigureWindowStyle','docked');
end

%function which handles the extraction and the analysis using lazy wavelets
if lazytransform
    lazy_wavelet(coef_extraction,coef_analysis,k_mean,otsu,wat,J,r,num,levels,...
        k,plot_base_mesh,plot_recons,cells);
end
    
%function which handles the extraction using soho wavelets
if sohotransform
    soho_wavelet(coef_extraction,J,r,num_smo,plot_base_mesh,plot_recons,plot_repartition,cells);
end

if overtransform
	if coef_extraction
		[num,cells]=overcomplete_wavelet(num_over,levels,cells,correction,BW,...
			max_invertibility,kernel_sizes,sigma);
	end
	%Bleb analysis
	if coef_analysis
		if length(cells)>2
			bleb_detection_fast_computation(cells,correction,BW,max_invertibility,...
				kernel_sizes,sigma);
		else
			bleb_detection(num,levels,cells,correction,BW,max_invertibility,...
				kernel_sizes,sigma);
		end
	end
end

%fast visualization of the cell shape
if visu
    cell_visualization(num,cells);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





