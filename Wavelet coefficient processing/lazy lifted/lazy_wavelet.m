function lazy_wavelet(coef_extraction,coef_analysis,k_mean,otsu,wat,J,r,num,levels,k_wat,plot_base_mesh,plot_recons)

    % Application of the toolbox to three spherical functions describing the
    % surface of our amoebas. The three spherical functions correspond to the
    % evolution of the coordinnates X, Y and Z. 
    % This is one way of analyzing
    % the surface of our cells, another method should use the wavelet
    % transform on the meshed surface directly. 
    %
    % Wavelet Transform of Functions Defined on Surfaces
    % A wavelet transform can be used to compress a function defined on a
    % surface. Here we take the example of a 3D sphere. The wavelet transform
    % is implemented with the Lifting Scheme of Sweldens, extended to
    % triangulated meshes by Sweldens and Schroder in a SIGGRAPH 1995 paper.
    %
    %for the analysis you need for num a vector of MAXIMUM 12 components.

base_path='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
path(path, base_path);
path(path, [base_path 'toolbox/']);
path(path, [base_path 'gim/']);
path(path, [base_path 'data/']);
path(path, [base_path 'funcRegMeshes/']);
path(path, '../reusable_code');
clear options;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% COMPUTATION OF WAVELET COEFFICIENTS  %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if coef_extraction
    
    %%%%%%%%%%%%%%% COMPUTE THE SEMI-REGULAR SPHERE %%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % options for the display
    options.use_color = 1;
    options.rho = .3;
    options.color = 'rescale';
    options.use_elevation = 0;
    % options for the multiresolution mesh
    options.base_mesh = 'ico';
    %smoothing or not from the regular icosahedron with the parameter
    %relaxation
    options.relaxation = 0;
    options.keep_subdivision = 1;

    [vertex,face] = compute_semiregular_sphere(J,options);

    % We can display the semi-regular sphere.

    if plot_base_mesh
        
        selj = 1:J;
        figure
        for j=1:length(selj)
            subplot(3,3,j);
            plot_mesh(vertex{selj(j)},face{selj(j)}, options);
            shading faceted; axis tight;
            title(['Subdivision level ' num2str(selj(j))]);
        end
        colormap gray(256);
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    

    %%%%%%% GET THE DATA AND PERFORM THE SPHERICAL WAVELET TRANSFORM %%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % load the file                

    Folder = [base_path 'data_sph' ];
    Folder=[Folder '/RobinOutput'];
    d = dir(Folder);

    
    %split_fr stands for splitting frame dividing the vector num into all the
    %analyzed cells
    split_fr=find(num(2:end)-num(1:end-1)~=1);
    split_fr=[0,split_fr];
    for i=1:length(split_fr)
        if split_fr==0
            num_calc=num;
        else
            if i==length(split_fr)
                num_calc=num(split_fr(i)+1:end);
            else
                num_calc=num(split_fr(i)+1:split_fr(i+1));
            end
        end

        for idx = num_calc(end)
            name =  d(idx).name ;
            load([Folder,'/',name]);
%             load('../Toolboxes and functions/toolbox_wavelet_meshes/data_sph/06-01-10_E64 1H30_#0001/06-01-10_E64 1H30_#0001_T0017_CALD_LSF_des_wavelet_icosa5.mat');
            disp(name);

            f = vertices' ; 

            % Perform the wavelet transform and remove small coefficients.

            %f={coef of the icosahedron, and then from the points at the 
            %middle of each edges... till the final mesh division
            %The highest coefficients are simply the values of the function
            %in these space points (initialisation)
            fw = perform_wavelet_mesh_transform(vertex,face, f, +1, options);
            % threshold (remove) most of the coefficient

            fwT = keep_biggest( fw, round(r*length(fw)) );

            % backward transform
            f1 = perform_wavelet_mesh_transform(vertex,face, fwT, -1, options);

            disp('the reconstruction error is:');
            disp(mean(abs(f-f1)));
            disp('with the number of coefficients different from zero:')
            disp(length(find(fwT~=0)));

            save(['../results/wavelet_coefs/Lazy_wavelet/',name(1:length(name)-23),'_SphWav.mat'],...
                'f','vertex','face','fw','fwT','f1','r');
        end

        if plot_recons
            % display the resulting reconstruction
            figure
            subplot(1,2,1);
            plot_mesh(f,face{end},options); shading faceted; axis tight;
            title('Original surface');
            subplot(1,2,2);
            plot_mesh(f1,face{end},options); shading faceted; axis tight;
            title('Wavelet approximation');
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% ANALYZE ON THE DIFFERENT LEVELS OF RESOLUTION %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if coef_analysis
    
    wavelet_coefficient_analysis(num,k_mean,otsu,wat,levels,k_wat);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






end
 
 
