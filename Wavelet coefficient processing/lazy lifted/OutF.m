function [ Outmost, Out5 ] = OutF(vertex,face,k)
% This function computes the points that should not be considered in the
% spherical wavelets analysis. These are the points that have five
% neighboors and not 6, and that therefore are detected to be particular by
% the wavelet coefficients computed from their neighboors. 

%Out5=zeros(1,60);
Out5 = [];
%Out5_vertex=zeros(1,60);
Out5_vtx=[];
f = face{k}; 
vtx = vertex{k};

temp=[];
Outmost=[];

for v = 1:length(vtx)
    if(length(find(f == v)) ~= 6)
        [~,J] = find(f == v);
        for n = 1: length(J)      
            Out5 = [Out5; f(:,J(n))];
            for i=1:3
                if ~isempty(find(Out5_vtx==f(i,J(n)) & f(i,J(n))~=v, 1))
                    Out5_vtx=[Out5_vtx,f(i,J(n))];
                end
            end
            
            [~,J_out]=find(f==Out5_vtx(n));
            for m=1:length(J_out)
                for i=1:3
                    if isempty(find(Out5_vtx==f(i,J_out(m)) & f(i,J_out(m))~=v, 1))
                        temp=[temp,f(i,J_out(n))];
                    end
                end
            end
            for p=1:length(temp)
                false=0;
                [I_det,J_det]=find(f==temp(p));
                for q=1:length(J_det)
                    for r=1:length(Out5_vtx)
                        if ~isempty(find(f(:,J_det(q))==Out_vtx(r))) && false==0
                            false=1;
                        end                
                    end
                end
                if false==0
                    Outmost=[Outmost;temp];
                end
            end
        end
    end
end




end

