function [ Out_vtx ] = Outvtx(vertex,face,k)
% This function computes the points that should not be considered in the
% spherical wavelets analysis. These are the points that have five
% neighboors and not 6, and that therefore are detected to be particular by
% the wavelet coefficients computed from their neighboors. 

Out_vtx=zeros(1,60);
f = face{k}; 
vtx = vertex{k};
idx=1;

for v = 1:length(vtx)
    if(length(find(f == v)) ~= 6)
        [~,J] = find(f == v);
        for n = 1: length(J)      
            for i=1:3
                if isempty(find(Out_vtx==f(i,J(n)), 1))
                    if f(i,J(n))~=v
                        Out_vtx(idx)=f(i,J(n));
                        idx=idx+1;
                    end
                end
            end          
        end
    end
end

end