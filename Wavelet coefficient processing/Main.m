close all
clear cells overtransform sohotransform lazytransform coef_extraction coef_analysis levels visu num num_smo num_over

global RELATIV_ROOT
RELATIV_ROOT='../';

path(path, 'lazy lifted/');
path(path, 'soho/');
path(path, 'overcomplete/');
path(path, [RELATIV_ROOT 'reusable_code']);


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%% OVERCOMPLETE WAVELET TRANSFORM %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

%set to 1 if you want to use the Overcomplete wavelet transform from Yeo
%%%%%%%%%%%%% EVERY TIME AT ONE %%%%%%%%%%%%%%
overtransform=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if overtransform
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT EXTRACTION %%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %set to 1 if you want to extract the wavelet coefficient from your
    %SPHERICAL representation of the cartesian function
    coef_extraction=1;
    
    %Laplacian filter parameters (you directly have to change the function
    %filter_choice
    [BW, max_invertibility, kernel_sizes, sigma]=filter_choice;
    
    %%%%%%%%%%%%%%%%%%%%%% COEFFICIENT ANALYSIS %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

    %analysis?
    coef_analysis=1;

	%levels of depth you want to dig in
	levels=3;
        
        
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% SHAPE VISUALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%set to 1 if you want to see cell shapes
% no visu for simulated cells (visu will work later, don't worry)
visu=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% DATA TO WORK ON %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

%1=shen parameterization
%2=gu parameterization (to perform the spherical param you nedd windows...)
%3=Fischl parameterization (THE PARAMETERIZATION)
%4=simulated cells (with radial projection or Fischl parameterization)
cells{1}.on=4;

% the date has to be written in this way: day-month-year
% example:   06-01-10
%number has to contain 2 digits consequently the cell 1 has to be written
%01 (you only have to define it in the first cell
%type can be either WT_ or E64 (donc forget the underscore for WT!!!!!!!!!)
%time can be 0h30 or 1h or 1h30
%frames is a vector of integers.
%all these parameters have to be strings unless frames.
%

if cells{1}.on==1

	%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %the function cell choice define the aray of structure cells in a general way
%    cells=cell_choice;
	
	%study of few "chosen by expert" cells
% 	cells=COI;

    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


    %the following lines has to be used if you want to work on one cell only
    %(faster than modifying the cell_choice function
	cells{1}.date='19-06-09';
	cells{1}.type='E64';
	cells{1}.time='1h30';
	cells{1}.number='00';
	cells{1}.frames=2; %4-82
	% cells{1}.set=2;
	
elseif cells{1}.on==2
	
	%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %the function cell choice define the aray of structure cells in a general way
%    cells=cell_choice;
	
	%study of few "chosen by expert" cells
% 	cells=COI;
% 	cells{1}.on=2;

    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	%the following lines has to be used if you want to work on one cell only
	%(faster than modifying the cell_choice function
	cells{1}.date='19-06-09';
	cells{1}.type='E64';
	cells{1}.time='1h30';
	cells{1}.number='00';
	cells{1}.frames=2; %4:82
	% cells{1}.set=2;
	
elseif cells{1}.on==3
	
	%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %the function cell choice define the aray of structure cells in a general way
%    cells=cell_choice;
	
	%study of few "chosen by expert" cells
% 	cells=COI;
% 	cells{1}.on=3;

    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	%the following lines has to be used if you want to work on one cell only
	%(faster than modifying the cell_choice function
	cells{1}.date='15-01-10';
	cells{1}.type='WT_'; % WT_ paske ca doit faire 3 caracteres banane
	cells{1}.time='1h30';
	cells{1}.number='01';
	cells{1}.frames=0:1; %4-82
	
	% set is used for multiple series with the same experiment name
	% cells{1}.set=2;

end

%%
%%%%%%%%%%%%%%%%%%%%%%%%% SIMULATED SIGNALS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%in order to define simulated signals, you have to give the basic shape and
%the number of gaussians you want (different of the final bleb number)
%if you set up sigma1 and sigma2, it means that you are analyzing one bleb 
%if the field minblebs and maxblebs and frames don't exists a simulated cell will be created anyway. If
%you want to analyze an already computed simulated cell, you have to give
%its number in the field number.

if cells{1}.on==4

	%for high thorughput simulation (excel creation) 
% 	cells=simutest;



	cells{1}.type='cir';
% 	% ze number of ze gaussiennes (blebs)
	cells{1}.gaus=5;

	%set to one if you want to use the freesurfer parameterization instead of "wrong" radial projection.
	cells{1}.freesurfer=1;
	
	%the next three lines have to be uncommented if the cell exists.
	cells{1}.minblebs=4;
	cells{1}.maxblebs=4;
	cells{1}.frames=1;
	


	%data for plotting only one gaussian: almost obselete nowadays, but I
	%left the possibility to doit just in case.
% 	cells{1}.k=0.2;
% 	cells{1}.sigma1=0.2;
% 	cells{1}.sigma2=0.1;
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%% THE MAIN PROCESSES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

if overtransform
	set(0,'DefaultFigureWindowStyle','docked');
	if coef_extraction
		[num,cells]=overcomplete_wavelet(levels,cells,BW,...
			max_invertibility,kernel_sizes,sigma);
	end
	%Bleb analysis
	if coef_analysis
		if length(cells)>9
			%function created to analyze lots of signals without plotting
			%any graph and creating an excel file at the end
			%uniquement sur signaux simul?s.
			bleb_detection_fast_computation(cells,BW,max_invertibility,...
				kernel_sizes,sigma);
		else
			if ~isempty(num)
				bleb_detection(num,levels,cells,BW,max_invertibility,...
					kernel_sizes,sigma);
			else
				disp('the simulated cell is not valid, and minbleb, maxbleb values are 0 . If you are not in the simulated case this is a bug');
			end
		end
	end
end

%fast visualization of the cell shape
if visu
	set(0,'DefaultFigureWindowStyle','normal');
    cell_visualization(cells);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





