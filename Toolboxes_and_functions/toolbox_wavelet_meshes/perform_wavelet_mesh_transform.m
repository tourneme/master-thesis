function f = perform_wavelet_mesh_transform(vertex,face, f, dir, options)

% perform_wavelet_mesh_transform - compute a wavelet tranform on a mesh
%
%   f = perform_wavelet_mesh_transform(vertex,face, f, dir, options);
%
%   vertex,face must be a semi-regular cell array of meshes.
%
%   Compute the wavelet transform of a function defined on a semi-regular
%   mesh. The full sized mesh is stored as vertex{end},face{end}, and f is
%   a vector with f(i) being the value of the function at vertex i.
%
%   This transform is implemented using the lifting scheme and a butterfly
%   predictor, as explained in
%
%       Peter Schroder and Wim Sweldens
%       Spherical Wavelets: Texture Processing 
%       Rendering Techniques 95, Springer Verlag
%
%       Peter Schrodder and Wim Sweldens
%    	Spherical Wavelets: Efficiently Representing Functions on the Sphere
%       Siggraph 95
%
%   Copyright (c) 2007 Gabriel Peyre


options.null = 0;
J = length(vertex);

if size(f,1)<size(f,2)
    f = f';
end

temps=0;

if size(f,2)>1
    for i=1:size(f,2)      
        f(:,i) = perform_wavelet_mesh_transform(vertex,face, f(:,i), dir, options);
    end
    return;
end

global vring;
global e2f;
global fring;
global facej;
    
jlist = J-1:-1:1;
if dir==-1
    jlist = jlist(end:-1:1);
end

do_update = getoptions(options,'do_update', 1);
do_scaling = getoptions(options,'do_scaling', 1);

if do_update
    % compute the integral of the scaling function
    n = length(f);
    I = zeros(n,1);
    for j=J-1:-1:1
        % compute navigator helpers
        vring = compute_vertex_ring(face{j+1});
        e2f = compute_edge_face_ring(face{j});
        fring = compute_face_ring(face{j});
        facej = face{j};

        % number of coarse points
        nj = size(vertex{j},2);
        % number of fine points
        nj1 = size(vertex{j+1},2);
        
        if j==J-1
            I(nj+1:end) = 1;
        end

        %%%% PREDICT STEP %%%%
        for k=nj+1:nj1  % for all the fine point
            % retrieve coarse neighbors
            [e,v,g] = compute_butterfly_neighbors(k, nj);

            % do interpolation with butterfly
            I(e) = I(e) + 1/2 * I(k);
            I(v) = I(v) + 1/8 * I(k);
            I(g) = I(g) - 1/16 * I(k);
        end
        
    end    
end

for j=jlist
%     for j=5

    % compute navigator helpers
    vring = compute_vertex_ring(face{j+1});
    e2f = compute_edge_face_ring(face{j});
    fring = compute_face_ring(face{j});
    facej = face{j};

    % number of coarse points
    nj = size(vertex{j},2);
    % number of fine points
    nj1 = size(vertex{j+1},2);
    
    %%%% SCALING %%%%%
    if dir==-1 && do_scaling
        f(nj+1:nj1) = f(nj+1:nj1) / sqrt(2^(J-1-j));
    end

    %%%% Reverse UPDATE STEP %%%%
    if dir==-1 && do_update        
        for k=nj+1:nj1  % for all the fine point
            % retrieve coarse neighbors
            e = compute_butterfly_neighbors(k, nj);
            f(e) = f(e) + I(k)./(2*I(e))*f(k);
        end
    end
    
%     figure;
%     idx=1;
    
    %%%% PREDICT STEP %%%%
    for k=nj+1:nj1  % for all the fine point
        % retrieve coarse neighbors
        [e,v,g] = compute_butterfly_neighbors(k, nj);
        % do interpolation with butterfly
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%  PENTON INFLUENCE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         %used to observe penton influence
%         
%         %at level 5
%         %pentone
%         if j==5 && (k==8363 || k==4195 || k==2595 || k==2579 || k==2987 )
% %         random points
% %         if j==5 && (k==6092 || k==6106 || k==6111 || k==5547 || k==5384 || k==5332 )
% 
%         %at level 3
%         %pentone
% %         if j==2 && (k==45 || k==47 || k==54 || k==79 || k==138 || k==43 ||...
% %            k==61 || k==70 || k==82 || k==142 || k==44 || k==64 || k==73 || ...
% %            k==96 || k==113 || k==46 || k==50 || k==57 || k==99 || k==117 )
%            
%         %random points
% %         if j==2 && (k==56 || k==63 || k==66 || k==76 || k==81 ||...
% %                 k==84 || k==89 || k==95 || k==101 || k==104 || k==110 ||...
% %                 k==115 || k==123 || k==125 || k==131 || k==135 || k==144 ||...
% %                 k==154 || k==159 || k==162) 
%                 
%         
%             %G is the weight of points on the top of the butterfly wings.
%             %V is the weight of points at the head and tail of the
%             %butterfly
%             %Val is the value of the scaling function at the child position
%             %E is the weight of the two points from the linear
%             %aprroximation
%             disp(['val G-V : ' num2str(   sum(f(g))/16 - sum(f(v))/8   )]);
%             disp(['val Val-E : ' num2str(  f(k)-sum(f(e))/2)]);
%             disp(['val wavelet coef : ' ...
%                 num2str(f(k) -(1/2*sum(f(e)) + 1/8*sum(f(v)) - 1/16*sum(f(g))))]);
%             disp('--------------');
%             
%             % parents distance (between the two points from e):         
%             dist_par=sqrt(sum((vertex{j+1}(:,e(1))-vertex{j+1}(:,e(2))).^2));
%   
%             disp(['parent points distance of point ' num2str(k) ' : ' num2str(dist_par) ]); 
%             disp(['and parents value difference : ' num2str( abs(f(e(1))-f(e(2))) )]);
%             disp('--------------');
% 
%             
%             % child distance in percentage of the parents distance (distance between the points from e):
%             dist_child=100* sqrt(sum((vertex{j+1}(:,e(1))-vertex{j+1}(:,k)).^2))/dist_par;
%             
%             disp(['child-parent : ' num2str(dist_child)  ' %' ]);
%             
%             disp('-----------');
%             disp(['corrected value of Val-E: ' num2str(f(k) - (dst_che1*f(e(1)) + (1-dst_che1)*f(e(2))))]);
%             disp('-----------');
%             dist_interp=100*(f(k)-f(e(1)))*dist_par/(f(e(2))-f(e(1)));
%             disp(['distance of interpolation' num2str(dist_interp) ' %']);
%             
%             
%             
%             %influence of the top line and the bottom line of the butterfly
%             %the loops seem useless since every time the result is ind_1=1
%             %and ind_2=2
%             vrhigh=compute_vertex_ring(face{j});
%             
%             for ind_1=1:3
%                 for ind_2=ind_1+1:4
%                     if ~isempty(find(vrhigh{v(1)}==g(ind_1), 1)) && ~isempty(find(vrhigh{v(1)}==g(ind_2), 1))
%                         disp('------------');
%                         disp(['the first component of G-V is : ' num2str( ( (f(g(ind_1))+f(g(ind_2)))/2-f(v(1)) )/8 )]);
% 
%                         other_side=[1,2,3,4];      
%                         other_side=other_side(other_side~=ind_1 & other_side~=ind_2);
% 
%                         disp(['the second component of G-V is : ' num2str( ( (f(g(other_side(1)))+f(g(other_side(2))))/2-f(v(2)) )/8 )]);
% 
%                     end
%                 end
%             end
%             disp('------------');
%             disp('------------');
%             
%             xe1=0;
%             xe2=100;
%             
%             
%             subplot(2,3,idx);
%             plot([0,dist_par],[f(e(1)),f(e(2))]);
%             hold on
%             plot([dist_interp/100,sqrt(sum((vertex{j+1}(:,e(1))-vertex{j+1}(:,k)).^2))],[f(k),f(k)],'*r');
%             plot([
%             idx=idx+1;
%         end
%         
%         %make a pause at the end of a first analysis.
%         if k==6992 || k==162
%             keyboard;
%         end
        
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%  Wavelet coefficient COMPUTATION %%%%%%%%%%%%%%%%%%%%%


        
%         %formula when the child position is taking into account
%         if j==4 || j==5 || j==6
%         dist_par_e=sqrt(sum((vertex{j+1}(:,e(1))-vertex{j+1}(:,e(2))).^2));
% 
%         dst_che1=sqrt(sum((vertex{j+1}(:,e(1))-vertex{j+1}(:,k)).^2))/dist_par_e;
%         
%         
% %         dist_par_v1=sqrt(sum((vertex{j+1}(:,g(1))-vertex{j+1}(:,g(2))).^2));
% 
% %         dst_chv1=sqrt(sum((vertex{j+1}(:,g(1))-vertex{j+1}(:,k)).^2))/dist_par_v1;
% 
% %         infl_v1=( f(v(1)) - dst_chv1*f(g(1)) - (1-dst_chv1)*f(g(2)) )/8; 
%         
% 
% %         dist_par_v2=sqrt(sum((vertex{j+1}(:,g(3))-vertex{j+1}(:,g(4))).^2));
% 
% %         dst_chv2=sqrt(sum((vertex{j+1}(:,g(3))-vertex{j+1}(:,k)).^2))/dist_par_v2;
% 
% %         infl_v2=( f(v(2)) - dst_chv2*f(g(3)) - (1-dst_chv2)*f(g(4)) )/8;   
%         
%         
% %         fi= dst_che1*f(e(1)) + (1-dst_che1)*f(e(2)) + infl_v1 + infl_v2;
%         fi= dst_che1*f(e(1)) + (1-dst_che1)*f(e(2)) + 1/8*sum(f(v)) - 1/16*sum(f(g));
%         else
        
        %formula when the child position are right in the middle of a
        %segment
        fi = 1/2*sum(f(e)) + 1/8*sum(f(v)) - 1/16*sum(f(g));

%         end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
          
        if dir==1
            f(k) = f(k) - fi;
        else
            f(k) = f(k) + fi;
        end
    end

    %%%% Direct UPDATE STEP %%%%
    if dir==1 && do_update
        for k=nj+1:nj1  % for all the fine point
            % retrieve coarse neighbors
            e = compute_butterfly_neighbors(k, nj);
            f(e) = f(e) - I(k)./(2*I(e))*f(k);
        end
    end    
    
    %%%% SCALING %%%%%
    if dir==1 && do_scaling
        f(nj+1:nj1) = f(nj+1:nj1) * sqrt(2^(J-1-j));
    end
end
end
