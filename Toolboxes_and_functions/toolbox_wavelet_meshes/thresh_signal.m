function [level,th, em] = thresh_signal(I,num_bins, n)
%thresh_signal Global signal threshold using Otsu's method.
%   LEVEL = thresh_signal(I) computes a global threshold (LEVEL) that can be
%   used to detect points with high values. LEVEL
%   is a normalized intensity value that lies in the range [0, 1].
%   thresh_signal uses Otsu's method, which chooses the threshold to minimize
%   the intraclass variance of the thresholded low and high points of the signal.
%
%   [LEVEL EM] = thresh_signal(I) returns effectiveness metric, EM, as the
%   second output argument. It indicates the effectiveness of thresholding
%   of the input image and it is in the range [0, 1]. The lower bound is
%   attainable only by images having a single gray level, and the upper
%   bound is attainable only by two-valued images.
%
%   Class Support
%   -------------
%   The input image I can be uint8, uint16, int16, single, or double, and it
%   must be nonsparse.  LEVEL and EM are double scalars. 
%
%   Example
%   -------
%       I = imread('coins.png');
%       level = graythresh(I);
%       BW = im2bw(I,level);
%       figure, imshow(BW)
%
%   See also IM2BW.

%   Copyright 1993-2009 The MathWorks, Inc.
%   $Revision: 1.9.4.10 $  $Date: 2009/11/09 16:24:24 $

% Reference:
% N. Otsu, "A Threshold Selection Method from Gray-Level Histograms,"
% IEEE Transactions on Systems, Man, and Cybernetics, vol. 9, no. 1,
% pp. 62-66, 1979.

% One input argument required.
% iptchecknargin(1,1,nargin,mfilename);
% iptcheckinput(I,{'uint8','uint16','double','single','int16'},{'nonsparse'}, ...
%               mfilename,'I',1);

if ~isempty(I)
    
  % Convert all N-D arrays into a single column.  Convert to uint8 for
  % fastest histogram computation.
  
%   I = im2uint8(I(:));
% 
%   disp(I)
%   num_bins = 256;
%   counts = imhist(I,num_bins);
  
%   num_bins = ceil(max(I));
  [counts, center] = hist(I, num_bins);
%   figure
%   hist(I, num_bins);
%   disp('center');disp(center);
  
  % Variables names are chosen to be similar to the formulas in
  % the Otsu paper.
  p = counts / sum(counts);
  omega = cumsum(p);
  mu = cumsum(p .* (1:num_bins));
  mu_t = mu(end);
  
  if  n == 2
      sigma_b_squared = (mu_t * omega - mu).^2 ./ (omega .* (1 - omega));

      % Find the location of the maximum value of sigma_b_squared.
      % The maximum may extend over several bins, so average together the
      % locations.  If maxval is NaN, meaning that sigma_b_squared is all NaN,
      % then return 0.
      maxval = max(sigma_b_squared);
      isfinite_maxval = isfinite(maxval);
      if isfinite_maxval
        idx = mean(find(sigma_b_squared == maxval));
%         disp('idx');
%         disp(idx);
        th = center(floor(idx));
%         disp(th);
        % Normalize the threshold to the range [0, 1].
       level = (idx - 1) / (num_bins - 1);
      else
        level = 0.0;
      end
  elseif n == 3
      
      omega0 = omega;
      omega2 = fliplr(cumsum(fliplr(p)));
      [omega0,omega2] = ndgrid(omega0,omega2);
    
      mu0 = mu./omega;
      mu2 = fliplr(cumsum(fliplr((1:num_bins).*p))./cumsum(fliplr(p)));
      [mu0,mu2] = ndgrid(mu0,mu2);
    
      omega1 = 1-omega0-omega2;
      omega1(omega1<=0) = NaN;
    
      sigma2B =...
          omega0 .* ( mu0 - mu_t ).^ 2 + omega2 .* ( mu2 - mu_t ).^2 +...
          (omega0.*( mu0 - mu_t ) + omega2.*( mu2 - mu_t ) ).^2 ./ omega1;
      sigma2B(isnan(sigma2B)) = 0; % zeroing if k1 >= k2
      
%       disp('sigma2B is');
%       disp(sigma2B)
%       disp(size(sigma2B));
      [maxval,k] = max(sigma2B(:));
%       disp(sigma2B(k))                                                            
%       disp('maxval is');
%       disp(maxval)
      [k1,k2] = ind2sub([num_bins num_bins],k);
      th1 = center(floor(k1));
      th2 = center(floor(k2));
      th = [th1 , th2];                                                                                                                                                                                                                                                                                                                                                                                                                                                
%       disp('th are');
%       disp(th);
%       disp(I);

      % segmented image
      IDX = ones(size(I))*3;
      IDX(I <= (th1)) = 1;
      IDX(I >  (th1) & I <=  (th2)) = 2;
%       disp(IDX);
      
      level = 0.0;
      
      isfinite_maxval = isfinite(maxval);
%       disp(isfinite_maxval)
  end
  
  
else
  level = 0.0;
  isfinite_maxval = false;
end



% compute the effectiveness metric
if nargout > 1
  if isfinite_maxval
    em = maxval/(sum(p.*((1:num_bins).^2)) - mu_t^2);
  else
    em = 0;
  end
end


