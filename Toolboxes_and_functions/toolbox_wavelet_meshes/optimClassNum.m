function [ n, cluster ] = optimClassNum( X, maxCl ,plot_crit)
% This function computes the optimal number of clusters in which we should
% separates the spherical wavelet coefficientsat one precise level. The
% clutering method is the Kmeans one and the criterion to select the best
% number of classes is computed based on the same principle. 

% INPUT: X : The 1D vector of swav coefficients at one precise level
%        maxCl : the m,aximal number of clusters we want to test. 
% OUTPUT: n: the optimal number of clusters 
% 

% We test for different number of classes : 
k = 2:maxCl;



Dinter = zeros(1,length(k));
Dintra = zeros(1,length(k));

Idx = zeros(size(X,1),length(k));
minCrit = zeros (1, length(k))+inf; 

% The computation of the criteria interClass and IntraClass requires the
% computation of the centroid :
Centroid = mean(X,1);
MaxIter = 100;
critere = zeros(MaxIter,length(k));

% Since the initilization is random, we will test the results on 100
% iterations :

for iter = 1: MaxIter
    for j = 1:length(k)
        [Idx(:,j),C,sumD] = kmeans(X,k(j));
        
        % Computation of the inter class criterion, it corresponds to the
        % sum of the classes of the number of point in each class by the distance of the
        % centroid of the class to the centroid of the set of points:
        dinter = 0;
        for i = 1:k(j)
            dinter = dinter + size(find(Idx(:,j)==i),1)*(sum((C(i,:)-Centroid).^2));
        end

        % sumD is the vector of the mean squared distance from each points to its
        % centroid
       Dinter(j) = dinter; 
       Dintra(j) = sum(sumD);
       critere(iter,j) =  Dintra(j)/(Dinter(j));
    end
    
    for n = 1:length(k)
        if (minCrit(n)> critere(iter,n))
            minCrit(n) = critere(iter,n);
        end
    end
end



% RESULTS FOR THE NUMBER OF CLASSES
%     figure
%     plot(k,Dintra,'r');
%     title('evolution of the mean distance of the points from their centroids, regarding r');
%     hold on 
%     plot(k,Dinter,'b');
%     legend('Within Class Distance','Between Class Distance');

C = mean(critere,1).*k.^2 ;

if plot_crit

%     figure
    plot(k,minCrit,'g','lineWidth',1.5);
    hold on 
    plot(k,minCrit.*k,'r');
    hold on
    plot(k,C,'b');
    legend('ratio Dintra/Dinter','Dintra*n/Dinter', 'mean.*n.^2');
    title(['evolution of the criterion on ',num2str(MaxIter),' iterations']);
end


disp(['Best class number found on the mean of the ' num2str(MaxIter) ' initialization of each class (number taken as output):']);
idxC = find(C == min(C))+1;
disp(idxC);
disp('Best class number found on one random initialization :')
idxK = find(minCrit.*k == min(minCrit.*k))+1;
disp(idxK);

n= idxC;
 


% RESULTS OF THE CLASSIFICATION:

disp(['the size of the Index matrix is : ', num2str(size(Idx))]);
cluster = zeros(length(Idx),n);
% size(cluster)


for i = 1:n
    cluster(:,i) = (Idx(:,(n-1)) == i);
end

% According to the graph, we will stop at 5 classes :
% class1 = find(Idx(:,4)==1); class1r = class1-1; % the tests start at 0...
% class2 = find(Idx(:,4)==2); class2r = class2-1;
% class3 = find(Idx(:,4)==3); class3r = class3-1;
% class4 = find(Idx(:,4)==4); class4r = class4-1;
% class5 = find(Idx(:,4)==5); class5r = class5-1;
% 
% class4_1 = find(Idx(:,3)==1); class4_1r = class4_1-1;
% class4_2 = find(Idx(:,3)==2); class4_2r = class4_2-1;
% class4_3 = find(Idx(:,3)==3); class4_3r = class4_3-1;
% class4_4 = find(Idx(:,3)==4); class4_4r = class4_4-1;
% 
% class3_1 = find(Idx(:,2)==1); class3_1r = class3_1-1;
% class3_2 = find(Idx(:,2)==2); class3_2r = class3_2-1;
% class3_3 = find(Idx(:,2)==3); class3_3r = class3_3-1;
% 
% class2_1 = find(Idx(:,1)==1);
% class2_2 = find(Idx(:,1)==2);

end

