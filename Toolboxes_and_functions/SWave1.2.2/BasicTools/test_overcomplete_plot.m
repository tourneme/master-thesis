base_path_1='/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/toolbox_wavelet_meshes/';
path(path, base_path_1);
path(path, [base_path_1 'toolbox/']);

% load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode/test.mat');

load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/SWave1.2.2/overcomplete_wavelets/ReleaseSampleCode/WaveletBankBW500inv200.mat');
check_bool=0;




disp('Performing Wavelet Decomposition');


load('/Users/cducroz/Documents/Robin/CODE MATLAB/Toolboxes and functions/soho/data_sph/Amoebas treated/06012010_Wt-1H_smo/06012010_Wt-1H_#0009_smo/06012010_Wt-1H_#0009_T0030_CALD_smo.mat');


vertices=vertices-repmat(mean(vertices,1),length(vertices),1);


vertexFaces =  MARS_convertFaces2FacesOfVert(int32(faces), int32(size(sph_verts, 1)));
num_per_vertex = length(vertexFaces)/size(sph_verts,1);
vertexFaces = reshape(vertexFaces, size(sph_verts,1), num_per_vertex);

vertexNbors = MARS_convertFaces2VertNbors(int32(faces), int32(size(sph_verts,1)));
num_per_vertex = length(vertexNbors)/size(sph_verts,1);
vertexNbors = reshape(vertexNbors, size(sph_verts,1), num_per_vertex);

sbjMesh.vertices=sph_verts';
sbjMesh.metricVerts=vertices';
sbjMesh.faces=faces';
sbjMesh.vertexFaces=vertexFaces';
sbjMesh.vertexNbors=vertexNbors';

[recon_cell, analysis_cell] = PartiallyReconMesh(sbjMesh, analysis_filters_my, synthesis_filters_my, check_bool);


close all

for i=1:length(analysis_cell)

a=analysis_cell{i};
b=recon_cell{i};

t=sqrt(sum(a.vertices.^2,1));

figure;
s_handle=trisurf(b.faces',b.vertices(1,:)',b.vertices(2,:)',b.vertices(3,:)',t);
shading interp;
view(90, 0);
camlight('headlight', 'infinite');
view(270, 0);
camlight('headlight', 'infinite');
axis off;
set(s_handle, 'DiffuseStrength', 1);
set(s_handle, 'SpecularStrength', 0);
axis equal;

end