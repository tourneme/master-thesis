% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function DecomposeSubjectsIntoWavelets(hemi)

DW_atlas = CreateEmptyAtlas;
parms = DW_atlas.parms;

parms.hemi = hemi;
parms.SUBJECTS_DIR = '/afs/csail/group/vision/cortex/ythomas/ManualLabeled/';
parms.uniform_dir = '/afs/csail/group/vision/cortex/ythomas/work/MARS/';
parms.surf_filename = 'sphere.unfolded.rotated';
parms = rmfield(parms, 'data_filename_cell');
parms.WORK_DIR = 'DW';
parms.uniform_meshes = {'ic4.tri'  'ic5.tri'  'ic6.tri'  'ic7.tri'};
parms.metric_surf_filename = 'white';
parms

numSubjects = length(parms.subject_cell);

load('WaveletBankBW500inv200.2.mat');
for i = 1:numSubjects
    disp(num2str(i));
    parms.SUBJECT = parms.subject_cell{i};
    MARS_sbjMesh = MARS2_readSbjMesh(parms);

    [recon_cell, coeffs_mat_x, coeffs_mat_y, coeffs_mat_z] = PartiallyReconMesh(MARS_sbjMesh, analysis_filters_my, synthesis_filters_my);

    for j = 1:length(recon_cell)
        write_surf( single(transpose(recon_cell{j}.vertices)), single(transpose(recon_cell{j}.faces))-1, ...
            [parms.SUBJECTS_DIR '/' parms.SUBJECT '/surf/' parms.hemi '.wavelet2.' num2str(j)]);
    
    end
end