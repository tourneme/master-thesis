function values = sampFnRequestedGrid(theta, phi, Functor, vecBool, varargin)

%sampFnRequestedGrid will calculate function values of functor at requested
%theta and phi. If vecBool == 1, then we assume Functor can handle a whole
%vector of theta and phi. This will be faster...
values = zeros(length(theta)*2, 1);
if(vecBool==0)
    for i = 1:length(theta)
        if(isempty(varargin))
            fnVal = feval(Functor, theta(i), phi(i));
        else
            fnVal = feval(Functor, theta(i), phi(i), varargin{1:end});
        end
        values(2*i-1) = real(fnVal);
        values(2*i) = imag(fnVal);
    end
else
    if(isempty(varargin))
        fnVals = feval(Functor, theta, phi);
    else
        fnVals = feval(Functor, theta, phi, varargin{1:end});
    end
    values(1:2:end-1) = real(fnVals);
    values(2:2:end) = imag(fnVals);
end