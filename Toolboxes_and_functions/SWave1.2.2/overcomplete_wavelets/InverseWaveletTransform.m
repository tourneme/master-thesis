% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function output = InverseWaveletTransform(wavelet_coeffs_prefix, synthesis_filename_prefix, write_bool, output_prefix, scales)

% output = InverseWaveletTransform(wavelet_coeffs_prefix, synthesis_filename_prefix, write_bool, output_prefix, scales)

for i = 1:length(scales)

    disp(num2str(scales(i)));
    image = load([wavelet_coeffs_prefix num2str(scales(i)) '.s2']); 
    if(i == 1)
        output = zeros(length(scales), length(image));
    end
    
    filter = load([synthesis_filename_prefix num2str(scales(i)) '.s2']);
    
    %if(write_bool)
    %    output_filename = [output_prefix num2str(scales(i)) '.s2'];
    %else
    %    output_filename = [];
    %end
    
    %output(i, :) = ConvolveSpheres(image, filter, write_bool, output_filename, 0);
    output(i, :) = ConvolveSpheres(image, filter, 0, [], 0);
    
end

if(write_bool)
    out_sum = sum(output(1:end,:),1);
    output_filename = [output_prefix '.s2'];
    fid = fopen(output_filename,'w');
    fprintf(fid, '%1.15f\n', out_sum);
end    
   
