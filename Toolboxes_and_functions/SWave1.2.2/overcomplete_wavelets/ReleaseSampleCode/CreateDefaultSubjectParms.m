% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function subject_parms = CreateDefaultSubjectParms(hemi)

if(nargin == 1)
    subject_parms.hemi = hemi;
else
    subject_parms.hemi = 'lh';
end

subject_parms.SUBJECTS_DIR = fullfile('..','..','example_surfaces');
subject_parms.uniform_mesh_dir = fullfile('..','..');

subject_parms.read_surface = @MARS2_readSbjMesh;
subject_parms.radius = 100;
subject_parms.surf_filename = 'sphere';
subject_parms.metric_surf_filename = 'white';
subject_parms.data_filename_cell = {'inflated.H', 'sulc', 'curv'}; 
subject_parms.normalizeBool = 1; % 1 implies it will normalize the data that is read in.
subject_parms.unfoldBool = 1;
subject_parms.flipFacesBool = 1; % Probably do not want to change this!!
subject_parms.uniform_meshes = {'ic4.tri', 'ic5.tri', 'ic6.tri', 'ic7.tri'};
subject_parms.subject_cell = {'OAS1_0001_MR1' ,...
    'OAS1_0002_MR1' ,...
    'OAS1_0003_MR1' ,...
    'OAS1_0004_MR1' ,...
    'OAS1_0005_MR1'};
