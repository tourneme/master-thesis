% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
% 
%
% Just type "CreateLaplacianAnalysisSynthesisFilters" without any arguments and things should work!
%
% This function creates a set of analysis and corresponding synthesis filters and 
% save them as ['WaveletBankBW' num2str(BW) 'inv' num2str(max_invertibility) '.mat']; 
% There are 3 input parameters you can play with, but the default should be good for most purposes.
%
% What to expect:
%           By default, the function will create 7 levels of filters (although we only set 6 kernel sizes). 
%           The whole process should take only a few minutes.
%           The filters will be invertible up to degree = BW - 1 = 500 - 1 = 499
%
% This function
%       1)  For each of the 6 kernel sizes, sample the corresponding laplacian filter assuming BW = 500 (so 1000 x 1000)
%       2)  Use yawtb interface to S2kit, to perform fast Spherical Harmonic Transform of filter
%       3)  Compute frequency responses of the analysis filters up max_invertibilty
%       4)  Compute synthesis filters so that the combined analysis and synthesis filters result in invertibility up to max_invertibility.
%       
%           So why not compute it all the way up to BW = 500, which is the actual final invertibility? 
%           
%               From Figure 1 (after you run the code), you will see that the harmonic coefficients of the 6 filters 
%               decay after degree 100 and is essentially 0 after degree 250. If we normalize the system to be invertible up to BW = 500,
%               the synthesis filters would be unstable for values outside of 250. Therefore, we introduce a 7th analysis and synthesis 
%               filter, known as the residual high pass filters. As seen in Figure 3 (after you run the code). The
%               frequency response of the first 6 filters is 1 up to degree 199. The 7th set of filters ensure invertibility up to 
%               BW = 500. Also notice in Figure 2, that the 5 synthesis filters have similar shape. We did not plot the 6th
%               synthesis filter because it dominates the first 5 filters. This is a result of the fact that only the 6th analysis
%               filter is non-zero beyond degree 140, and so the 6th synthesis filter has to compensate for that.        
%                   
%      5)   Once the first 6 analysis-synthesis filters are computed, we look at the remaining spectrum between max_invertibility and BW
%      (500), and we divide the spectrum equally between the 7th set of analysis-synthesis filters.
%
% The general theories behind the overcomplete wavelet transform is based on the following papers (http://yeoyeo02.googlepages.com/publications).
% We note that the theories in the paper is far more general than the overcomplete wavelet codes we provide here.
% The paper also discussed the construction of spherical steerable pyramid, which are overcomplete wavelet with oriented filters unlike the
% axisymmetric laplacian filters we use here. 
%
%           (a) On the Construction of Invertible Filter Banks on the 2-Sphere.  
%               B.T.T. Yeo, W. Ou and P. Golland. 
%               IEEE Transactions on Image Processing, 17(3):283--300, 2008
%
%           (b) Invertible Filter Banks on the 2-Sphere. 
%               B.T.T. Yeo, W. Ou and P. Golland. 
%               Proceedings of the IEEE International Conference on Image Processing (ICIP), 2161--2164, 2006
%
% The application of these overcomplete spherical wavelets to cortical surfaces is based on the following papers
% (http://yeoyeo02.googlepages.com/publications):
%
%           (c) Shape Analysis with Overcomplete Spherical Wavelets. 
%               B.T.T. Yeo, P. Yu, P.E. Grant, B. Fischl, P. Golland. 
%               Proceedings of the International Conference on Medical Image Computing and Computer Assisted Intervention (MICCAI), 
%               volume 5241 of LNCS, 468--476, 2008 
%
%           (d) Cortical Folding Development Study based on Over-complete Spherical Wavelets. 
%               P. Yu, B.T.T. Yeo, P.E. Grant, B. Fischl and P. Golland. 
%               Proceedings of the Workshop on Mathematical Methods in Biomedical Image Analysis, International Conference on Computer Vision, 2007
%
%
%MODIFIED BY: Robin Tournemenne the 26/07/2013 "creation of create_SphWavelets" 


function CreateLaplacianAnalysisSynthesisFilters(checkBool,varargin)
close all

global RELATIV_ROOT

base_path_1=[ RELATIV_ROOT 'Toolboxes and functions/toolbox_wavelet_meshes/'];
path(path, base_path_1);
path(path, [base_path_1 'toolbox/']);

if(nargin < 1)
   checkBool = 1; %checkBool tells whether to do some checking. With checking, things are slower but you get the peace of mind!
end

BW = varargin{1};
max_invertibility = varargin{2}; %Degree above which we create a high pass filter.
kernel_sizes = varargin{3}; % number of filters will be equal to kernel_sizes + 1.


%%%%%%%%%%%%%%%%%%%%%%
% Real Code Begins
%%%%%%%%%%%%%%%%%%%%%%

numFilters = length(kernel_sizes);
analysis_filters = zeros(numFilters+1, BW);
synthesis_filters = zeros(numFilters+1, BW);

% First Create Analysis Filters.
disp('Creating Analysis Filters');
for i = 1:length(kernel_sizes)
    
    [~, ~, values]=create_SphWavelets(BW,kernel_sizes,i,0);
    
    % Forward Spherical Harmonic Transform
        
    values_yawtb_compatible = reshape(values(1:2:end), BW*2, BW*2)';
    s2kit_harm_coefficients = s2fst(values_yawtb_compatible);
    
    if(checkBool)
        % Inverse Spherical Harmonic Transform
        recovered_values = s2ifst(s2kit_harm_coefficients);
        disp(['imag part (should be zero): ' num2str(max(imag(recovered_values(:)))) ', diff in real part (should be close to 0): ' num2str(max(abs(real(recovered_values(:)) - values_yawtb_compatible(:))))]);
    end
    
    % Convert into my format
    my_harm_coefficients = ConvertS2kitFormat2MyFormat(s2kit_harm_coefficients);
    analysis_filters(i, :) = ExtractWithinMRangeFromMyFormat(my_harm_coefficients, 0, 0); 
end

%Compute Frequency response of analysis filter up to max_invertibility.
disp(['Computing Frequency Response of Analysis Filters up to ' num2str(max_invertibility)]);

fr = 8*pi^2./(2*(0:max_invertibility-1)+1).* sum(analysis_filters(:, 1:max_invertibility).^2, 1); %here is H defined in the eq. 9 of the paper

fr = 1./fr; %classic self invertible formula from the equ 12.

% SMOOTHLY pad the frequency response after max_invertibility 
numEntries = BW - max_invertibility;
append = 1./ (1 + exp(linspace(-10,100,numEntries))) * fr(end);
fr = [fr append];

%Compute the synthesis filters to ensure invertibility up to max_invertibility
disp(['Computing Synthesis Filters to ensure invertibility up to ' num2str(max_invertibility)]);
for i = 1:length(kernel_sizes)
   synthesis_filters(i, :) = (fr) .* analysis_filters(i, :);   
end

%Check last analysis and synthesis filter to ensure invertibility up to BW
disp(['Compute last analysis and synthesis filter to ensure invertibility up to' num2str(BW)]);
freq_resp_mat = repmat(8*pi^2./(2*(0:BW-1)+1), numFilters, 1) .* analysis_filters(1:numFilters, 1:BW) .* synthesis_filters(1:numFilters, 1:BW);
residual =  (2*(0:BW-1) +1)/(8*pi^2).* (ones(1, BW) - sum(freq_resp_mat, 1)); %theorem 4.2
analysis_filters(end, :) = sqrt(residual);
synthesis_filters(end, :) = sqrt(residual);

if(checkBool)
    figure; hold on;
    for i = 1:numFilters
        plot(abs(analysis_filters(i, :)), 'r')
    end
    title('First 6 Analysis Filters. Last one is for normalization so not plotted.');
    
    figure; hold on;
    for i = 1:numFilters-1
        plot(abs(synthesis_filters(i, :)), 'r')
    end
    title('First 5 Synthesis Filters. Remaining ones not plotted because of normalization effects.');
    
    % Compute final frequency response
    freq_resp_mat = repmat(8*pi^2./(2*(0:BW-1)+1), numFilters+1, 1) .* analysis_filters .* synthesis_filters;
    figure; hold on;
    for i = 1:numFilters+1
        plot(freq_resp_mat(i, :), 'r');
    end
    plot(sum(freq_resp_mat, 1), 'b');
    title('Red: Frequency Response of individual filters. Blue: Total Response (should be equal to 1)');
end

%Cleaning up before saving
disp('Rearranging filters into my format before saving');
analysis_filters_my = zeros(numFilters+1, (BW)*(BW+1)/2);
synthesis_filters_my = zeros(numFilters+1, (BW)*(BW+1)/2);

for i = 1:numFilters+1
    analysis_filters_my(i, :) = InsertWithinMRangeIntoMyFormat(analysis_filters(i, :), 0, 0, BW-1);
    synthesis_filters_my(i, :) = InsertWithinMRangeIntoMyFormat(synthesis_filters(i, :), 0, 0, BW-1);
end

kernel1=num2str(kernel_sizes(1));
kernelend=num2str(kernel_sizes(end));

save_path = [RELATIV_ROOT 'results/axisymmetric_filter/LoG/LaplacianBW' num2str(BW) 'inv'...
    num2str(max_invertibility) 'kernel' kernel1(3:end) 'to' ...
    kernelend(3:end) 'sigma' num2str(varargin{4}) '.mat'];
display(['Saving Filters in ' save_path]);
save(save_path, 'analysis_filters_my', 'synthesis_filters_my');



