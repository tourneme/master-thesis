% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function coefficients = convolveSphere(coeffs1, coeffs2, forwardBool)

%We assume coeffs1 corresponds to f, coeffs2 corresponds to g.
%and we assume f*g (left convolution)

%B1 gives bandwidth of coeffs1
%B2 gives bandwidth of coeffs2
B2 = 1/2 *(-1 + sqrt(1+8*length(coeffs2)));
B1 = 1/2 *(-1 + sqrt(1+ 8*length(coeffs1)));
BW = min(B1, B2);

if(nargin < 3)
   forwardBool = 1; 
end

count = 0;
factor = [];
for i = 0:(BW-1)
    
    count = count + 1;
    if(forwardBool)
        factor = [factor; repmat(sqrt(4*pi/(1 + 2*i))*coeffs2(length(factor)+1), count,1)];
    else
        factor = [factor; repmat(2*pi*sqrt(4*pi/(2*i+1))*coeffs2(length(factor)+1), count,1)];
    end
end

if (B2 < B1)

coefficients = coeffs1(1:length(coeffs2)) .* factor;

else

coefficients = coeffs1 .* factor;

end

