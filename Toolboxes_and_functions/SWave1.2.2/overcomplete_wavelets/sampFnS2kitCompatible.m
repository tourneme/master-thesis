function [theta, phi, values] = sampFnS2kitCompatible(Functor, N, writeBool, filename, vecBool, varargin)

%Samples (2N)^2 samples according to S2kit.
%WriteBool = 1 if you want to write to file.
%Functor is a function pointer. The function is assumed to take in 2
%arguments, first argument is theta, second argument is phi.
%Functor will be used by sampFnRequestedGrid. sampFnRequestedGrid will call
%feval(Functor, theta, phi)
%If vecBool = 1, then Functor can take in a whole range of theta and phi
%and output a whole range of theta and phi, this can speed things up.
%
% theta1, phi1
% theta1, phi2
%   .......
% theta1, phi2N
% theta2, phi1
%   .......
% theta2, phi2N

TotalSamples = (2*N)^2;
values = zeros(TotalSamples, 1);

thetaVec = pi/(4*N):pi/(2*N):(4*N-1)*pi/(4*N);
theta = repmat(thetaVec, 2*N, 1);
theta = reshape(theta, TotalSamples, 1);

phiVec = (0:2*pi/(2*N) : (2*N-1)*2*pi/(2*N))'; %Notice the transpose
phi = repmat(phiVec, 1, 2*N);
phi = reshape(phi, TotalSamples, 1);

if(isempty(varargin))
    values = sampFnRequestedGrid(theta, phi, Functor, vecBool);
else
    values = sampFnRequestedGrid(theta, phi, Functor, vecBool, varargin{1:end});
end
    
if(writeBool)
    fid = fopen(filename, 'w');
    fprintf(fid, '%1.15f\n', values);
end