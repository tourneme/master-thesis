% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function mat = ConvertMyFormat2S2kitFormat(vec)

% S2kit format is output of ilmshape and s2fst:
%
% [COMPLEX MATRIX]: The spherical harmonic
% coefficients. Those are defined in a matrix of size B*B (with B
% define above). This matrix is formed by the concataining of the
% coefficents C(m,l) ($|m|\leq l$) in the following order:
%   C(0,0) C(0,1) C(0,2)  ...                 ... C(0,B-1)
%          C(1,1) C(1,2)  ...                 ... C(1,B-1)
%          etc.
%                                   C(B-2,B-2)    C(B-2,B-1)
%		                                  C(B-1,B-1)
%			                          C(-(B-1),B-1)
%		                    C(-(B-2),B-2) C(-(B-2),B-1)
%	  etc.
%	          C(-2,2) ...                 ... C(-2,B-1)
%	  C(-1,1) C(-1,2) ...                 ... C(-1,B-1)
%    
%   This only requires an array of size (B*B) but is very difficult
%   to read.
% 
% My format (assumes we represent real images) and is:
% 
% B - 1 = bandwidth = max degree;
% Coeffs(1) corresponds to coefficient of l = 0, m = 0
% Coeffs(2) corresponds to coefficient of l = 1, m = 0
% Coeffs(3) corresponds to coefficient of l = 1, m = 1
% Coeffs(4) corresponds to coefficient of l = 2, m = 0 and so on
% ...
% Coeffs((L+1)L/2 + m + 1) corresponds to coefficient of degree L and order m


B = 1/2 *(-1 + sqrt(1+8*length(vec)));
L = B - 1;
mat = zeros(B, B+1); %note that we will discard last column of B entries later.

% Create l, m index of my format
[pos_i, pos_j] = meshgrid(0:L);
index = find(pos_j <= pos_i);
l = pos_i(index);
m = pos_j(index);

% We assume m >= 0 in my format, so
seanindex = m.*(L + 1) - (m.*(m-1))/2 + (l -m) + 1; % +1 because of c-index. This formula copied from FST_semi_fly.c
mat(seanindex) = vec;

% We still need to fill out m < 0
m = -m; 
seanindex = ( ( L * ( L + 3 ) ) /2 ) + 1 + ( ( L + m ) .* ( L + m + 1 ) / 2 ) +  (l - abs(m)) + 1; % +1 because of c-index. This formula copied from FST_semi_fly.c
mat(seanindex) = (-1).^(-m) .* conj(vec);

%unfortunately sean-index now includes m = 0 again..., so we discard last B
%entries
mat = transpose(mat(1:B, 1:B));
























