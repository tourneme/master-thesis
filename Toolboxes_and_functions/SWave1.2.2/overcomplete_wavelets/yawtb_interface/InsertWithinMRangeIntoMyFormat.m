% Contact ythomas@csail.mit.edu or msabuncu@csail.mit.edu for bugs or questions 
%
%=========================================================================
%
%  Copyright (c) 2008 Thomas Yeo and Mert Sabuncu
%  All rights reserved.
%
%Redistribution and use in source and binary forms, with or without
%modification, are permitted provided that the following conditions are met:
%
%    * Redistributions of source code must retain the above copyright notice,
%      this list of conditions and the following disclaimer.
%
%    * Redistributions in binary form must reproduce the above copyright notice,
%      this list of conditions and the following disclaimer in the documentation
%      and/or other materials provided with the distribution.
%
%    * Neither the names of the copyright holders nor the names of future
%      contributors may be used to endorse or promote products derived from this
%      software without specific prior written permission.
%
%THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
%ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
%WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
%ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
%ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    
%
%=========================================================================
function out = InsertWithinMRangeIntoMyFormat(in, min_m, max_m, L)

% out = InsertWithinMRangeIntoMyFormat(in, min_m, max_m, L)
%
% My format (assumes we represent real images) and is:
% 
% B - 1 = bandwidth = max degree;
% Coeffs(1) corresponds to coefficient of l = 0, m = 0
% Coeffs(2) corresponds to coefficient of l = 1, m = 0
% Coeffs(3) corresponds to coefficient of l = 1, m = 1
% Coeffs(4) corresponds to coefficient of l = 2, m = 0 and so on
% ...
% Coeffs((L+1)L/2 + m + 1) corresponds to coefficient of degree L and order m
%
%
% inserts everything between min_m and max_m inclusive
% Note that if L(in) < L, then it will be padded with zeros
% If L(in) > L, it will be truncated!!

% Create l, m index of my format
[pos_i, pos_j] = meshgrid(0:L);
m = pos_j(pos_j <= pos_i);

%insert range
out = zeros( (L+1)*(L+2)/2, 1);

index = find(m >= min_m & m <= max_m);

if(length(in) <= length(index))
    out(index(1:length(in))) = in;
else
    out(index) = in(1:length(index));
    %error('Truncating Input!');
end