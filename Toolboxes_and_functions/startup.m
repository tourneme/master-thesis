%% YAWtb installation
    olddir=pwd;
    cd yawtb;
    yaload;
    cd(olddir);
    clear olddir;

%% SWave 1.2.2 installation
disp 'installing SWave 1.2.2'
    olddir=pwd;
    cd SWave1.2.2;
    add_all_paths;
    cd(olddir);
    clear olddir;
