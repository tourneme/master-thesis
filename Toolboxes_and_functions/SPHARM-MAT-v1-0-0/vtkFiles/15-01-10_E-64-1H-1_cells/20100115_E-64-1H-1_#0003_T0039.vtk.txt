# vtk DataFile Version 3.0
A great-looking cell
ASCII
DATASET POLYDATA
POINTS 278 double
332.172764352563 197.522224609225 70.38264227859135
347.5213957505941 204.88119896586522 66.40799299165745
333.218426677355 196.7174408778859 55.75421724348375
339.94483489799825 204.82114839959854 75.80846482559541
347.3665402214611 204.18106470813257 68.64929620850994
337.9454667098054 204.67445274720026 48.919442681866975
337.60854045675944 188.63504034982546 67.3845435662704
345.62842719533523 207.8766630453034 59.87520631136159
340.95553551862037 195.02638868145496 51.730221510069356
348.6156304202821 194.2356818819899 58.25401568425344
339.0218939901814 197.23244966330705 73.89923915188795
344.51020917486153 208.57254824829292 56.54302419317125
347.113395937966 206.59217144591017 61.039518010135694
334.2294100364549 204.59592671322756 55.177366445440335
337.6628810711266 208.7230668164415 65.52057462645267
334.7959015578591 194.60955835542305 54.23556868288942
340.40870466946393 209.10515569781674 66.51116617695467
335.0406997717032 201.11803368804382 52.352682711576676
342.4512203762447 189.7483435085185 57.06048941070578
336.0863447648175 206.93375430942956 70.71331877495041
341.16161878795873 199.37081140320674 75.7573843451919
336.72792936702666 202.70456676152168 49.37990074825481
335.62338537725435 207.06080917935225 56.10274643587634
337.3273401338713 188.46561520840604 60.90535927512789
340.8654641525109 202.34263706707455 46.64775414647662
337.6728667231218 207.8433409147321 54.04851735495351
333.1714351849056 199.2255303244285 63.44394577402853
340.7817310207066 198.5954225476048 48.78656631226801
342.19155535172735 188.393067969407 60.34409608536372
334.4946222619908 202.646716915089 53.39724614157164
339.96323704962657 208.78090181601004 55.49573124672175
333.33722356940547 202.5805151054964 63.019357322252915
340.2423195408949 204.33205529194888 48.28430490014688
341.6305100917023 200.127801480333 47.34836228662124
334.1411247056856 204.75877559777769 69.44090596611059
348.2290950368563 203.9945444815801 54.06825333781347
346.0609972771002 207.53026861605426 55.30880398210608
331.24119298089624 196.60178803245807 63.80926036597132
344.2650143825092 205.07886685239333 49.15457282686758
334.69819970506165 199.40460462701185 71.89360035160705
349.5403906969165 198.84416331671756 60.174365163285636
345.4769494095973 200.99409985646292 48.96114442025347
336.03741357019504 207.59871566895035 58.84457813975526
349.19602491495317 203.59130213466673 60.46800383022946
334.4963843083521 204.8216395399523 72.54545815741962
346.09848225224147 206.96405411142567 65.16328241369295
334.1419379903029 199.92357105253362 68.90041973026483
331.9407544209387 194.87188216997137 58.720199462608825
342.3318783074109 209.11233840393692 56.670515414685205
345.07847339832654 207.59774289243165 52.764439974459506
345.41914032416787 199.01003919147138 49.21710186219647
333.65935671205773 201.35265316388174 65.99928568482626
333.4872655519736 204.56761876148025 62.18609567381473
335.2345971772456 204.64159870780975 52.608099317830664
338.62591360478405 207.59232967358597 51.38074584753128
338.6281994207162 202.65813070856495 47.92618092009421
333.68371279127757 202.48330383221284 68.41887130018223
345.8286307819438 195.96893017764782 52.37333261499345
349.1657505531198 203.6414480470679 58.47830289083866
338.1636960414443 208.93558571382331 59.41998437497336
336.69771551408263 206.34558004194366 52.00607621073991
331.9338976012505 191.68502805736605 64.87175580437588
344.57958185766574 203.08827705010913 48.30464122528906
338.91642399076136 209.16514552160808 61.596491168694776
344.5380525307534 195.23472289297985 71.27149261200704
334.3733084208123 200.60499920105195 54.07645065450064
342.24465535317364 193.27369533017344 53.20735876481678
348.77132548793463 200.46391666512523 53.94506118533136
335.6508718123479 190.28085931737098 72.83334151728431
340.01923403298895 209.25976956774383 57.932598016082224
343.7385327402523 195.05564642199602 52.01869334964645
343.4524807163104 208.81907983584716 62.76209270760743
334.0078667319102 202.2380494950857 70.94599731832595
347.11810115997287 197.7019638023196 52.12337007015888
341.7760982371847 207.1806625932924 73.65239703759413
333.5383479149695 204.15189058968733 66.70597838615808
337.38280439849984 206.46065311935834 49.97710751167276
343.70882665361216 208.75881292665042 60.77039351275757
337.7451720471759 208.63755098073088 56.90824921017944
342.7287007208328 189.47270062739946 70.24264368320033
336.2759750529578 195.44912675008032 52.66853079328639
348.1819866681214 202.61587754339257 67.1939361984381
339.95205821894393 192.26930215455144 72.421018393013
349.50137184380185 202.4420150826412 62.14619788178368
342.1471794360106 196.76888568302164 73.36607523341594
348.78715832854186 203.82971033503512 56.26639176860223
336.46890930149857 200.48287387935142 50.769404418736585
348.35296466289907 196.3833736529226 56.080895421406524
331.09818878378206 196.34331928523207 68.44627848492117
331.09055858160514 195.05227056982773 61.62097239536218
336.03223832854985 189.16810203770038 66.14960316733477
346.09230376405105 206.36354106012774 68.31220053150219
336.3297628105467 198.96830858261555 74.71802228358838
346.88143040082224 189.94265752321687 67.05560589715357
331.8928316792148 191.7070376748859 68.24606857865555
331.07635320374146 194.47218605423265 69.93224558768759
334.03412819699514 202.0997264411495 55.33502001466506
336.92009486138284 189.73334019102043 57.47020498256804
343.8719506494906 189.1793081292104 67.98025131241486
339.69850228270326 209.16097494783764 63.840955287515285
337.6579902158679 208.80379298256602 63.20850498900463
348.679151085646 203.20680668612354 64.60915994184009
342.6789099196273 208.37618424341258 52.6070560111064
339.1838631723866 208.09243057301026 71.95078280018429
349.7158978689328 201.86266777699393 57.08872112986008
340.0235259206153 188.12437591694086 65.93527764196062
337.32495392087486 189.14488719489054 70.41481319154543
336.58589878678913 195.88757259871244 74.45791260874829
345.17912680849656 189.71573416666664 59.05796806628397
345.9223094349176 205.82808497702027 50.945937541673935
349.0786297279731 198.31179068831 56.38406426657184
340.1846542343692 195.48056042712318 72.88068217452376
335.26645520165175 193.18172709315462 74.06847605307374
332.1744555795811 193.2873751807167 72.72734788964854
342.15917919594807 208.985463687843 64.39718002224541
346.0941859695805 192.8191165152496 54.97578240613703
336.6682611564059 208.32652716814073 61.32753786202829
347.3620665862093 201.90990188830534 69.65053058728726
346.7460520655909 199.30718986979105 50.34388955315107
335.8071151817422 189.02933069822626 62.877598300597654
334.5311694221955 190.11657052709344 61.57614443713889
335.59349812441695 189.82142626550078 59.56523977809757
340.0961785525322 209.2398237556821 60.000404674431756
331.017286320868 193.98031069775115 63.63970226048529
335.19316892495203 202.71279766619205 51.21238124928409
338.6058353362732 198.79377155063517 75.67250833127387
344.599513991984 206.16238712734898 73.55853005505944
348.85770159964346 198.21563339766777 54.110284144716935
335.1118604110892 189.65237639091742 70.66755708677745
344.2769581306582 191.63829979505854 70.61106504965801
349.18496825943566 193.4168767065276 61.55625673941244
333.32603769288477 203.77828793472085 60.34858523049817
345.0777181072187 197.3737856813607 50.465944976721836
348.91023474556687 191.72508467024534 66.5548435248582
335.26774551948074 206.8886676324385 67.02651194478376
349.63497768197396 195.5598294827861 62.94485616774979
348.4454656349384 200.12606096581396 67.81640415636781
338.2785405304377 195.03138139123237 51.70253904284237
347.3566859540277 203.90235436700752 51.849397666984736
345.36704719610685 203.44965360041803 74.69041775119109
335.54794342128906 206.40109435607755 53.84279949052197
349.31344653138854 200.71030434305973 63.84094502646187
349.74234055501455 201.50243615732575 59.708888496946045
337.0009160000334 193.38195401639905 53.370174399773965
333.70917202759153 191.48290767711072 73.22683635565072
339.602532182659 192.7381118263565 52.57409115220311
332.14300306429715 192.33749807615723 61.601807717394145
333.75692053116904 193.91395656367698 56.46292071175817
339.64989519187003 189.5342006159821 56.12752534389462
339.87105937842176 188.11907294755915 60.62579436512721
335.25237517771507 204.4895028198034 74.66663582366978
345.40776131317295 194.29287172733365 53.95025972540331
338.1523933294877 200.88172738735156 76.66935626446926
347.06205939058873 206.41839081665057 63.04668953034501
341.60150243918685 208.3700829573539 71.6331955892523
341.10368600100946 207.80517945701612 50.71298914145046
347.9243300911488 190.42378104927658 63.48340106637561
347.1405345760086 194.22050212789864 55.61071292376231
337.1920164871358 191.63438985453982 74.14912812842711
334.7452336126251 189.71050404289738 68.14361075992058
343.8189451524273 208.56789385215174 54.68352875779012
341.80619888973166 188.45260978464054 66.98108135722339
347.95651559847124 191.1131913213819 59.82174210312812
346.9392985898208 205.91432956226853 53.334460932238926
343.90402881873956 207.56940781147074 70.84090884360259
333.9096580033916 199.87212588517372 56.5598272869539
345.312982131919 207.66126458528137 62.42625391522098
341.439285847402 193.77464837989888 71.96022985364975
330.7249488713265 194.66164691127543 66.53268593468806
338.1387842230434 208.51236521415137 68.60114490109164
343.7076395655827 207.26664061873362 50.49204959270621
345.2542782708492 197.9489274113437 73.04467345105874
337.8064483221875 201.02490558695425 48.92620211020657
339.71556804883056 188.42039574985958 58.551410792797796
346.58959162029817 189.77237573993034 61.12689953512704
334.46691318191677 189.7640612715719 64.69600239720182
344.7465672625924 190.48509012144797 56.73515221513036
349.5705975809814 199.62998190947476 57.8616282282123
346.28876770493395 201.14924659836583 72.4595933987686
335.9527242957995 201.23379248415893 76.11894422741105
338.0561278313347 190.8539988668531 54.389877941658675
335.7450404014119 207.59555929606967 63.75026675138208
331.6642877301531 195.6156808558344 71.97016520209036
349.3385764808278 193.57895861453176 64.51978394449382
346.9624112520689 198.65931318153892 70.24130092518202
333.0889742081821 190.7672412004154 63.13935851681658
333.33568671799566 202.7587807112546 57.533253275835676
345.7701464708715 190.42361038215515 68.98751859899807
349.1402637274343 196.3534949300339 65.16321882884061
349.2379196153482 198.1482987221947 62.851684569329144
344.1225052935897 192.04609607614884 54.979887205156054
349.20816925041777 196.25224506473805 60.664904762489584
348.39390749192586 201.1028867882628 65.72504105941354
343.9544015061911 193.54678548027385 53.39759555712251
332.98683406756817 197.86980598644885 58.420789545833365
348.0930646575036 205.33986817198283 57.64143574594718
344.02880703250213 208.65274558451637 58.463900618806264
334.1738770431049 205.5461571007485 58.16458084788048
349.63825886791335 200.32540603180783 61.75672281229316
337.01145464890635 206.24193188215494 74.14656186670535
339.5805756355232 197.02310681703793 50.53788521486025
334.8827796996531 202.09360611353293 74.12531888687992
333.28049414766997 190.8575773319581 71.01592815316212
345.9164094589144 203.46245337124063 49.902489570223615
339.47820452037564 206.41852133938136 49.375754370275494
349.25291479720335 200.09343758869366 55.69760686865229
346.48076481853417 204.67509892121535 71.38317924595869
347.0660956959973 206.710380895338 56.990760700665064
339.4821337706241 189.25974579962937 71.3247457833202
342.4760152701341 188.38731316148755 62.57573219261259
344.02499505867166 200.46734382209382 74.47870146857824
333.8523313184631 205.24563875726804 64.14086095029411
333.553384035011 200.46577004154102 60.063607199579685
342.9916868113361 205.09445571171818 75.2060367326033
347.1057162267461 195.4253458395169 69.83343318166656
345.81775283763733 207.8000596916528 57.66578260506748
343.555541939707 208.2706917652202 67.04088618175459
335.5750319662338 192.17677053914895 55.581363777158536
349.08246887777136 197.27716668419663 58.22907414121679
335.08448934968044 197.03382557827433 53.504659280185805
348.31732912769036 205.20641490616663 59.77669888389658
334.05715596844243 191.60174145089653 58.548407321933894
331.6156593581105 192.3226195603296 70.5993019063825
349.0029957218522 202.07913782337218 54.93519698353479
341.09578764156856 191.1481461176434 54.51193005946645
341.94195329760703 206.05836609383712 48.931053042090646
342.5391597837229 202.56096932755722 76.05061425741151
348.4094011938855 197.3353217124542 67.73206031748516
339.4392372956319 206.97150832152647 74.21721384619548
344.6528901997477 189.10097970423269 62.35460877127064
338.76765337362747 193.6972216135137 73.87598598485685
334.45276800187065 206.33521937334436 61.23473267396575
347.0892999096567 206.67217624545194 58.82808984799222
347.14063210243745 192.29972308739647 69.00263658464063
347.1148209474878 201.61266563429132 50.923068504371116
348.4027421825279 204.79653148379725 62.22009046065922
345.2557064707003 189.27248812540822 65.14052842273895
337.313717975359 188.56720820674502 64.39352390817355
335.9955072034935 204.8204210707255 50.316860533633005
346.3141032823979 196.67061209615756 71.41369863456013
348.0485331192463 199.86580433417413 52.24806097475102
340.26165311539546 208.49794261674546 53.048668411452034
342.28629637390947 196.91792950467132 50.226877412838036
346.62556114583475 191.76609116534834 56.792488727191866
347.735323426267 205.37749728676 64.32353344085783
339.6426483016573 200.60467558894703 48.10847266974975
338.298130093173 199.00023976636115 49.38747289505491
333.39708585969436 203.35020077483387 64.96726850603889
341.9559104723864 209.1593298291551 59.27023362703003
334.2848226258566 198.69059218565775 54.46322708965874
342.65159326544966 188.54192865776835 64.82425773473166
341.81588616734933 208.88355080278185 54.463785963635786
336.4920278644526 203.81162457142952 76.73325637416761
341.4392478388076 209.1743911108845 61.80387376648156
340.30345287502666 202.12545880452092 76.9561943662461
338.7593196880478 203.12621927517262 77.55792420824292
346.01951685673066 193.549871418497 70.59700272403869
343.4125839352625 198.3418714193811 74.71654969965927
338.5519191327337 190.88180497123952 72.49534547190011
347.68079536880975 205.65815265656846 55.568616231092236
336.86879307525686 197.52338543358925 51.39918259824518
333.1373175950635 190.55213269974462 66.48760873845058
333.56063808966064 196.5131820577581 73.66706069666478
347.23395338750237 196.04853176526598 53.95973371389327
347.95242462839474 201.97190544016806 53.05389574080269
335.0742533145728 199.18917498409323 52.55396307081252
331.9553240624418 197.51868543795754 60.94248500454594
341.12736814826684 190.66959283113948 71.49959616643613
348.7420195738672 198.9617961838266 65.50160790030323
342.59114856534364 203.8041749944116 47.9545041905625
343.51385706467806 199.08170958631945 48.672137323316505
344.18015453694846 208.26434942636428 64.36611547190225
348.6733267225969 194.445965914436 67.28232163374109
343.475647131789 201.50679948320413 47.17458123604655
341.17039472436636 208.66260871915406 69.46985981981425
340.26413489154965 188.09049598301988 62.79774292496401
340.54585988628094 188.53793269809907 69.13984814883877
332.1318559347644 198.05818101244412 66.7939314967157
POLYGONS 552 2208
3 37 168 277
3 0 46 277
3 277 26 37
3 212 266 26
3 26 266 37
3 56 51 46
3 46 51 277
3 51 26 277
3 39 0 262
3 56 75 51
3 34 75 56
3 34 56 72
3 72 44 34
3 61 94 168
3 46 0 39
3 51 31 26
3 26 31 212
3 266 194 47
3 194 212 165
3 34 134 75
3 134 211 75
3 47 147 221
3 221 146 47
3 44 19 34
3 19 134 34
3 173 18 28
3 2 147 47
3 44 199 19
3 19 199 103
3 39 201 72
3 72 201 44
3 186 96 165
3 165 96 65
3 186 13 96
3 103 169 19
3 19 169 134
3 134 169 14
3 14 169 16
3 46 39 72
3 72 56 46
3 212 186 165
3 18 108 28
3 8 70 66
3 4 91 206
3 206 91 164
3 4 81 1
3 45 91 1
3 1 91 4
3 81 4 117
3 194 165 2
3 2 47 194
3 266 212 194
3 12 166 153
3 153 166 45
3 167 64 84
3 146 221 120
3 8 145 137
3 8 66 145
3 66 224 145
3 18 148 224
3 9 218 191
3 117 178 184
3 81 101 1
3 64 167 129
3 210 178 139
3 139 178 206
3 206 117 4
3 206 178 117
3 28 108 229
3 171 184 178
3 81 117 136
3 117 184 136
3 214 227 184
3 42 59 78
3 59 69 78
3 78 69 30
3 224 66 190
3 190 18 224
3 243 157 9
3 162 108 243
3 243 9 162
3 171 178 210
3 84 64 171
3 2 219 15
3 84 20 10
3 66 193 190
3 190 193 151
3 66 70 193
3 193 70 151
3 190 176 18
3 18 176 108
3 190 243 176
3 176 243 108
3 127 263 73
3 263 157 151
3 16 274 216
3 103 274 169
3 169 274 16
3 65 249 165
3 165 249 2
3 249 219 2
3 75 247 51
3 51 247 31
3 75 211 247
3 247 211 31
3 173 148 18
3 186 197 13
3 197 231 42
3 14 181 134
3 134 181 211
3 217 97 221
3 17 265 65
3 260 219 265
3 15 147 2
3 15 217 147
3 137 145 143
3 73 132 118
3 78 22 42
3 22 140 13
3 42 116 59
3 42 231 116
3 243 115 157
3 157 115 151
3 243 190 115
3 115 190 151
3 15 143 217
3 147 217 221
3 216 45 271
3 45 166 271
3 216 91 45
3 216 164 91
3 12 7 166
3 7 77 166
3 274 164 216
3 219 80 15
3 137 143 80
3 80 143 15
3 148 180 224
3 224 180 145
3 148 97 180
3 23 97 173
3 173 97 148
3 145 180 143
3 97 217 180
3 180 217 143
3 12 232 7
3 195 207 232
3 232 215 7
3 7 215 196
3 232 207 215
3 195 259 207
3 35 163 259
3 196 215 11
3 163 138 109
3 264 234 138
3 234 203 138
3 225 155 170
3 170 49 109
3 170 109 38
3 138 203 109
3 203 38 109
3 138 35 264
3 138 163 35
3 245 33 27
3 27 33 270
3 197 42 22
3 22 13 197
3 25 60 140
3 137 200 8
3 273 24 269
3 273 33 24
3 238 53 60
3 60 53 140
3 55 21 5
3 54 76 60
3 60 76 238
3 54 204 76
3 220 12 235
3 12 153 235
3 195 232 220
3 220 232 12
3 231 197 131
3 131 197 186
3 17 124 86
3 17 29 124
3 5 238 76
3 76 204 5
3 13 29 96
3 80 219 260
3 86 124 21
3 249 65 265
3 265 219 249
3 17 86 265
3 265 86 260
3 86 246 260
3 246 200 260
3 21 238 5
3 21 124 238
3 17 65 29
3 65 96 29
3 21 172 86
3 245 246 172
3 172 246 86
3 200 137 260
3 137 80 260
3 203 62 38
3 273 269 62
3 62 269 38
3 245 172 55
3 55 172 21
3 55 32 24
3 24 32 269
3 204 32 5
3 5 32 55
3 8 242 70
3 242 27 270
3 31 131 212
3 212 131 186
3 231 52 211
3 211 52 31
3 231 131 52
3 52 131 31
3 241 25 30
3 30 25 78
3 78 25 22
3 22 25 140
3 140 53 13
3 13 53 29
3 59 122 69
3 100 63 116
3 116 63 59
3 238 124 53
3 53 124 29
3 48 30 69
3 55 24 245
3 245 24 33
3 77 71 166
3 166 71 271
3 211 181 231
3 231 181 116
3 269 32 225
3 225 32 204
3 200 246 27
3 27 246 245
3 16 99 14
3 14 99 100
3 16 114 99
3 142 177 104
3 218 9 87
3 87 9 157
3 99 114 253
3 67 205 127
3 30 251 241
3 241 251 102
3 122 248 69
3 248 48 69
3 205 104 177
3 35 259 85
3 263 127 87
3 87 157 263
3 142 83 198
3 235 101 83
3 122 253 248
3 253 114 71
3 71 114 271
3 127 110 87
3 127 205 110
3 110 205 177
3 58 195 220
3 151 57 263
3 263 57 73
3 57 132 73
3 59 63 122
3 122 63 253
3 100 99 63
3 63 99 253
3 102 160 49
3 160 48 11
3 60 25 54
3 54 25 241
3 77 253 71
3 118 50 41
3 41 50 270
3 118 132 50
3 50 132 270
3 177 40 218
3 177 142 40
3 40 142 198
3 218 40 191
3 198 189 40
3 40 189 191
3 196 48 248
3 58 104 85
3 235 43 220
3 235 83 43
3 43 83 142
3 218 87 110
3 110 177 218
3 58 220 43
3 271 114 216
3 216 114 16
3 225 204 155
3 155 204 54
3 116 181 100
3 100 181 14
3 198 83 141
3 67 223 205
3 35 85 223
3 163 36 259
3 259 36 207
3 248 253 77
3 205 223 104
3 104 223 85
3 118 240 73
3 264 67 240
3 11 48 196
3 270 273 41
3 270 33 273
3 38 225 170
3 38 269 225
3 132 57 70
3 70 57 151
3 73 240 127
3 127 240 67
3 207 36 215
3 215 36 11
3 102 170 155
3 102 49 170
3 118 41 234
3 234 41 203
3 273 62 41
3 41 62 203
3 109 49 163
3 163 49 36
3 7 196 77
3 77 196 248
3 49 160 36
3 36 160 11
3 270 132 242
3 242 132 70
3 35 223 264
3 264 223 67
3 48 251 30
3 259 195 85
3 195 58 85
3 264 240 234
3 234 240 118
3 104 58 142
3 58 43 142
3 102 155 241
3 155 54 241
3 48 160 251
3 251 160 102
3 235 244 101
3 101 244 1
3 235 153 244
3 1 244 45
3 45 244 153
3 23 149 275
3 275 149 28
3 23 173 149
3 149 173 28
3 27 242 200
3 200 242 8
3 268 227 188
3 188 189 268
3 189 135 191
3 189 188 135
3 135 188 183
3 227 136 184
3 227 268 136
3 233 272 214
3 214 272 227
3 272 188 227
3 183 133 156
3 233 93 133
3 133 93 156
3 191 130 9
3 9 130 162
3 188 272 183
3 233 133 272
3 272 133 183
3 135 183 130
3 130 191 135
3 162 130 156
3 156 130 183
3 129 256 64
3 64 256 214
3 129 233 256
3 256 233 214
3 237 23 275
3 136 192 81
3 81 192 101
3 141 192 268
3 268 192 136
3 101 141 83
3 101 192 141
3 198 141 189
3 141 268 189
3 221 121 120
3 120 121 23
3 221 97 121
3 121 97 23
3 262 113 112
3 230 111 10
3 10 111 84
3 230 167 111
3 111 167 84
3 230 158 258
3 258 158 68
3 230 112 158
3 158 112 68
3 10 107 230
3 262 112 107
3 107 112 230
3 68 106 258
3 106 159 6
3 167 267 129
3 250 105 275
3 6 237 105
3 105 237 275
3 94 95 168
3 39 92 201
3 39 262 92
3 6 105 276
3 6 90 237
3 237 90 175
3 6 159 90
3 90 159 175
3 276 106 6
3 107 10 92
3 92 262 107
3 120 185 146
3 146 185 61
3 120 175 185
3 185 175 61
3 237 119 23
3 23 119 120
3 237 175 119
3 119 175 120
3 175 261 61
3 61 261 94
3 175 159 261
3 261 159 94
3 84 257 20
3 20 257 210
3 84 171 257
3 257 171 210
3 199 252 3
3 74 228 3
3 3 228 199
3 74 103 228
3 228 103 199
3 210 226 20
3 210 139 226
3 20 254 152
3 20 226 254
3 254 226 3
3 3 213 74
3 3 226 213
3 213 226 139
3 93 236 156
3 156 236 229
3 236 250 229
3 229 209 28
3 28 209 275
3 229 250 209
3 209 250 275
3 258 208 267
3 267 208 276
3 258 106 208
3 208 106 276
3 250 236 98
3 98 236 93
3 129 187 233
3 233 187 93
3 129 98 187
3 187 98 93
3 108 174 229
3 229 174 156
3 108 162 174
3 174 162 156
3 152 179 92
3 92 179 201
3 152 252 179
3 179 252 201
3 276 161 98
3 98 161 250
3 276 105 161
3 161 105 250
3 74 154 103
3 103 154 274
3 74 164 154
3 154 164 274
3 201 150 44
3 44 150 199
3 201 252 150
3 150 252 199
3 164 126 206
3 206 126 139
3 164 74 126
3 152 125 20
3 20 125 10
3 152 92 125
3 125 92 10
3 213 139 126
3 126 74 213
3 152 255 252
3 252 255 3
3 152 254 255
3 255 254 3
3 214 239 64
3 64 239 171
3 214 184 239
3 239 184 171
3 159 202 94
3 0 182 262
3 262 182 113
3 0 95 182
3 182 95 113
3 113 144 112
3 112 144 68
3 113 202 144
3 144 202 68
3 68 128 106
3 106 128 159
3 68 202 128
3 128 202 159
3 61 123 146
3 61 168 123
3 123 168 37
3 266 89 37
3 266 47 89
3 89 47 146
3 94 222 95
3 95 222 113
3 94 202 222
3 222 202 113
3 123 37 89
3 89 146 123
3 168 88 277
3 277 88 0
3 168 95 88
3 88 95 0
3 258 82 230
3 230 82 167
3 258 267 82
3 82 267 167
3 98 79 276
3 276 79 267
3 98 129 79
3 79 129 267
