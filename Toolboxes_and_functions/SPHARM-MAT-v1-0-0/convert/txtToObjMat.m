function [vertices, faces] = txtToObjMat(icFileName, ibSegments)
    fRead = fopen(icFileName, 'r');

    vertices = [];
    vPoint = [];
    faces = [];
    imSegments = [];
    bHeader = 1;
    
    
    
    % Get the first line with an interesting information :
    for i=1:3
        cLine = fgets(fRead);
        
    end
    cLine = fgets(fRead);
    obj = cLine(1);
    cLine = cLine(2:end);
%     count = 1;
%     while(obj=='v')
%            cLine = fgets(fRead);
%            obj = cLine(1);
%            cLine = cLine(2:end); 
%            count = count + 1;
%     end
%    disp(count);
    if (obj ~= -1)
        % Store the Data corresponding to the points
        while ((obj == 'v') && ~isempty(vPoint) || bHeader)
            vPoint = str2num(cLine);
            if (~isempty(vPoint))
                bHeader = 0;
                vertices(end + 1, :) = vPoint(:, 1:3);
                if (length(vPoint) > 3)
                    vertices(end + 1, :) = vPoint(:, 4:6);
                    if (length(vPoint) > 6)
                        vertices(end + 1, :) = vPoint(:, 7:9);
                    end
                end
            end
            % get the next line
            cLine = fgets(fRead);
            obj = cLine(1);
            cLine = cLine(2:end);
        end


% New initilization:
vPoint =[];     
bHeader = 1;
       % Store the data corresponding to the faces:
       while ((obj == 'f') && ~isempty(vPoint) || bHeader)
            vPoint = str2num(cLine);
            if (~isempty(vPoint))
                bHeader = 0;
                faces(end + 1, :) = vPoint(:, 1:3);
                if (length(vPoint) > 3)
                     faces(end + 1, :) = vPoint(:, 4:6);
                     if (length(vPoint) > 6)
                         faces(end + 1, :) = vPoint(:, 7:9);
                     end
                 end
            end
            % get the next line
            cLine = fgets(fRead);
            obj = cLine(1);
            cLine = cLine(2:end);
            % disp(cLine);
        end 
       
    
        if (ibSegments)
            bHeader = 1;
            while (ischar(cLine) && ~isempty(vPoint) || bHeader)
                vPoint = str2num(cLine);
                if (~isempty(vPoint))
                    bHeader = 0;
                    imSegments(end + 1, :) = vPoint(:, 2:4);
                end
                cLine = fgets(fRead);
            end
        end
    end

    fclose(fRead);
end