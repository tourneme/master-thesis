function [vertices, faces] = vtkToObjMat(icFileName)
    fRead = fopen(icFileName, 'r');

    vertices = [];
    vPoint = [];
    faces = [];
    bHeader = 1;
    
    
    % Get the first line
    cLine = fgets(fRead);
    if (cLine ~= -1)
        % Store the Data corresponding to the points
        while (ischar(cLine) && ~isempty(vPoint) || bHeader)
            vPoint = str2num(cLine);
            if (~isempty(vPoint))
                bHeader = 0;
                vertices(end + 1, :) = vPoint(:, 1:3);
                if (length(vPoint) > 3)
                    vertices(end + 1, :) = vPoint(:, 4:6);
                    if (length(vPoint) > 6)
                        vertices(end + 1, :) = vPoint(:, 7:9);
                    end
                end
            end
            % get the next line
            cLine = fgets(fRead);
        end
        
% disp(cLine);

% New initilization:
vPoint =[];     
bHeader = 1;
       % Store the data corresponding to the faces:
       while (ischar(cLine) && ~isempty(vPoint) || bHeader)
            vPoint = str2num(cLine);
            if (~isempty(vPoint))
                bHeader = 0;
                faces(end + 1, :) = vPoint(:, 2:4);
            end
            % get the next line
            cLine = fgets(fRead);

        end 

    end

    fclose(fRead);
end