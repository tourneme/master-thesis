function eta = pwhNormalizationScalingFunction( tri)
%
% eta = pwhNormalizationScalingFunction( tri)
%
% Normalization constant for the scaling basis function associated with
% partition \a tri for the pseudo Haar wavelet basis.
%
% @param  tri  partition over which the scaling basis function is defined

  eta = 1.0;

end
