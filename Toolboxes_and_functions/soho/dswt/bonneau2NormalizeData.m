function coeffs = bonneau2NormalizeData( data, tri)
% 
% coeffs = bonneau2NormalizeData( data, tri)
%
% Normalize input data (usually at the finest level) so that it can be used
% as scaling function coefficients
% @return normalized coefficients
% @param data   input data to normalize
% @param tri    triangle those childs are associated with the data
  
  % void operation 
  coeffs = data;
  
end