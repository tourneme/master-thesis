function out = noop( in)
%
% out = noop( in)
%
% Pass-through function which returns the input argument without
% alteration. Usually used as helper function.
  
  out = in
  
end