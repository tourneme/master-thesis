function deg = rad_to_deg( rad)
%
% deg = rad_to_deg( rad)
%
% Convert from radians to degree

  deg = (rad * 180) / pi;
  
end