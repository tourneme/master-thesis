function rad = deg_to_rad( deg)
%
% rad = deg_to_rad( deg)
%
% Convert from degree to radians

  rad = (deg * pi) / 180;
  
end