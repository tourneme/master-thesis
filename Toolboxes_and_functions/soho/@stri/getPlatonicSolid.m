function ps = getPlatonicSolid( st)
% Get the platonic solid name

	ps = st.platonic_solid;

end