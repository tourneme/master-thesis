function level = getLevel( st)
% Get the level of the spherical triangle

  level = st.level;

end