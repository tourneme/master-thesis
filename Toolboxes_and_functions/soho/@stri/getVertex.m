function vert = getVertex( st, index)
%
% vert = getVertex( st, index)
%
% Get the index-th vertex of \a st in euclidean coordinates

  vert = st.verts_ec(:,index);

end