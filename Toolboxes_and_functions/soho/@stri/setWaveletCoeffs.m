function st = setWaveletCoeffs( st, vals)
%
% st = setWaveletCoeffs( st, index, val)
%
% Set the wavelet basis function coefficients for the all wavelet
% defined over \a st

  st.w_coeffs = vals;
  
end