function st = setChilds( st, c)
%
% st = setChilds( st, c)
%
% Set all childs
%
% @return  updated spherical triangle
% @param st  spherical triangle to update
% @param c   childs

  st.childs = c;

end