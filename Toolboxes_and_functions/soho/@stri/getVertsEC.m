function verts = getVertsEC( st)
% Get the vertices in euclidean coordinates
% verts  is a 3 x 3 matrix where each column represents the coordinates of one
% vertex

  verts = st.verts_ec;
  
end