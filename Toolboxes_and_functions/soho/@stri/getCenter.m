function cp = getCenter( st)
% Get the center of the spherical triangle \st
% @param st   spherical trinagle those centre is seeked
% @return cp  center of \st

  cp = st.c_point;

end