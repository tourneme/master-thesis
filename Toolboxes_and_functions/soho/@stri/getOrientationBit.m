function bit = getOrientationBit( st)
%
% bit = getOrientationBit( st)
%
% Get the orientation bit of the spherical triangle

  bit = st.orientation;

end