function tri = setVertsEC( tri, verts)
%
% tri = setVertsEC( tri, verts)
%
% Set the vertices of the triangle in euclidean space
%
% @param  tri  triangle those vertices to set
% @param  verts  new vertices of triangle

  tri.verts_ec = verts;
  
end