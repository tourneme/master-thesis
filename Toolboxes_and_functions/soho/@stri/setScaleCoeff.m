function st = setScalingCoeff( st, val)
% Get scaling function coefficient of the scaling function associated with the
% sub-domain represented by the spherical triangle

  st.scale_coeff = val;

end