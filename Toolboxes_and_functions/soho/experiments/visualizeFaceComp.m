x_min = 7 * 1024;
x_max = 8 * 1024;
y_min = -0.2;
y_max = 0.2;


subplot( 4, 4, 1);
bar( coeffs_fig1_1_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 2);
bar( coeffs_fig1_2_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 3);
bar( coeffs_fig1_3_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 4);
bar( coeffs_fig1_4_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 5);
bar( coeffs_fig2_1_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 6);
bar( coeffs_fig2_2_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 7);
bar( coeffs_fig2_3_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 8);
bar( coeffs_fig2_4_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 9);
bar( coeffs_fig3_1_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 10);
bar( coeffs_fig3_2_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 11);
bar( coeffs_fig3_3_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 12);
bar( coeffs_fig3_4_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 13);
bar( coeffs_fig4_1_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 14);
bar( coeffs_fig4_2_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 15);
bar( coeffs_fig4_3_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);

subplot( 4, 4, 16);
bar( coeffs_fig4_4_synth);
xlim([x_min x_max]);
ylim([y_min y_max]);
